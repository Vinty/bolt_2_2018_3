﻿using System;

namespace Bolt
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
	public sealed class ExtractAttribute : Attribute
	{
		public ExtractAttribute(bool extract)
		{
			this.extract = extract;
		}

		public ExtractAttribute()
		{
			extract = true;
		}

		public bool extract { get; }
	}
}