﻿using System;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public abstract class EventMachine<TGraph, TMacro> : Machine<TGraph, TMacro>, IEventMachine
		where TGraph : class, IGraph, new()
		where TMacro : Macro<TGraph>, new()
	{
		protected void TriggerEvent(string name)
		{
			if (hasGraph)
			{
				TriggerRegisteredEvent(new EventHook(name, this), new EmptyEventArgs());
			}
		}

		// We use our own enabled bool which does not return true in Awake
		// to avoid double OnEnable triggering due to GraphNest delegates
		// See: https://support.ludiq.io/communities/5/topics/1971-/
		[DoNotSerialize]
		private bool _enabled;
		
		[InspectorLabel("Hybrid Mode")]
		public HybridRuntimeMode hybridRuntimeMode { get; set; }

		protected void TriggerEvent<TArgs>(string name, TArgs args)
		{
			if (hasGraph)
			{
				TriggerRegisteredEvent(new EventHook(name, this), args);
			}
		}

		protected void TriggerUnregisteredEvent(string name)
		{
			if (hasGraph)
			{
				TriggerUnregisteredEvent(name, new EmptyEventArgs());
			}
		}

		protected virtual void TriggerRegisteredEvent<TArgs>(EventHook hook, TArgs args)
		{
			EventBus.Trigger(hook, args);
		}

		protected virtual void TriggerUnregisteredEvent<TArgs>(EventHook hook, TArgs args)
		{
			using (var stack = reference.ToStackPooled())
			{
				stack.TriggerEventHandler(_hook => _hook == hook, args, parent => true, true);
			}
		}

		protected override void Awake()
		{
			GlobalMessageListener.Require();
			
			var graph = nest.graph;

			if (graph != null)
			{
				var editorRuntimeMode = EditorRuntimeModeBinding.runtimeMode;

				var swapToScript = false;

				var scriptIsUpToDate = graph.generatedVersionID > 0 && graph.generatedVersionID >= graph.liveVersionID;

				if (editorRuntimeMode == RuntimeMode.Hybrid)
				{
					switch (hybridRuntimeMode)
					{
						case HybridRuntimeMode.Automatic: swapToScript = scriptIsUpToDate; break;
						case HybridRuntimeMode.ForceGenerated: swapToScript = true; break;
						case HybridRuntimeMode.ForceLive: swapToScript = false; break;
						default: throw new UnexpectedEnumValueException<HybridRuntimeMode>(hybridRuntimeMode);
					}
				}
				else
				{
					swapToScript = editorRuntimeMode == RuntimeMode.Generated;
				}

				if (swapToScript)
				{
					if (scriptIsUpToDate)
					{
						var machineScriptTypeName = graph?.machineScriptTypeName;

						if (machineScriptTypeName != null)
						{
							IMachineScript machineScript = null;

							foreach (var assembly in RuntimeCodebase.assemblies)
							{
								var machineType = assembly.GetType(machineScriptTypeName);

								if (machineType == null)
								{
									continue;
								}

								MachineScriptAwakeHelper.awakeData = new MachineScriptAwakeData(graph.CreateData());

								machineScript = (IMachineScript)gameObject.AddComponent(machineType);

								break;
							}

							if (machineScript == null)
							{
								Debug.LogError($"Could not find machine script '{machineScriptTypeName}' on '{name}'.", gameObject);
							}
						}
					}
					else
					{
						Debug.LogError($"Graph script on '{name}' is out of date and will not be run on the current runtime.\nGenerated Version: {graph.generatedVersionID}. Live Version: {graph.liveVersionID}", gameObject);
					}

					destroyedDuringSwap = true;

					Destroy(this);

					return;
				}
			}

			base.Awake();
		}

		public override void InstantiateNest()
		{
			base.InstantiateNest();

			if (UnityThread.allowsAPI && _enabled)
			{
				OnEnable();
			}
		}

		public override void UninstantiateNest()
		{
			if (UnityThread.allowsAPI && _enabled)
			{
				OnDisable();
			}

			base.UninstantiateNest();
		}

		protected virtual void OnEnable()
		{
			_enabled = true;

			TriggerEvent(EventHooks.OnEnable);
		}

		protected virtual void Start() 
		{
			TriggerEvent(EventHooks.Start);
		}

		protected virtual void Update()
		{
			TriggerEvent(EventHooks.Update);
		}

		protected virtual void FixedUpdate()
		{
			TriggerEvent(EventHooks.FixedUpdate);
		}

		protected virtual void LateUpdate()
		{
			TriggerEvent(EventHooks.LateUpdate);
		}

		protected virtual void OnDisable() 
		{ 
			if (destroyedDuringSwap)
			{
				return;
			}

			TriggerEvent(EventHooks.OnDisable);

			_enabled = false;
		}

		protected override void OnDestroy()
		{
			if (destroyedDuringSwap)
			{
				return;
			}

			try
			{
				TriggerEvent(EventHooks.OnDestroy);
			}
			finally
			{
				base.OnDestroy();
			}
		}

		public override void TriggerAnimationEvent(AnimationEvent animationEvent)
		{
			TriggerEvent(EventHooks.AnimationEvent, animationEvent);
		}

		public override void TriggerUnityEvent(string name)
		{
			TriggerEvent(EventHooks.UnityEvent, name);
		}

		private void OnDrawGizmos()
		{
			TriggerUnregisteredEvent(EventHooks.OnDrawGizmos);
		}

		private void OnDrawGizmosSelected()
		{
			TriggerUnregisteredEvent(EventHooks.OnDrawGizmosSelected);
		}
	}
}
