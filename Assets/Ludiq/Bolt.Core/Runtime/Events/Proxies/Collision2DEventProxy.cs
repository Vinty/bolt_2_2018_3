﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class Collision2DEventProxy : MonoBehaviour
	{
		public void OnCollisionEnter2D(Collision2D collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionEnter2D, gameObject, collision);
		}

		public void OnCollisionExit2D(Collision2D collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionExit2D, gameObject, collision);
		}

		public void OnCollisionStay2D(Collision2D collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionStay2D, gameObject, collision);
		}
	}
}
