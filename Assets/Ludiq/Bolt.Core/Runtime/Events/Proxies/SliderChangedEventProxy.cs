﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class SliderChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<Slider>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnSliderValueChanged, gameObject, value));
		}
	}
}
