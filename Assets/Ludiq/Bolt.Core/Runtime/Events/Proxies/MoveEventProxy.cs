﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class MoveEventProxy : MonoBehaviour
	{
		public void OnMove(AxisEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnMove, gameObject, eventData);
		}
	}
}
