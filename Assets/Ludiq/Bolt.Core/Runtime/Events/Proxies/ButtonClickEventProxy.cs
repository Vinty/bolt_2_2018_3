﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ButtonClickEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<Button>()?.onClick?.AddListener(() => EventBus.Trigger(EventHooks.OnButtonClick, gameObject));
		}
	}
}
