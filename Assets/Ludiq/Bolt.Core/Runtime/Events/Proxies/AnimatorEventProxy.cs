﻿using UnityEngine;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class AnimatorEventProxy : MonoBehaviour
	{
		private void OnAnimatorMove()
		{
			EventBus.Trigger(EventHooks.OnAnimatorMove, gameObject);
		}

		private void OnAnimatorIK(int layerIndex)
		{
			EventBus.Trigger(EventHooks.OnAnimatorIK, gameObject, layerIndex);
		}
	}
}
