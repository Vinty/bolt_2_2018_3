﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class TriggerEventProxy : MonoBehaviour
	{
		public void OnTriggerEnter(Collider other)
		{
			EventBus.Trigger(EventHooks.OnTriggerEnter, gameObject, other);
		}

		public void OnTriggerExit(Collider other)
		{
			EventBus.Trigger(EventHooks.OnTriggerExit, gameObject, other);
		}

		public void OnTriggerStay(Collider other)
		{
			EventBus.Trigger(EventHooks.OnTriggerStay, gameObject, other);
		}
	}
}
