﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class CollisionEventProxy : MonoBehaviour
	{
		public void OnCollisionEnter(Collision collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionEnter, gameObject, collision);
		}

		public void OnCollisionExit(Collision collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionExit, gameObject, collision);
		}

		public void OnCollisionStay(Collision collision)
		{
			EventBus.Trigger(EventHooks.OnCollisionStay, gameObject, collision);;
		}
	}
}
