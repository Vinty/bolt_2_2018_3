﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ControllerColliderHitEventProxy : MonoBehaviour
	{
		public void OnControllerColliderHit(ControllerColliderHit hit)
		{
			EventBus.Trigger(EventHooks.OnControllerColliderHit, gameObject, hit);
		}
	}
}
