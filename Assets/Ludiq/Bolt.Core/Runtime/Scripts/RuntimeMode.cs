﻿namespace Bolt
{
	public enum RuntimeMode
	{
		/// <summary>
		/// Graphs are run from optimized reflection memory for live editing and in-depth debugging.
		/// </summary>
		Live,

		/// <summary>
		/// Graphs are run from their generated C# scripts for optimal performance.
		/// </summary>
		Generated,

		/// <summary>
		/// Graphs are run from their generated C# scripts if they are up to date, otherwise from optimized reflection.
		/// You can force each object to use either the Live or Generated runtime from the inspector.
		/// </summary>
		Hybrid,
	}
}
