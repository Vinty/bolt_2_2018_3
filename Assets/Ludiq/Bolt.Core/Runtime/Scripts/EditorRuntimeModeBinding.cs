﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public static class EditorRuntimeModeBinding
	{
		public static Func<RuntimeMode> runtimeModeBinding;

		public static RuntimeMode runtimeMode => runtimeModeBinding != null ? runtimeModeBinding() : RuntimeMode.Generated;
	}
}
