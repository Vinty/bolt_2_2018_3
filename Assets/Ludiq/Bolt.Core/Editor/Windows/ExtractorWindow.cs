﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public sealed class ExtractorWindow : SinglePageWindow<ExtractorPage>
	{
		public static ExtractorWindow instance { get; }

		static ExtractorWindow()
		{
			instance = new ExtractorWindow();
		}
		
		protected override ExtractorPage CreatePage()
		{
			return new ExtractorPage();
		}

		protected override void ConfigureWindow()
		{
			window.titleContent = new GUIContent("Extractor");
			window.minSize = new Vector2(500, 400);
		}

		[MenuItem("Tools/Bolt/Extractor...", priority = BoltProduct.ToolsMenuPriority + 301)]
		private static void HookUpdateWizard()
		{
			if (instance.isOpen)
			{
				instance.window.Focus();
			}
			else
			{
				Show();
			}
		}

		public new static void Show()
		{
			instance.ShowUtility();
			instance.window.Center();
		}
	}
}
