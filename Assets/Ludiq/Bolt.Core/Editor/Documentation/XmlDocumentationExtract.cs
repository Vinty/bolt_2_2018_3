﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public sealed class XmlDocumentationExtract : IExtract
	{
		public Dictionary<string, XmlDocumentationTags> documentations { get; set; } = new Dictionary<string, XmlDocumentationTags>();
	}
}
