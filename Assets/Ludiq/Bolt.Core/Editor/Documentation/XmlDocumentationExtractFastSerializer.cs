﻿using System.IO;
using Ludiq;

namespace Bolt
{
	public class XmlDocumentationExtractFastSerializer : FastSerializer<XmlDocumentationExtract>
	{
		public static XmlDocumentationExtractFastSerializer instance { get; } = new XmlDocumentationExtractFastSerializer();

		public override XmlDocumentationExtract Instantiate()
		{
			return new XmlDocumentationExtract();
		}

		public override void Write(XmlDocumentationExtract value, BinaryWriter writer)
		{
			writer.Write(value.documentations.Count);

			foreach (var documentation in value.documentations)
			{
				writer.Write(documentation.Key);
				XmlDocumentationTagsFastSerializer.instance.Write(documentation.Value, writer);
			}
		}

		public override void Read(ref XmlDocumentationExtract value, BinaryReader reader)
		{
			var documentationsCount = reader.ReadInt32();

			for (int i = 0; i < documentationsCount; i++)
			{
				value.documentations.Add(reader.ReadString(), XmlDocumentationTagsFastSerializer.instance.Read(reader));
			}
		}
	}
}
