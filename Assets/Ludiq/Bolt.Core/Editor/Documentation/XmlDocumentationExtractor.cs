﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Ludiq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Compilation;
using Assembly = System.Reflection.Assembly;
using Debug = UnityEngine.Debug;

namespace Bolt
{
	public sealed class XmlDocumentationExtractor : IExtractor
	{
		static XmlDocumentationExtractor()
		{
			try
			{
				SyncVS = Assembly.GetAssembly(typeof(Editor)).GetType("UnityEditor.SyncVS", true);
				SyncVS_SyncSolution = SyncVS.GetMethod("SyncSolution", BindingFlags.Static | BindingFlags.Public);
				
				if (SyncVS_SyncSolution == null)
				{
					throw new MissingMemberException(SyncVS.ToString(), "SyncSolution");
				}
			}
			catch (Exception ex)
			{
				throw new UnityEditorInternalException(ex);
			}
		}

		public string label => "XML Documentation";

		public string path => BoltCore.Paths.xmlDocumentation;

		public int extractPriority => 1;

		public int loadPriority => 2;

		public IExtract ReadExtract(Stream stream)
		{
			using (var reader = new BinaryReader(stream))
			{
				return XmlDocumentationExtractFastSerializer.instance.Read(reader);
			}
		}

		public void WriteExtract(IExtract extract, Stream stream)
		{
			using (var writer = new BinaryWriter(stream))
			{
				XmlDocumentationExtractFastSerializer.instance.Write((XmlDocumentationExtract)extract, writer);
			}
		}
		
		public IExtract Extract(ExtractionConfiguration configuration)
		{
			if (configuration.mode == ExtractionMode.Fast || !BoltCore.Configuration.extractDocumentation)
			{
				return null;
			}

			XmlDocumentationExtract extract = null;

			Task.Run("Extracting XML Documentation...", 2, extraction =>
			{
				extraction.StartStep("Generating Project Documentation Files...");

				if (BoltCore.Configuration.generateDocumentation)
				{
					Thread.Sleep(100); // Just to give a chance to the progress window to repaint before freezing

					UnityAPI.Await(SyncUnitySolution);

					extraction.AllowCancellation();

					var unityProjectPaths = new HashSet<string>();

					foreach (var assemblyDefinition in UnityAPI.Await(CompilationPipeline.GetAssemblies))
					{
						var unityProjectPath = Paths.AssemblyProject(assemblyDefinition);

						if (File.Exists(unityProjectPath))
						{
							unityProjectPaths.Add(unityProjectPath);
						}
					}

					Task.Run("Generating Project Documentation Files...", unityProjectPaths.Count, projectsGeneration =>
					{
						foreach (var unityProjectPath in unityProjectPaths)
						{
							projectsGeneration.AllowCancellation();

							var unityProjectFileName = Path.GetFileName(unityProjectPath);

							projectsGeneration.StartStep(unityProjectFileName);

							var warning = $"Failed to generate documentation for project '{unityProjectFileName}'.\nSome editor documentation may be missing.";

							try
							{
								GenerateUnityProjectDocumentation(unityProjectPath);
							}
							catch (BuildFailedException ex)
							{
								var errorLines = ExtractErrorLines(ex.Message).ToArray();

								Debug.LogWarning($"{warning} Potential Errors:\n\n{errorLines.ToSeparatedString("\n\n")}\n");
							}
							catch (Exception ex)
							{
								Debug.LogWarning($"{warning}\n{ex}");
							}

							projectsGeneration.CompleteStep();
						}
					});
				}

				extraction.CompleteStep();

				extraction.AllowCancellation();

				extraction.StartStep("Parsing Documentation Files...");

				var assemblyDocumentationPaths = configuration.types.Select(t => t.Assembly).Distinct().Select(GetAssemblyDocumentationPath).NotNull().ToHashSet();
				
				var documentations = new Dictionary<string, XmlDocumentationTags>();

				Task.Run("Parsing Documentation Files...", assemblyDocumentationPaths.Count, parsing =>
				{
					foreach (var assemblyDocumentationPath in assemblyDocumentationPaths)
					{
						parsing.AllowCancellation();

						parsing.StartStep(Path.GetFileName(assemblyDocumentationPath));

						var assemblyDocumentations = ParseDocumentationXml(assemblyDocumentationPath);

						foreach (var assemblyDocumentation in assemblyDocumentations)
						{
							if (!documentations.ContainsKey(assemblyDocumentation.Key))
							{
								documentations.Add(assemblyDocumentation.Key, assemblyDocumentation.Value);
							}
						}

						parsing.CompleteStep();
					}
				});

				extraction.CompleteStep();

				extract = new XmlDocumentationExtract();
				extract.documentations = documentations;
			});

			if (extract != null)
			{
				Debug.Log($"XML documentation extraction succeeded.\n{extract.documentations.Count} documentation tags included.");

				XmlDocumentation.Load(extract.documentations);
			}

			return extract;
		}
		
		void IExtractor.Load(IExtract extract)
		{
			Load((XmlDocumentationExtract)extract);
		}
		
		public void Load(XmlDocumentationExtract extract)
		{
			XmlDocumentation.Load(extract.documentations);
		}

		public void Fallback()
		{
			XmlDocumentation.Clear();
		}

		public int Compare(ExtractionConfiguration configuration)
		{
			return 0;
		}


		#region Parsing

		private static readonly string[] fallbackDirectories =
		{
			LudiqCore.Paths.dotNetDocumentation,
			LudiqCore.Paths.assemblyDocumentations
		};

		public static string GetAssemblyDocumentationPath(Assembly assembly)
		{
			var assemblyPath = assembly.Location;

			var documentationPath = Path.ChangeExtension(assemblyPath, ".xml");
			
			if (!File.Exists(documentationPath))
			{
				foreach (var fallbackDirectory in fallbackDirectories)
				{
					if (Directory.Exists(fallbackDirectory))
					{
						var fallbackPath = Path.Combine(fallbackDirectory, Path.GetFileName(documentationPath));

						if (File.Exists(fallbackPath))
						{
							documentationPath = fallbackPath;
							break;
						}
					}
				}
			}

			if (!File.Exists(documentationPath))
			{
				return null;
			}

			return documentationPath;
		}

		private static Dictionary<string, XmlDocumentationTags> ParseDocumentationXml(string path)
		{
			if (!File.Exists(path))
			{
				return null;
			}

			XDocument document;

			try
			{
				document = XDocument.Load(path);
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Failed to load XML documentation:\n" + ex);

				return null;
			}

			var ns = document.Root.Name.Namespace;

			var dictionary = new Dictionary<string, XmlDocumentationTags>();

			foreach (var memberElement in document.Element(ns + "doc").Element(ns + "members").Elements(ns + "member"))
			{
				var nameAttribute = memberElement.Attribute("name");

				if (nameAttribute != null)
				{
					if (dictionary.ContainsKey(nameAttribute.Value))
					{
						// Unity sometimes has duplicate member documentation in their XMLs.
						// Safely skip.
						continue;
					}

					dictionary.Add(nameAttribute.Value, new XmlDocumentationTags(memberElement));
				}
			}

			return dictionary;
		}
		
		#endregion



		#region Unity Projects

		private static Type SyncVS; // internal class UnityEditor.SyncVS : AssetPostprocessor

		private static MethodInfo SyncVS_SyncSolution; // public static void SyncSolution()
		
		public static void SyncUnitySolution()
		{
			try
			{
				SyncVS_SyncSolution.Invoke(null, null);
			}
			catch (Exception ex)
			{
				throw new UnityEditorInternalException(ex);
			}
		}

		public static string GenerateUnityProjectDocumentation(string projectPath)
		{
			PathUtility.CreateDirectoryIfNeeded(LudiqCore.Paths.assemblyDocumentations);

			var projectName = Path.GetFileNameWithoutExtension(projectPath);

			if (!File.Exists(projectPath))
			{
				throw new FileNotFoundException($"Project file not found: '{projectPath}'.");
			}

			if (!File.Exists(Paths.projectBuilder))
			{
				throw new FileNotFoundException($"Project builder not found: '{Paths.projectBuilder}'.\nYou can download the latest MSBuild from: {Paths.MsBuildDownloadLink}");
			}

			using (var process = new Process())
			{
				var projectXml = XDocument.Load(projectPath);
				var projectRootNamespace = projectXml.Root.GetDefaultNamespace();
				var assemblyName = projectXml.Descendants(projectRootNamespace + "AssemblyName").Single().Value;
				var documentationPath = Path.Combine(LudiqCore.Paths.assemblyDocumentations, assemblyName + ".xml");

				process.StartInfo = new ProcessStartInfo();
				process.StartInfo.FileName = Paths.projectBuilder;
				
				process.StartInfo.Arguments =
					"/p:LangVersion=7.2 " +
					"/p:Configuration=Debug " +
					"/p:GenerateDocumentation=true " +
					"/p:WarningLevel=0 " +
					$"/p:DocumentationFile=\"{documentationPath}\" " +
					$"\"{projectPath}\"";

				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.RedirectStandardError = true;
				process.StartInfo.CreateNoWindow = true;
				process.StartInfo.UseShellExecute = false;

				var timeout = 20000;

				var output = new StringBuilder();
				var error = new StringBuilder();
				
				using (var outputWaitHandle = new AutoResetEvent(false))
				using (var errorWaitHandle = new AutoResetEvent(false))
				{
					process.OutputDataReceived += (sender, e) =>
					{
						if (e.Data == null)
						{
							outputWaitHandle.Set();
						}
						else
						{
							output.AppendLine(e.Data);
						}
					};

					process.ErrorDataReceived += (sender, e) =>
					{
						if (e.Data == null)
						{
							errorWaitHandle.Set();
						}
						else
						{
							error.AppendLine(e.Data);
						}
					};

					process.Start();

					process.BeginOutputReadLine();
					process.BeginErrorReadLine();

					if (process.WaitForExit(timeout) &&
						outputWaitHandle.WaitOne(timeout) &&
						errorWaitHandle.WaitOne(timeout))
					{
						if (process.ExitCode != 0)
						{
							throw new BuildFailedException($"Failed to build project '{projectName}':\n{process.StartInfo.Arguments}\n{error}\n{output}");
						};

						return output.ToString();
					}
					else
					{
						throw new TimeoutException("Build process timed out.");
					}
				}
			}
		}

		private static IEnumerable<string> ExtractErrorLines(string s)
		{
			var lines = s.Split('\n');

			var errorLines = new List<string>();

			var maxLength = 300;

			foreach (var line in lines)
			{
				if (line.ContainsInsensitive("Error") || line.ContainsInsensitive("Warning"))
				{
					var _line = line;

					if (line.Length > maxLength)
					{
						_line = _line.Substring(0, maxLength) + "...";
					}

					errorLines.Add(_line);
				}
			}

			return errorLines;
		}

		#endregion
	}
}
