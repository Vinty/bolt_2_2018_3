﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Editor(typeof(IEventMachine))]
	public class EventMachineEditor : MachineEditor
	{
		protected Metadata hybridModeMetadata => metadata[nameof(IEventMachine.hybridRuntimeMode)];

		protected override bool showConfiguration => true;

		public EventMachineEditor(Metadata metadata) : base(metadata) { }

		private IGraph graph => (IGraph)graphMetadata.value;

		private bool graphScriptIsUpToDate => graph.generatedVersionID > 0 && graph.generatedVersionID >= graph.liveVersionID;

		private bool showHybridModeMessage => graph != null;

		private HybridRuntimeMode hybridRuntimeMode => (HybridRuntimeMode)hybridModeMetadata.value;

		private MessageType hybridModeMessageType
		{
			get
			{
				if (hybridRuntimeMode == HybridRuntimeMode.Automatic)
				{
					if (graphScriptIsUpToDate)
					{
						return MessageType.Info;
					}
					else
					{
						return MessageType.Warning;
					}
				}
				else if (hybridRuntimeMode == HybridRuntimeMode.ForceGenerated)
				{
					if (graphScriptIsUpToDate)
					{
						return MessageType.Info;
					}
					else
					{
						return MessageType.Error;
					}
				}
				else if (hybridRuntimeMode == HybridRuntimeMode.ForceLive)
				{
					if (graphScriptIsUpToDate)
					{
						return MessageType.Warning;
					}
					else
					{
						return MessageType.Info;
					}
				}
				else
				{
					throw new UnexpectedEnumValueException<HybridRuntimeMode>(hybridRuntimeMode);
				}
			}
		}

		private string hybridModeMessage
		{
			get
			{
				if (hybridRuntimeMode == HybridRuntimeMode.Automatic)
				{
					if (graphScriptIsUpToDate)
					{
						return "Script is up to date. The Generated runtime will be used.";
					}
					else
					{
						return "Script is out of date. The Live runtime will be used.";
					}
				}
				else if (hybridRuntimeMode == HybridRuntimeMode.ForceGenerated)
				{
					if (graphScriptIsUpToDate)
					{
						return "Script is up to date.";
					}
					else
					{
						return "Script is out of date. The Generated runtime will fail!";
					}
				}
				else if (hybridRuntimeMode == HybridRuntimeMode.ForceLive)
				{
					if (graphScriptIsUpToDate)
					{
						return "Script is up to date, but Live runtime will be used anyway.";
					}
					else
					{
						return "Script is out of date.";
					}
				}
				else
				{
					throw new UnexpectedEnumValueException<HybridRuntimeMode>(hybridRuntimeMode);
				}
			}
		}

		protected override float GetConfigurationHeight(float width)
		{
			var height = 0f;

			height += LudiqGUI.GetInspectorHeight(this, hybridModeMetadata, width);

			if (showHybridModeMessage)
			{
				height += EditorGUIUtility.standardVerticalSpacing;
				height += LudiqGUIUtility.GetHelpBoxHeight(hybridModeMessage, hybridModeMessageType, width);
			}

			height += EditorGUIUtility.standardVerticalSpacing;

			return height;
		}

		protected override void OnConfigurationGUI(Rect position)
		{
			var hybridModePosition = position.VerticalSection(ref y, LudiqGUI.GetInspectorHeight(this, hybridModeMetadata, position.width));
			LudiqGUI.Inspector(hybridModeMetadata, hybridModePosition);

			if (showHybridModeMessage)
			{
				y += EditorGUIUtility.standardVerticalSpacing;
				var hybridModeMessagePosition = position.VerticalSection(ref y, LudiqGUIUtility.GetHelpBoxHeight(hybridModeMessage, hybridModeMessageType, position.width));
				EditorGUI.HelpBox(hybridModeMessagePosition, hybridModeMessage, hybridModeMessageType);
			}

			y += EditorGUIUtility.standardVerticalSpacing;
		}
	}
}
