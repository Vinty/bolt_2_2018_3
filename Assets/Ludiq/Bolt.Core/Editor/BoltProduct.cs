using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Product(ID)]
	public sealed class BoltProduct : Product
	{
		public BoltProduct() { }

		public override void Initialize()
		{
			base.Initialize();
			
			logo = BoltCore.Resources.LoadTexture("Logos/LogoBolt.png", CreateTextureOptions.Scalable).Single();
			authorLogo = BoltCore.Resources.LoadTexture("Logos/LogoLudiq.png", CreateTextureOptions.Scalable).Single();
		}

		public override string configurationPanelLabel => "Bolt";

		public override string name => "Bolt";
		public override string description => "The ultimate visual scripting solution for Unity.";
		public override string authorLabel => "Designed & Developed by ";
		public override string author => "Lazlo Bonin";
		public override string copyrightHolder => "Ludiq";
		public override SemanticVersion version => "2.0.0a3";
		public override string publisherUrl => "http://ludiq.io";
		public override string websiteUrl => "http://ludiq.io/bolt";
		public override string communityUrl => "http://ludiq.io/bolt/community";
		public override string learnUrl => "http://ludiq.io/bolt/manual";
		public override string assetStoreUrl => "http://ludiq.io/bolt/buy";

		public const string ID = "Bolt";

		public const int ToolsMenuPriority = -990000;
		public const int DeveloperToolsMenuPriority = ToolsMenuPriority + 1000;

		public static BoltProduct instance => (BoltProduct)ProductContainer.GetProduct(ID);
		
		public string documentationPath => Path.Combine(rootPath, "Documentation");

		[PreferenceItem("Bolt")]
		private static void HookConfigurationPanel()
		{
			instance.configurationPanel.PreferenceItem();
		}

		[MenuItem("Tools/Bolt/About...", priority = ToolsMenuPriority + 1)]
		private static void HookAboutWindow()
		{
			instance.aboutWindow.Show();
		}

		[MenuItem("Tools/Bolt/Support...", priority = ToolsMenuPriority + 2)]
		private static void HookSupport()
		{
			WebWindow.Show(new GUIContent("Bolt Support", LudiqCore.Icons.supportWindow[IconSize.Small]), instance.communityUrl);
		}

		[MenuItem("Tools/Bolt/Manual...", priority = ToolsMenuPriority + 3)]
		private static void HookDocumentation()
		{
			WebWindow.Show(new GUIContent("Bolt Manual", LudiqCore.Icons.supportWindow[IconSize.Small]), instance.learnUrl);
		}

		[MenuItem("Tools/Bolt/Setup Wizard...", priority = ToolsMenuPriority + 101)]
		private static void HookSetupWizard()
		{
			instance.setupWizard.Show();
		}

		[MenuItem("Tools/Bolt/Update Wizard...", priority = ToolsMenuPriority + 102)]
		private static void HookUpdateWizard()
		{
			instance.updateWizard.Show();
		}

		[MenuItem("Tools/Bolt/Configuration...", priority = ToolsMenuPriority + 103)]
		private static void HookConfiguration()
		{
			instance.configurationPanel.Show();
		}

		[MenuItem("Tools/Bolt/Generate C# Scripts", priority = ToolsMenuPriority + 201)]
		private static void Generate()
		{
			Generation.Generate();
		}

		[MenuItem("Tools/Bolt/Developer/Delete Generated C# Scripts...", priority = ToolsMenuPriority + 1501)]
		private static void CleanGeneratedScripts()
		{
			if (!EditorUtility.DisplayDialog("Delete Generated C# Scripts", "Are you sure you want to delete all generated C# scripts?\n\n(You can always regenerate them with \"Tools/Bolt/Generate C# Scripts\")", "Confirm", "Cancel"))
			{
				return;
			}
			
			Generation.Clean();
		}
		
		[MenuItem("Tools/Bolt/Toggle Window Layout... %Space", priority = ToolsMenuPriority + 401)]
		private static void ToggleWindowLayout()
		{
			if (WindowLayoutUtility.lastLoadedLayoutName == BoltCore.Paths.windowLayoutName)
			{
				if (!File.Exists(BoltCore.Paths.restoredWindowLayout))
				{
					EditorUtility.DisplayDialog("Bolt Window Layout", "No Bolt restore window layout found.", "OK");
					return;
				}

				WindowLayoutUtility.SaveLayout(BoltCore.Paths.windowLayout);
				WindowLayoutUtility.LoadLayout(BoltCore.Paths.restoredWindowLayout);
			}
			else
			{
				if (!File.Exists(BoltCore.Paths.windowLayout))
				{
					EditorUtility.DisplayDialog("Bolt Window Layout", "No Bolt window layout found. Create a layout and save it first.", "OK");
					return;
				}

				WindowLayoutUtility.SaveLayout(BoltCore.Paths.restoredWindowLayout);
				WindowLayoutUtility.LoadLayout(BoltCore.Paths.windowLayout);
			}
		}
		
		[MenuItem("Tools/Bolt/Save Window Layout... #%Space", priority = ToolsMenuPriority + 402)]
		private static void SaveWindowLayout()
		{
			if (File.Exists(BoltCore.Paths.windowLayout) &&
			    !EditorUtility.DisplayDialog("Bolt Window Layout", "Are you sure you want to overwrite the project's Bolt window layout with the current layout?", "Save", "Cancel"))
			{
				return;
			}

			WindowLayoutUtility.SaveLayout(BoltCore.Paths.windowLayout);
			EditorUtility.DisplayDialog("Bolt Window Layout", "Window layout saved! Use [Ctrl/Cmd]+Space to toggle to it.", "OK");
			WindowLayoutUtility.LoadLayout(BoltCore.Paths.windowLayout);
		}

		[MenuItem("Tools/Bolt/Developer/Prepare for Release...", priority = DeveloperToolsMenuPriority + 101)]
		private static bool PrepareForRelease()
		{
			if (!EditorUtility.DisplayDialog("Delete Generated Files", "This action will delete all generated files, including those containing user data.\n\nAre you sure you want to continue?", "Confirm", "Cancel"))
			{
				return false;
			}
			
			foreach (var plugin in PluginContainer.plugins)
			{
				PathUtility.DeleteDirectoryIfExists(plugin.paths.generatedRoot);
			}
			
			PluginAcknowledgement.GenerateLicenseFile(Path.Combine(instance.rootPath, "LICENSES.txt"));

			return true;
		}

		[MenuItem("Tools/Bolt/Developer/Export Release Package...", priority = DeveloperToolsMenuPriority + 101)]
		private static void ExportReleasePackage()
		{
			if (!PrepareForRelease())
			{
				return;
			}

			var packagePath = EditorUtility.SaveFilePanel("Export Release Package",
														  Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
														  "Bolt_" + instance.version.ToString().Replace(".", "_").Replace(" ", "_"),
														  "unitypackage");

			if (packagePath == null)
			{
				return;
			}

			var packageDirectory = Path.GetDirectoryName(packagePath);

			ProgressUtility.DisplayProgressBar("Exporting Release Package", "Creating Unity Package...", 0);

			var paths = new List<string>();

			foreach (var product in ProductContainer.products)
			{
				paths.Add(product.rootPath);
			}

			foreach (var plugin in PluginContainer.plugins)
			{
				paths.Add(plugin.paths.package);
			}

			AssetDatabase.ExportPackage(paths.Select(PathUtility.FromProject).ToArray(), packagePath, ExportPackageOptions.Recurse);

			ProgressUtility.DisplayProgressBar("Exporting Release Package", "Creating Source Archive...", 0.5f);

			var sourceArchiveFileName = Path.GetFileNameWithoutExtension(packagePath) + "_Source.zip";
			var sourceArchivePath = Path.Combine(packageDirectory, sourceArchiveFileName);
			var sourceDirectory = Directory.GetParent(Directory.GetParent(Paths.project).FullName).FullName;
			var gitArchiveProcess = new ProcessStartInfo();
			gitArchiveProcess.WorkingDirectory = sourceDirectory;
			gitArchiveProcess.FileName = "git";
			gitArchiveProcess.Arguments = $"archive -o \"{sourceArchivePath}\" HEAD";

			try
			{
				Process.Start(gitArchiveProcess).WaitForExit(3000);
			}
			catch (Exception ex)
			{
				UnityEngine.Debug.LogError("Failed to create source archive: \n" + ex);
			}

			ProgressUtility.ClearProgressBar();

			if (EditorUtility.DisplayDialog("Export Release Package", "Release package export complete.\nOpen containing folder?", "Open Folder", "Close"))
			{
				Process.Start(packageDirectory);
			}
		}
	}
}