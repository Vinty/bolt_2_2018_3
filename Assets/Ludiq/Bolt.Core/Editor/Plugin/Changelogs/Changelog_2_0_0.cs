﻿using System;
using System.Collections.Generic;
using Bolt;

namespace Ludiq
{
	[Plugin(BoltCore.ID)]
	internal class Changelog_2_0_0a1 : PluginChangelog
	{
		public Changelog_2_0_0a1(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a1";
		public override DateTime date => new DateTime(2018, 11, 07);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] C# Script Generation for Flow and State Graphs";
				yield return "[Added] Realtime C# Preview Window (Window > Bolt > C# Preview)";
				yield return "[Added] Runtimes: Live, Generated, Hybrid";
				yield return "[Added] Workflow Toolbar (Window > Bolt > Toolbar)";
				yield return "[Added] Extractor Window";
				yield return "[Added] Bulk Type or Assembly Extraction";
				yield return "[Added] Fast (Incremental) Extraction";
				yield return "[Added] XML Documentation Extraction";
				yield return "[Changed] Grouped Window Menu under Bolt";
				yield return "[Optimized] Extraction Speed";
				yield return "[Optimized] Loading Speed of extracted data";
			}
		}
	}
}