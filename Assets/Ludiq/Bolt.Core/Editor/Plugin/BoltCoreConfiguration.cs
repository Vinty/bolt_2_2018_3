﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Ludiq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	public sealed class BoltCoreConfiguration : PluginConfiguration
	{
		private BoltCoreConfiguration(BoltCore plugin) : base(plugin) { }

		public override string header => "Core";



		#region Extraction

		/// <summary>
		/// The types extracted from your codebase.
		/// </summary>
		[ProjectSetting(visible = false, resettable = false)]
		[InspectorReorderable(false)]
		public List<TypeExtraction> typeExtractions { get; private set; } = new List<TypeExtraction>()
		{
			new TypeExtraction(typeof(UnityObject), true),

			typeof(object),
			typeof(bool),
			typeof(int),
			typeof(float),
			typeof(string),
			typeof(Vector2),
			typeof(Vector3),
			typeof(Vector4),
			typeof(Vector2Int),
			typeof(Vector3Int),
			typeof(Quaternion),
			typeof(Matrix4x4),
			typeof(Rect),
			typeof(Bounds),
			typeof(Color),
			typeof(AnimationCurve),
			typeof(LayerMask),
			typeof(Ray),
			typeof(Ray2D),
			typeof(RaycastHit),
			typeof(RaycastHit2D),
			typeof(ContactPoint),
			typeof(ContactPoint2D),
			typeof(ParticleCollisionEvent),
			typeof(Scene),
			
			typeof(Application),
			typeof(Mathf),
			typeof(Debug),
			typeof(Input),
			typeof(Touch),
			typeof(Screen),
			typeof(Cursor),
			typeof(Time),
			typeof(UnityEngine.Random),
			typeof(Physics),
			typeof(Physics2D),
			typeof(SceneManager),
			typeof(GUI),
			typeof(GUILayout),
			typeof(GUIUtility),
			typeof(NavMesh),
			typeof(Gizmos),
			typeof(AnimatorStateInfo),
			new TypeExtraction(typeof(BaseEventData), true),
			
			new TypeExtraction(typeof(ParticleSystem), true),

			typeof(IList),
			typeof(IDictionary),
			typeof(AotList),
			typeof(AotDictionary),

			typeof(Exception),

			new TypeExtraction(typeof(Tween), true),
		};

		/// <summary>
		/// The namespaces extracted from your codebase.
		/// </summary>
		[ProjectSetting(visible = false, resettable = false)]
		[InspectorReorderable(false)]
		public List<NamespaceExtraction> namespaceExtractions { get; private set; } = new List<NamespaceExtraction>()
		{
			"UnityEngine",
		};
		
		/// <summary>
		/// The assemblies extracted from your codebase.
		/// </summary>
		[ProjectSetting(visible = false, resettable = false)]
		[InspectorReorderable(false)]
		public List<AssemblyExtraction> assemblyExtractions { get; private set; } = new List<AssemblyExtraction>();

		/// <summary>
		/// Whether extracted data should be analyzed on start to detect
		/// whether it is out-of-date automatically. 
		/// </summary>
		[EditorPref(visible = false)]
		public bool compareExtractions { get; set; } = false;

		/// <summary>
		/// Whether full extraction should parse available XML documentation files
		/// to show codebase documentation in the editor.
		/// </summary>
		[EditorPref(visible = false)]
		public bool extractDocumentation { get; set; } = true;

		/// <summary>
		/// Whether full extraction should compile the C# projects to generate their
		/// associated XML documentation files.
		/// </summary>
		[EditorPref(visible = false)]
		public bool generateDocumentation { get; set; } = true;

		#endregion
		

		

		/// <summary>
		/// Whether the header help panel should be shown in the  variables window.
		/// </summary>
		[EditorPref]
		public bool showVariablesHelp { get; set; } = true;

		/// <summary>
		/// Whether the scene variables object should be created automatically.
		/// </summary>
		[EditorPref]
		public bool createSceneVariables { get; set; } = true;

		/// <summary>
		/// The runtime mode to be used when executing Bolt graphs.
		/// </summary>
		[EditorPref]
		public RuntimeMode runtimeMode { get; set; } = RuntimeMode.Hybrid;
	}
}