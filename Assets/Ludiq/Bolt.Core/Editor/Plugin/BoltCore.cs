﻿using System.Collections.Generic;
using Ludiq;
using Bolt;

namespace Bolt
{
	[Plugin(ID)]
	[PluginDependency(LudiqCore.ID)]
	[PluginDependency(LudiqGraphs.ID)]
	[Product(BoltProduct.ID)]
	[PluginRuntimeAssembly(ID + ".Runtime")]
	public sealed class BoltCore : Plugin
	{
		public const string ID = "Bolt.Core";
		
		public BoltCore() : base()
		{
			instance = this;
		}

		public static BoltCore instance { get; private set; }

		public static BoltCoreManifest Manifest => (BoltCoreManifest)instance.manifest;
		public static BoltCoreConfiguration Configuration => (BoltCoreConfiguration)instance.configuration;
		public static BoltCoreResources Resources => (BoltCoreResources)instance.resources;
		public static BoltCorePaths Paths => (BoltCorePaths)instance.paths;
		public static BoltCoreResources.Icons Icons => Resources.icons;

		public const string LegacyRuntimeDllGuid = "c8d0ad23af520fe46aabe2b1fecf6462";
		public const string LegacyEditorDllGuid = "7314928a14330c04fb980214791646e9";

		public override IEnumerable<ScriptReferenceReplacement> scriptReferenceReplacements
		{
			get
			{
				yield return ScriptReferenceReplacement.FromDll<Variables>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<VariablesAsset>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<VariablesSaver>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<SceneVariables>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<UnityMessageListener>(LegacyRuntimeDllGuid);
			}
		}

		public override IEnumerable<string> tips
		{
			get
			{
				yield return "Use Fast Extract to only extract types that changed since the last Full Extract.";
			}
		}
	}
}