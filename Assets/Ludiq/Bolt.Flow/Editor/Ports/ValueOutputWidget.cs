﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(ValueOutput))]
	public class ValueOutputWidget : UnitPortWidget<ValueOutput>
	{
		public ValueOutputWidget(FlowCanvas canvas, ValueOutput port) : base(canvas, port)
		{
			color = port.type.Color();
		}

		public override Axis2 axis => Axis2.Horizontal;

		public override Edge edge => Edge.Right;

		protected override Edges proxyEdges => Edges.Top;
		
		protected override bool colorIfActive => !BoltFlow.Configuration.animateControlConnections || !BoltFlow.Configuration.animateValueConnections;

		public override Color color { get; }

		protected override Vector2 handleSize => new Vector2(9, 9);

		protected override EditorTexture handleTextureConnected => BoltFlow.Icons.valuePortConnected;

		protected override EditorTexture handleTextureUnconnected => BoltFlow.Icons.valuePortUnconnected;
	}
}