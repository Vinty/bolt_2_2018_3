﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(IUnitPort))]
	public abstract class UnitPortWidget<TPort> : Widget<FlowCanvas, TPort>, IUnitPortWidget where TPort : class, IUnitPort
	{
		protected UnitPortWidget(FlowCanvas canvas, TPort port) : base(canvas, port) { }



		#region Model

		public TPort port => item;

		public IUnit unit => port.unit;

		// Usually very efficient, but cached because it's used so often
		private IUnitWidget _unitWidget;

		public IUnitWidget unitWidget
		{
			get
			{
				if (_unitWidget == null)
				{
					_unitWidget = canvas.Widget<IUnitWidget>(unit);
				}

				return _unitWidget;
			}
		}

		IUnitPort IUnitPortWidget.port => port;
		
		private new UnitPortDescription description => (UnitPortDescription)base.description;
		
		public Metadata inspectorMetadata { get; private set; }

		protected Inspector inspector { get; private set; }

		public override Metadata FetchMetadata()
		{
			return description.getMetadata(unitWidget.metadata);
		}

		public virtual Metadata FetchInspectorMetadata()
		{
			return null;
		}

		protected override void CacheDescription()
		{
			base.CacheDescription();

			labelContent.text = description.label;

			Reposition();
		}

		protected override void CacheMetadata()
		{
			base.CacheMetadata();

			inspectorMetadata = FetchInspectorMetadata();

			if (inspectorMetadata != null)
			{
				inspector = unitWidget.GetPortInspector(port, inspectorMetadata);
			}
			else
			{
				inspector = null;
			}
		}

		#endregion



		#region Lifecycle

		public override bool foregroundRequiresInput => showInspector;

		public bool wouldDisconnect { get; private set; }

		public bool willDisconnect => wouldDisconnect && isMouseOver;

		protected virtual bool canStartConnection => true;

		public override void HandleInput()
		{
			if (!canvas.isCreatingConnection)
			{
				if (e.IsMouseDown(MouseButton.Left))
				{
					if (canStartConnection)
					{
						StartConnection();
					}

					e.Use();
				}
				else if (e.IsMouseDown(MouseButton.Right))
				{
					wouldDisconnect = true;
				}
				else if (e.IsMouseUp(MouseButton.Right))
				{
					if (isMouseOver)
					{
						Disconnect();
					}

					wouldDisconnect = false;
					e.Use();
				}
			}
			else
			{
				var source = canvas.connectionSource;
				var isSource = source == port;

				if (!isSource && e.IsMouseDown(MouseButton.Left))
				{
					var destination = port;
					canvas.CompleteConnection(source, destination);
					e.Use();
				}
				else if (isSource && e.IsMouseUp(MouseButton.Left))
				{
					IUnitPort destination = null;

					var hovered = canvas.hoveredWidget;

					if (hovered is IUnitPortWidget hoveredPortWidget)
					{
						destination = hoveredPortWidget.port;
					}
					else if (hovered is IUnitWidget hoveredUnitWidget)
					{
						if (hoveredUnitWidget.CanCreateCompatiblePort(source))
						{
							destination = hoveredUnitWidget.CreateCompatiblePort(ref source);
						}
						else
						{
							destination = source.CompatiblePort(hoveredUnitWidget.unit);
						}
					}

					if (destination != null)
					{
						if (destination != source)
						{
							canvas.CompleteConnection(source, destination);
						}
					}
					else
					{
						if (canvas.isMouseOverBackground)
						{
							canvas.NewUnitContextual();
						}
						else if (!canvas.isMouseOver)
						{
							canvas.CancelConnection();
						}
					}
					
					e.Use();

					// HACK: Been debugging this for hours, I can't find why, but HandleRelease
					// never gets called on the widget connecting an Input to an Output node. 
					e.ReleaseMouse();
				}
				else if (isSource && e.IsMouseDrag(MouseButton.Left))
				{
					e.Use();
				}
				else if (isSource && e.IsMouseDown(MouseButton.Right))
				{
					canvas.CancelConnection();
					e.Use();
				}
			}
		}

		private void StartConnection()
		{
			canvas.StartConnection(port);
		}

		private void Disconnect()
		{
			foreach (var connectedPort in port.connectedPorts)
			{
				canvas.Widget(connectedPort.unit).Reposition();
			}

			unitWidget.Reposition();

			port.Disconnect();

			e.Use();

			GUI.changed = true;
		}

		#endregion



		#region Contents

		private readonly GUIContent labelContent = new GUIContent();

		#endregion



		#region Positioning

		public override IEnumerable<IWidget> positionDependencies => ((IWidget)unitWidget).Yield();

		public override IEnumerable<IWidget> positionDependers => port.connections.Select(connection => (IWidget)canvas.Widget(connection));
		
		public virtual Axis2 axis => Axis2.Horizontal;

		public abstract Edge edge { get; }

		public override Rect position
		{
			get => outerPosition;
			set { }
		}
		
		public Rect outerPosition { get; set; }

		private Rect _clippingPosition;

		public override Rect clippingPosition => _clippingPosition;

		public Rect handlePosition { get; private set; }

		public Vector2 dynamicHandleCenter
		{
			get
			{
				canvas.Cache();

				return handlePosition.center.PixelPerfect();
			}
			set
			{
				canvas.Cache();

				var unitToHandle = dynamicHandleCenter - unitWidget.position.position;

				unit.position = value.PixelPerfect() - unitToHandle;
				
				unitWidget.Reposition();

				canvas.Cache();
			}
		}

		public Rect labelPosition { get; private set; }

		public Rect iconPosition { get; private set; }

		public Rect inspectorPosition { get; private set; }

		public Rect identifierPosition { get; private set; }

		public Rect identifierAndHandlePosition { get; private set; }

		public Rect surroundPosition { get; private set; }

		public Vector2 innerSize { get; private set; }

		public Vector2 outerSize { get; private set; }

		public Dictionary<Edge, Rect> proxyPositions { get; } = new Dictionary<Edge, Rect>();

		protected abstract Vector2 handleSize { get; }

		public override Rect hotArea
		{
			get
			{
				if (canvas.isCreatingConnection)
				{
					if (canvas.connectionSource == port || canvas.connectionSource.CanValidlyConnectTo(port))
					{
						return Styles.easierGrabOffset.Add(identifierAndHandlePosition);
					}

					return Rect.zero;
				}

				return Styles.easierGrabOffset.Add(handlePosition);
			}
		}
		
		public override float zIndex
		{
			get { return unitWidget.zIndex + 0.5f; }
			set { }
		}

		private float GetInnerWidth()
		{
			var width = 0f;

			if (showIcon)
			{
				width += Styles.iconSize;
			}

			if (showIcon && showLabel)
			{
				width += Styles.spaceBetweenIconAndLabel;
			}

			if (showIcon && !showLabel && showInspector)
			{
				width += Styles.spaceBetweenIconAndInspector;
			}

			if (showLabel)
			{
				width += GetLabelWidth();
			}

			if (showLabel && showInspector)
			{
				width += Styles.spaceBetweenLabelAndInspector;
			}

			if (showInspector)
			{
				width += GetInspectorWidth();
			}

			return width;
		}

		private float GetInspectorWidth()
		{
			var width = inspector.GetAdaptiveWidth();

			width = Mathf.Min(width, Styles.maxInspectorWidth);

			if (!showLabel)
			{
				//width = Mathf.Max(width, Styles.labellessInspectorMinWidth);
			}

			return width;
		}

		private float GetLabelWidth()
		{
			return Mathf.Min(Styles.label.CalcSize(labelContent).x, Styles.maxLabelWidth);
		}

		public float GetInnerHeight()
		{
			var height = 0f;

			if (axis == Axis2.Horizontal)
			{
				height = Mathf.Max(height, EditorGUIUtility.singleLineHeight);
			}

			if (showIcon)
			{
				height = Mathf.Max(height, Styles.iconSize);
			}

			if (showLabel)
			{
				height = Mathf.Max(height, GetLabelHeight());
			}

			if (showInspector)
			{
				height = Mathf.Max(height, GetInspectorHeight());
			}

			return height;
		}

		private float GetLabelHeight()
		{
			return EditorGUIUtility.singleLineHeight;
		}

		private float GetInspectorHeight()
		{
			var width = GetInspectorWidth();

			using (LudiqGUIUtility.currentInspectorWidth.Override(width))
			{
				return inspector.GetCachedHeight(width, GUIContent.none, null);
			}
		}

		public void CacheSize()
		{
			innerSize = new Vector2(GetInnerWidth(), GetInnerHeight());

			RectOffset padding;

			switch (axis)
			{
				case Axis2.Horizontal:
					padding = Styles.horizontalPadding;
					break;
				case Axis2.Vertical:
					padding = Styles.verticalPadding;
					break;
				default: throw new UnexpectedEnumValueException<Axis2>(axis);
			}

			outerSize = new Vector2
			(
				innerSize.x > 0 ? padding.left + innerSize.x + padding.right : 0,
				innerSize.y > 0 ? padding.top + innerSize.y + padding.bottom : 0
			);
		}

		public override void CachePosition()
		{
			float x, y;
			float xDirection, yDirection;
			bool flipX, flipY;
			
			if (axis == Axis2.Horizontal)
			{
				xDirection = -edge.Normal().x;
				yDirection = -1;
				flipX = xDirection < 0;
				flipY = false;
				x = flipX ? outerPosition.xMax - Styles.horizontalPadding.right : outerPosition.x + Styles.horizontalPadding.left;
				y = Mathf.Floor(outerPosition.y + (outerPosition.height - innerSize.y) / 2);
			}
			else if (axis == Axis2.Vertical)
			{
				xDirection = 1;
				yDirection = -edge.Normal().y;
				flipX = false;
				flipY = yDirection < 0;
				x = Mathf.Floor(outerPosition.x + (outerPosition.width - innerSize.x) / 2);
				y = outerPosition.y + Styles.verticalPadding.top;
			}
			else
			{
				throw new UnexpectedEnumValueException<Axis2>(axis);
			}
			
			identifierPosition = new Rect(x, y, 0, 0);

			if (showIcon)
			{
				var iconPosition = new Rect
				(
					x,
					y,
					Styles.iconSize,
					Styles.iconSize
				).PixelPerfect();

				if (flipX)
				{
					iconPosition.x -= iconPosition.width;
				}

				x += iconPosition.width * xDirection;
				
				identifierPosition = identifierPosition.Encompass(iconPosition);

				this.iconPosition = iconPosition;
			}

			if (showIcon && showLabel)
			{
				x += Styles.spaceBetweenIconAndLabel * xDirection;
			}

			if (showIcon && !showLabel && showInspector)
			{
				x += Styles.spaceBetweenIconAndInspector * xDirection;
			}

			if (showLabel)
			{
				var labelPosition = new Rect
				(
					x,
					y + 1,
					GetLabelWidth(),
					GetLabelHeight()
				);

				if (flipX)
				{
					labelPosition.x -= labelPosition.width;
				}

				x += labelPosition.width * xDirection;
				
				identifierPosition = identifierPosition.Encompass(labelPosition);

				this.labelPosition = labelPosition;
			}

			if (showLabel && showInspector)
			{
				x += Styles.spaceBetweenLabelAndInspector * xDirection;
			}

			if (showInspector)
			{
				var inspectorPosition = new Rect
				(
					x,
					y,
					GetInspectorWidth(),
					GetInspectorHeight()
				);

				if (flipX)
				{
					inspectorPosition.x -= inspectorPosition.width;
				}

				x += inspectorPosition.width * xDirection;

				this.inspectorPosition = inspectorPosition;
			}

			Vector2 handleCenter;
			var handleSize = this.handleSize;
			var handleTextureSize = handleTexture[1].Size();

			if (axis == Axis2.Horizontal)
			{
				var edgeX = unitWidget.position.GetEdgeCenter(edge).x;

				handleCenter = new Vector2
				(
					edgeX + (Styles.spaceBetweenHorizontalEdgeAndHandle + handleSize.x / 2) * -xDirection,
					identifierPosition.y + identifierPosition.height / 2
				);
			}
			else if (axis == Axis2.Vertical)
			{
				var edgeY = unitWidget.position.GetEdgeCenter(edge).y;

				handleCenter = new Vector2
				(
					identifierPosition.x + identifierPosition.width / 2 - 1,
					edgeY + (Styles.spaceBetweenVerticalEdgeAndHandle + handleSize.y / 2) * -yDirection
				);
			}
			else
			{
				throw new UnexpectedEnumValueException<Axis2>(axis);
			}

			handleCenter = handleCenter.PixelPerfect();

			handlePosition = new Rect
			(
				handleCenter.x - handleTextureSize.y / 2,
				handleCenter.y - handleTextureSize.y / 2,
				handleTextureSize.x,
				handleTextureSize.y
			);

			_clippingPosition = outerPosition.Encompass(handlePosition);

			if (identifierPosition.width > 0 && identifierPosition.height > 0)
			{
				identifierAndHandlePosition = identifierPosition.Encompass(handlePosition);
			}
			else
			{
				identifierAndHandlePosition = handlePosition;
			}

			surroundPosition = Styles.surroundPadding.Add(identifierAndHandlePosition);

			foreach (var proxyEdge in Edges.All.Split())
			{
				var normal = proxyEdge.Normal();

				var proxySize = BoltFlow.Icons.unitPortProxies[proxyEdge][1].PointSize();

				proxyPositions[proxyEdge] = new Rect
				(
					handleCenter.x + (normal.x * Styles.spaceBetweenHandleAndProxy) - Mathf.Round(proxySize.x / 2),
					handleCenter.y + (normal.y * Styles.spaceBetweenHandleAndProxy) - Mathf.Round(proxySize.y / 2),
					proxySize.x,
					proxySize.y
				);
			}
		}

		#endregion



		#region Drawing

		public Edges drawnEdges { get; set; }

		protected abstract Edges proxyEdges { get; }

		public bool forceShowLabel { get; set; }

		public override bool canClip => base.canClip && canvas.connectionSource != port;

		protected virtual bool showInspector => false;

		protected virtual bool showIcon => description.icon != null;

		protected virtual bool showLabel => description.showLabel || forceShowLabel;

		public virtual Color color => Color.white;

		protected abstract EditorTexture handleTextureConnected { get; }

		protected abstract EditorTexture handleTextureUnconnected { get; }

		protected EditorTexture handleTexture
		{
			get
			{
				if (port.hasAnyConnection)
				{
					return handleTextureConnected;
				}
				else
				{
					return handleTextureUnconnected;
				}
			}
		}

		protected virtual bool colorIfActive => true;

		protected virtual bool pulseIfActive => !colorIfActive;

		protected override bool dim
		{
			get
			{
				var dim = LudiqGraphs.Configuration.dimInactiveNodes && !unit.Analysis<UnitAnalysis>(context).isEntered;

				if (unitWidget.isMouseOver || unitWidget.isSelected)
				{
					dim = false;
				}

				if (LudiqGraphs.Configuration.dimIncompatibleNodes && canvas.isCreatingConnection)
				{
					dim = canvas.connectionSource != port && !canvas.connectionSource.CanValidlyConnectTo(port);
				}

				return dim;
			}
		}

		public override void DrawBackground() { }

		public override void DrawForeground()
		{
			if (LudiqCore.Configuration.developerMode && LudiqGraphs.Configuration.debug)
			{
				EditorGUI.DrawRect(clippingPosition, new Color(0, 0, 0, 0.1f));
				LudiqGUI.DrawEmptyRect(outerPosition, Color.blue);
			}

			BeginDim();
			
			DrawEdges();

			DrawHandle();

			if (showIcon)
			{
				DrawIcon();
			}

			if (showLabel)
			{
				DrawLabel();
			}

			if (showInspector && canvas.zoom >= FlowCanvas.inspectorZoomThreshold)
			{
				DrawInspector();
			}

			EndDim();
		}

		public override void DrawOverlay()
		{
			base.DrawOverlay();

			var surroundFromPort = canvas.isCreatingConnection &&
			                       isMouseOver &&
			                       canvas.connectionSource.CanValidlyConnectTo(port);

			var surroundFromUnit = canvas.isCreatingConnection &&
			                       unitWidget.isMouseOver &&
								   !unitWidget.CanCreateCompatiblePort(canvas.connectionSource) &&
			                       canvas.connectionSource.CompatiblePort(unit) == port;

			if (surroundFromPort || surroundFromUnit)
			{
				DrawSurround();
			}

			if (canvas.connectionSource == port)
			{
				DrawConnectionSource();
			}
		}

		private void GetConnectionsNoAlloc(HashSet<IUnitConnection> connections)
		{
			connections.Clear();

			var graph = unit.graph;

			// Unit might have been removed from graph, but still drawn this frame.
			if (graph == null)
			{
				return;
			}

			var controlInput = port as ControlInput;
			var controlOutput = port as ControlOutput;
			var valueInput = port as ValueInput;
			var valueOutput = port as ValueOutput;
			var input = port as IUnitInputPort;
			var output = port as IUnitOutputPort;

			if (controlInput != null)
			{
				foreach (var connection in graph.controlConnections.WithDestinationNoAlloc(controlInput))
				{
					connections.Add(connection);
				}
			}

			if (controlOutput != null)
			{
				foreach (var connection in graph.controlConnections.WithSourceNoAlloc(controlOutput))
				{
					connections.Add(connection);
				}
			}

			if (valueInput != null)
			{
				foreach (var connection in graph.valueConnections.WithDestinationNoAlloc(valueInput))
				{
					connections.Add(connection);
				}
			}

			if (valueOutput != null)
			{
				foreach (var connection in graph.valueConnections.WithSourceNoAlloc(valueOutput))
				{
					connections.Add(connection);
				}
			}

			if (input != null)
			{
				foreach (var connection in graph.invalidConnections.WithDestinationNoAlloc(input))
				{
					connections.Add(connection);
				}
			}

			if (output != null)
			{
				foreach (var connection in graph.invalidConnections.WithSourceNoAlloc(output))
				{
					connections.Add(connection);
				}
			}
		}

		private void DrawEdges()
		{
			if (drawnEdges.HasFlag(Edges.Top))
			{
				EditorGUI.DrawRect(new Rect(outerPosition.xMin, outerPosition.yMin, outerPosition.width, 1), Styles.edgeHighlightColor);
			}

			if (drawnEdges.HasFlag(Edges.Left))
			{
				EditorGUI.DrawRect(new Rect(outerPosition.xMin, outerPosition.yMin, 1, outerPosition.height), Styles.edgeHighlightColor);
			}

			if (drawnEdges.HasFlag(Edges.Bottom))
			{
				EditorGUI.DrawRect(new Rect(outerPosition.xMin, outerPosition.yMax - 1, outerPosition.width, 1), Styles.edgeShadowColor);
			}

			if (drawnEdges.HasFlag(Edges.Right))
			{
				EditorGUI.DrawRect(new Rect(outerPosition.xMax - 1, outerPosition.yMin, 1, outerPosition.height), Styles.edgeShadowColor);
			}
		}

		private void DrawHandle()
		{
			// Trying to be very speed / memory efficient in this method

			if (!e.IsRepaint)
			{
				return;
			}

			var color = Color.white;

			var highlight = false;

			var invalid = false;

			var willDisconnect = false;

			var connections = HashSetPool<IUnitConnection>.New();

			GetConnectionsNoAlloc(connections);

			var isConnected = connections.Count > 0;

			var proxy = unit as IUnitPortProxy;
			var isProxied = proxy != null && port == proxy.target;

			if ((e.ctrlOrCmd && isMouseOver && !canvas.isCreatingConnection && proxy == null) || (canvas.connectionSource == port && canvas.connectViaProxy))
			{
				isProxied = true;
			}

			if (isConnected)
			{
				foreach (var connection in connections)
				{
					if (connection is InvalidConnection)
					{
						invalid = true;
					}

					var sourceWidget = canvas.Widget<IUnitPortWidget>(connection.source);
					var destinationWidget = canvas.Widget<IUnitPortWidget>(connection.destination);

					if (sourceWidget.isMouseOver || destinationWidget.isMouseOver)
					{
						highlight = true;
					}

					if (sourceWidget.willDisconnect || destinationWidget.willDisconnect)
					{
						willDisconnect = true;
					}

					if (!isProxied)
					{
						isProxied |= (port == connection.source && connection.destination.unit is IUnitPortProxy destinationProxy && connection.destination == destinationProxy.target) ||
						             (port == connection.destination && connection.source.unit is IUnitPortProxy sourceProxy && connection.source == sourceProxy.target);
					}
				}
			}

			if (isMouseOver)
			{
				highlight = true;
			}

			if (willDisconnect)
			{
				color = UnitConnectionStyles.disconnectColor;
			}
			else if (highlight)
			{
				color = UnitConnectionStyles.highlightColor;
			}
			else if (invalid)
			{
				color = UnitConnectionStyles.invalidColor;
			}
			else if (isConnected)
			{
				Color? resolvedColor = null;

				foreach (var connection in connections)
				{
					var connectionColor = canvas.Widget<IUnitConnectionWidget>(connection).color;

					if (resolvedColor == null)
					{
						resolvedColor = connectionColor;
					}
					else if (resolvedColor != connectionColor)
					{
						resolvedColor = this.color;

						break;
					}
				}

				color = resolvedColor.Value;
			}
			else
			{
				color = this.color;
			}

			float activity = 0;

			if (colorIfActive || (isProxied && pulseIfActive))
			{
				foreach (var connection in connections)
				{
					var connectionDebugData = reference.GetElementDebugData<IUnitConnectionDebugData>(connection);

					activity = Mathf.Max(activity, GraphGUI.GetActivity
					(
						connectionDebugData.lastInvokeFrame,
						connectionDebugData.lastInvokeTime
					));
				}
			}

			if (colorIfActive)
			{
				color = Color.Lerp(UnitConnectionStyles.activeColor, color, activity);
			}

			var handlePosition = this.handlePosition;

			if (highlight)
			{
				var widthExpansion = handlePosition.width * (Styles.highlightScaling - 1);
				var heightExpansion = handlePosition.height * (Styles.highlightScaling - 1);
				handlePosition.width += widthExpansion;
				handlePosition.height += heightExpansion;
				handlePosition.x -= widthExpansion / 2;
				handlePosition.y -= heightExpansion / 2;
			}
			
			var showConnected = highlight ||
			                    isConnected ||
			                    canvas.connectionSource == port ||
			                    canvas.isCreatingConnection && canvas.connectionSource.CanValidlyConnectTo(port);

			using (LudiqGUI.color.Override(color.WithAlphaMultiplied(LudiqGUI.color.value.a)))
			{
				GUI.DrawTexture(handlePosition, (showConnected ? handleTextureConnected : handleTextureUnconnected)[1]);
				
				if (isProxied)
				{
					var pulse = pulseIfActive ? Mathf.Lerp(0, EditorTimeBinding.time % Styles.pulseDuration * (1 / Styles.pulseDuration), activity) : 0;

					using (LudiqGUI.color.Override(Color.Lerp(LudiqGUI.color.value, LudiqGUI.color.value.WithAlpha(0), pulse)))
					{
						foreach (var edge in proxyEdges.Split())
						{
							GUI.DrawTexture(proxyPositions[edge], BoltFlow.Icons.unitPortProxies[edge][1]);
						}
					}
				}
			}

			HashSetPool<IUnitConnection>.Free(connections);
		}

		private void DrawIcon()
		{
			GUI.DrawTexture(iconPosition, description.icon?[Styles.iconSize]);
		}

		private void DrawLabel()
		{
			GUI.Label(labelPosition, description.label, Styles.label);
		}

		private void DrawInspector()
		{
			EditorGUI.BeginChangeCheck();

			using (LudiqGUIUtility.currentInspectorWidth.Override(inspectorPosition.width))
			using (Inspector.adaptiveWidth.Override(true))
			{
				inspector.Draw(inspectorPosition, GUIContent.none);
			}
			
			if (EditorGUI.EndChangeCheck())
			{
				unitWidget.Reposition();
			}
		}

		private void DrawConnectionSource()
		{
			var start = handlePosition.GetEdgeCenter(edge);

			if (window.IsFocused())
			{
				canvas.MoveConnectionEnd();
			}

			GraphGUI.DrawConnection
			(
				color,
				start,
				canvas.connectionEndPosition,
				edge,
				null,
				handleTextureConnected[1],
				handleSize,
				UnitConnectionStyles.relativeBend,
				UnitConnectionStyles.minBend
			);

			if (canvas.connectViaProxy)
			{
				// Kind of hacky here because we're relying on the proxy edges being the same
				// on both sides of the connection, but it just so happens to be the case all the time
				foreach (var proxyEdge in proxyEdges.Split())
				{
					var normal = proxyEdge.Normal();

					var proxyTexture = BoltFlow.Icons.unitPortProxies[proxyEdge][1];

					var proxySize = proxyTexture.PointSize();

					var proxyPosition = new Rect
					(
						canvas.connectionEndPosition.x + (normal.x * Styles.spaceBetweenHandleAndProxy) - Mathf.Round(proxySize.x / 2),
						canvas.connectionEndPosition.y + (normal.y * Styles.spaceBetweenHandleAndProxy) - Mathf.Round(proxySize.y / 2),
						proxySize.x,
						proxySize.y
					);

					LudiqGUI.color.BeginOverride(color);

					GUI.DrawTexture(proxyPosition, proxyTexture);

					LudiqGUI.color.EndOverride();
				}
			}
		}

		private void DrawSurround()
		{
			if (e.controlType == EventType.Repaint)
			{
				Styles.surround.Draw(surroundPosition, false, false, false, false);
			}
		}

		#endregion



		public static class Styles
		{
			static Styles()
			{
				label = new GUIStyle(EditorStyles.label);
				label.wordWrap = false;
				label.imagePosition = ImagePosition.TextOnly;
				label.padding = new RectOffset(0, 0, 0, 0);

				surround = new GUIStyle("LightmapEditorSelectedHighlight");
			}

			public const float highlightScaling = 1f;

			public const float pulseDuration = 0.5f;

			public static readonly RectOffset horizontalPadding = new RectOffset(4, 4, 6, 6);

			public static readonly RectOffset verticalPadding = new RectOffset(8, 8, 4, 4);

			public static readonly Vector2 handleSize = new Vector2(9, 12);
			
			public static readonly float spaceBetweenHorizontalEdgeAndHandle = 8;

			public static readonly float spaceBetweenVerticalEdgeAndHandle = 8;
			
			public static readonly float spaceBetweenIconAndLabel = 5;

			public static readonly float spaceBetweenIconAndInspector = 5;

			public static readonly float spaceBetweenLabelAndInspector = 5;

			public static readonly float labellessInspectorMinWidth = 75;

			public static readonly float maxInspectorWidth = 200;

			public static readonly float maxLabelWidth = 150;

			public static readonly float spaceBetweenHandleAndProxy = 11;

			public static readonly int iconSize = IconSize.Small;

			public static readonly GUIStyle label;

			public static readonly GUIStyle surround;

			public static readonly RectOffset easierGrabOffset = new RectOffset(5, 5, 4, 4);

			public static readonly RectOffset surroundPadding = new RectOffset(3, 3, 2, 2);

			public static readonly SkinnedColor edgeHighlightColor = Color.white.WithAlpha(0.15f);

			public static readonly SkinnedColor edgeShadowColor = Color.black.WithAlpha(0.25f);
		}
	}
}
