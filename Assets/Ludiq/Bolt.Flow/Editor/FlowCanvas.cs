using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[Canvas(typeof(FlowGraph))]
	public sealed class FlowCanvas : Canvas<FlowGraph>
	{
		public FlowCanvas(FlowGraph graph) : base(graph) { }



		#region Clipboard

		public override void ShrinkCopyGroup(HashSet<IGraphElement> copyGroup)
		{
			copyGroup.RemoveWhere(element =>
			{
				if (element is IUnitConnection)
				{
					var connection = (IUnitConnection)element;

					if (!copyGroup.Contains(connection.source.unit) ||
					    !copyGroup.Contains(connection.destination.unit))
					{
						return true;
					}
				}

				return false;
			});
		}

		#endregion



		#region Window

		private static class Contents
		{
			public static readonly GUIContent valuesCommand = new GUIContent("Values", BoltFlow.Icons.valuesCommand[1], "Show Connection Values");
		}

		protected override float GetDisplayToolbarWidth()
		{
			var width = base.GetDisplayToolbarWidth();
			
			// Show Connection Values
			width += LudiqStyles.commandButtonWidth;

			return width;
		}

		protected override void OnDisplayToolbarGUI(bool closeLeft, bool closeRight)
		{
			// Show Connection Values

			EditorGUI.BeginChangeCheck();

			BoltFlow.Configuration.showConnectionValues = GUILayout.Toggle(BoltFlow.Configuration.showConnectionValues, Contents.valuesCommand, LudiqStyles.CommandButtonSoft(closeLeft, false));
			
			if (EditorGUI.EndChangeCheck())
			{
				BoltFlow.Configuration.Save();
			}
			
			base.OnDisplayToolbarGUI(false, closeRight);
		}

		#endregion



		#region View

		protected override bool shouldEdgePan => base.shouldEdgePan || isCreatingConnection;

		public const float inspectorZoomThreshold = 0.7f;

		#endregion



		#region Lifecycle

		public override void Close()
		{
			base.Close();

			CancelConnection();
		}

		protected override void HandleHighPriorityInput()
		{
			if (isCreatingConnection)
			{
				if (e.IsMouseDown(MouseButton.Left))
				{
					MoveConnectionEnd();
					NewUnitContextual();
					e.Use();
				}
				else if (e.IsFree(EventType.KeyDown) && e.keyCode == KeyCode.Escape)
				{
					CancelConnection();
					e.Use();
				}
			}

			base.HandleHighPriorityInput();
		}
		
		protected override void HandleLowPriorityInput()
		{
			base.HandleLowPriorityInput();
			
			HandleElementCreation();
		}

		#endregion


		#region Context

		private void HandleElementCreation()
		{
			if (isMouseOver && e.IsKeyDown(KeyCode.Space) && !e.modified)
			{
				NewUnit(mousePosition, GetNewUnitOptions(UnitOptionFilter.Any));
			}
		}

		protected override void OnContext()
		{
			if (isCreatingConnection)
			{
				CancelConnection();
			}
			else
			{
				// Checking for Alt seems to lose focus, for some reason maybe
				// unrelated to Bolt. Shift or other modifiers seem to work though.
				if (base.GetContextOptions().Any() && (!BoltFlow.Configuration.skipContextMenu || e.shift))
				{
					base.OnContext();
				}
				else
				{
					NewUnit(mousePosition, GetNewUnitOptions(UnitOptionFilter.Any));
				}
			}
		}

		protected override IEnumerable<DropdownOption> GetContextOptions()
		{
			yield return new DropdownOption((Action<Vector2>)(NewUnit), "Add Unit...");

			foreach (var baseOption in base.GetContextOptions())
			{
				yield return baseOption;
			}
		}

		public void AddUnit(IUnit unit, Vector2 position)
		{
			UndoUtility.RecordEditedObject("Create Unit");
			unit.guid = Guid.NewGuid();
			unit.position = position.PixelPerfect();
			graph.units.Add(unit);
			selection.Select(unit);
			GUI.changed = true;
		}

		private UnitOptionTree GetNewUnitOptions(UnitOptionFilter filter)
		{
			var options = new UnitOptionTree(new GUIContent("Unit"));

			options.connectionSource = connectionSource;
			options.filter = filter;
			options.reference = reference;

			if (filter.CompatibleOutputType == typeof(object))
			{
				options.surfaceCommonTypeLiterals = true;
			}

			return options;
		}

		private void NewUnit(Vector2 position)
		{
			NewUnit(position, GetNewUnitOptions(UnitOptionFilter.Any));
		}

		private void NewUnit(Vector2 position, UnitOptionTree options, Action<IUnit> then = null)
		{
			delayCall += () =>
			{
				var activatorPosition = new Rect(e.mousePosition, new Vector2(200, 1));

				var context = this.context;

				LudiqGUI.FuzzyDropdown
				(
					activatorPosition,
					options,
					null,
					delegate(object _option)
					{
						context.BeginEdit();

						var option = (IUnitOption)_option;
						var unit = option.InstantiateUnit();
						AddUnit(unit, position);
						option.PreconfigureUnit(unit);
						then?.Invoke(unit);
						GUI.changed = true;

						context.EndEdit();
					}
				);
			};
		}
		
		private void CompleteContextualConnection(IUnitPort source, IUnitPort destination, Vector2 portPosition, IUnitPort aligned = null)
		{
			source.ValidlyConnectTo(destination);
			
			var alignedWidget = this.Widget<IUnitPortWidget>(aligned ?? destination);
			alignedWidget.dynamicHandleCenter = portPosition;

			CancelConnection();
			GUI.changed = true;
		}
		
		public void NewUnitContextual()
		{
			var position = connectionEndPosition;

			if (connectViaProxy)
			{
				var proxy = connectionSource.CreateProxy();

				AddUnit(proxy, mousePosition);

				CompleteContextualConnection(connectionSource, proxy.target, position, proxy.relay);
				
				StartConnection(proxy.relay);
			}
			else
			{
				var filter = UnitOptionFilter.Any;

				if (connectionSource is ValueInput valueInput)
				{
					filter.CompatibleOutputType = valueInput.type;
					filter.Expose = false;
					NewUnit(position, GetNewUnitOptions(filter), (unit) => CompleteContextualConnection(valueInput, unit.CompatibleValueOutput(valueInput.type), position));
				}
				else if (connectionSource is ValueOutput valueOutput)
				{
					filter.CompatibleInputType = valueOutput.type;
					NewUnit(position, GetNewUnitOptions(filter), (unit) => CompleteContextualConnection(valueOutput, unit.CompatibleValueInput(valueOutput.type), position));
				}
				else if (connectionSource is ControlInput controlInput)
				{
					filter.NoControlOutput = false;
					NewUnit(position, GetNewUnitOptions(filter), (unit) => CompleteContextualConnection(controlInput, unit.controlOutputs.First(), position));
				}
				else if (connectionSource is ControlOutput controlOutput)
				{
					filter.NoControlInput = false;
					NewUnit(position, GetNewUnitOptions(filter), (unit) => CompleteContextualConnection(controlOutput, unit.controlInputs.First(), position));
				}
			}
		}

		#endregion



		#region Drag & Drop

		private bool CanDetermineDraggedInput(UnityObject uo)
		{
			if (uo.IsSceneBound())
			{
				if (reference.self == uo.GameObject())
				{
					// Because we'll be able to assign it to Self
					return true;
				}

				if (reference.serializedObject.IsSceneBound())
				{
					// Because we'll be able to use a direct scene reference
					return true;
				}

				return false;
			}
			else
			{
				return true;
			}
		}

		public override bool AcceptsDragAndDrop()
		{
			return DragAndDropUtility.Is<FlowMacro>() ||
				   (DragAndDropUtility.Is<UnityObject>() && !DragAndDropUtility.Is<IMacro>() && CanDetermineDraggedInput(DragAndDropUtility.Get<UnityObject>())) ||
				   EditorVariablesUtility.isDraggingVariable;
		}

		public override void PerformDragAndDrop()
		{
			if (DragAndDropUtility.Is<FlowMacro>())
			{
				var flowMacro = DragAndDropUtility.Get<FlowMacro>();
				var superUnit = new SuperUnit(flowMacro);
				AddUnit(superUnit, DragAndDropUtility.position);
			}
			else if (DragAndDropUtility.Is<UnityObject>())
			{
				var uo = DragAndDropUtility.Get<UnityObject>();
				var type = uo.GetType();
				var filter = UnitOptionFilter.Any;
				filter.Literals = false;
				filter.Expose = false;
				var options = GetNewUnitOptions(filter);

				var root = new List<IFuzzyOption>();
				
				if (!uo.IsSceneBound() || reference.serializedObject.IsSceneBound())
				{
					if (uo == reference.self)
					{
						root.Add(new UnitOption<Self>(new Self()));
					}

					root.Add(new PresetLiteralOption(new Literal(type, uo)));
				}
				
				if (uo is MonoScript script)
				{
					var scriptType = script.GetClass();

					if (scriptType != null)
					{
						root.Add(new TypeOption(scriptType, FuzzyOptionMode.Branch));
					}
				}
				else
				{
					root.Add(new TypeOption(type, FuzzyOptionMode.Branch));
				}

				if (uo is GameObject)
				{
					root.AddRange(uo.GetComponents<Component>().Select(c => c.GetType()));
				}

				options.rootOverride = root.ToArray();

				NewUnit(DragAndDropUtility.position, options, (unit) =>
				{
					// Try to assign a correct input
					var compatibleInput = unit.CompatibleValueInput(type);

					if (compatibleInput == null)
					{
						return;
					}

					if (uo.IsSceneBound())
					{
						if (reference.self == uo.GameObject())
						{
							// The component is owned by the same game object as the graph.

							if (compatibleInput.nullMeansSelf)
							{
								compatibleInput.SetDefaultValue(null);
							}
							else
							{
								var self = new Self();
								self.position = unit.position + new Vector2(-150, 19);
								graph.units.Add(self);
								self.self.ConnectToValid(compatibleInput);
							}
						}
						else if (reference.serializedObject.IsSceneBound())
						{
							// The component is from another object from the same scene
							compatibleInput.SetDefaultValue(uo.ConvertTo(compatibleInput.type));
						}
						else
						{
							throw new NotSupportedException("Cannot determine compatible input from dragged Unity object.");
						}
					}
					else
					{
						compatibleInput.SetDefaultValue(uo.ConvertTo(compatibleInput.type));
					}
				});
			}
			else if (EditorVariablesUtility.isDraggingVariable)
			{
				var kind = EditorVariablesUtility.kind;
				var declaration = EditorVariablesUtility.declaration;

				UnifiedVariableUnit unit;

				if (e.alt)
				{
					unit = new SetVariable();
				}
				else if (e.shift)
				{
					unit = new IsVariableDefined();
				}
				else
				{
					unit = new GetVariable();
				}

				unit.kind = kind;
				AddUnit(unit, DragAndDropUtility.position);
				unit.name.SetDefaultValue(declaration.name);
			}
		}

		public override void DrawDragAndDropPreview()
		{
			var tooltipPosition = DragAndDropUtility.position + GraphGUI.Styles.tooltipCursorOffset;

			if (DragAndDropUtility.Is<FlowMacro>())
			{
				GraphGUI.DrawTooltip(tooltipPosition, DragAndDropUtility.Get<FlowMacro>().name, typeof(FlowMacro).Icon());
			}
			else if (DragAndDropUtility.Is<GameObject>())
			{
				var gameObject = DragAndDropUtility.Get<GameObject>();
				GraphGUI.DrawTooltip(tooltipPosition, gameObject.name + "...", gameObject.Icon());
			}
			else if (DragAndDropUtility.Is<UnityObject>())
			{
				var obj = DragAndDropUtility.Get<UnityObject>();
				var type = obj.GetType();
				GraphGUI.DrawTooltip(tooltipPosition, type.HumanName() + "...", type.Icon());
			}
			else if (EditorVariablesUtility.isDraggingVariable)
			{
				var kind = EditorVariablesUtility.kind;
				var name = EditorVariablesUtility.declaration.name;

				string label;

				if (e.alt)
				{
					label = $"Set {name}";
				}
				else if (e.shift)
				{
					label = $"Check if {name} is defined";
				}
				else
				{
					label = $"Get {name}";
				}

				GraphGUI.DrawTooltip(tooltipPosition, label, BoltCore.Icons.VariableKind(kind));
			}
		}

		#endregion
		


		#region Connection Creation
		
		public IUnitPort connectionSource { get; private set; }

		public bool allowProxyConnection => !(connectionSource.unit is IUnitPortProxy);

		public bool connectViaProxy => allowProxyConnection && e.ctrlOrCmd;

		public Vector2 connectionEndPosition { get; private set; }

		public bool isCreatingConnection => connectionSource != null;

		public void StartConnection(IUnitPort connectionSource)
		{
			this.connectionSource = connectionSource;
			window.Focus();
		}

		public void CancelConnection()
		{
			connectionSource = null;
		}

		public void CompleteConnection(IUnitPort source, IUnitPort destination)
		{
			if (source.CanValidlyConnectTo(destination))
			{
				UndoUtility.RecordEditedObject("Connect Units");

				if (connectViaProxy)
				{
					ConnectViaProxy(source, destination);
				}
				else
				{
					source.ValidlyConnectTo(destination);
				}

				this.Widget(source.unit).Reposition();
				this.Widget(destination.unit).Reposition();

				CancelConnection();

				GUI.changed = true;
			}
			else
			{
				Debug.LogWarningFormat
				(
					"Cannot connect this {0} to this {1}.\n",
					source.GetType().HumanName().ToLower(),
					destination.GetType().HumanName().ToLower()
				);
			}
		}

		public void MoveConnectionEnd()
		{
			connectionEndPosition = mousePosition;

			var connectionSourceWidget = this.Widget<IUnitPortWidget>(connectionSource);
			var connectionSourcePosition = connectionSourceWidget.handlePosition.center;
			var connectionAxis = connectionSourceWidget.axis.Perpendicular();;

			if (SnappingUtility.Distance(connectionSourcePosition, connectionEndPosition, connectionAxis) < LudiqGraphs.Configuration.snapThreshold)
			{
				connectionEndPosition = connectionEndPosition.WithComponent(connectionSourcePosition.Component(connectionAxis), connectionAxis);
			}
		}
		
		private void ConnectViaProxy(IUnitPort source, IUnitPort destination)
		{
			// It's more logical to always proxy the output, regardless of the source/dest order
			var input = (source as IUnitInputPort) ?? (destination as IUnitInputPort);
			var output = (source as IUnitOutputPort) ?? (destination as IUnitOutputPort);
			source = output;
			destination = input;

			var proxy = source.CreateProxy();
			proxy.guid = Guid.NewGuid();
			graph.elements.Add(proxy);
					
			source.ValidlyConnectTo(proxy.target);
			proxy.relay.ValidlyConnectTo(destination);

			var proxyRelayWidget = this.Widget<IUnitPortWidget>(proxy.relay);
			var destinationWidget = this.Widget<IUnitPortWidget>(destination);
			proxyRelayWidget.dynamicHandleCenter = destinationWidget.dynamicHandleCenter + destinationWidget.edge.Normal() * Styles.proxyConnectionSpacing;
		}
		
		private void ReplaceConnectionByProxy(IUnitConnection connection)
		{
			if (connection.source.unit is IUnitPortProxy ||
			    connection.destination.unit is IUnitPortProxy)
			{
				return;
			}

			ConnectViaProxy(connection.source, connection.destination);
			graph.elements.Remove(connection);
		}

		#endregion


		#region Collapsing

		public override bool CanCollapse(IEnumerable<IGraphElement> elements)
		{
			return elements.Any(e => e is IUnit || e is GraphGroup);
		}

		protected override void FilterCollapseGroup(HashSet<IGraphElement> @group)
		{
			base.FilterCollapseGroup(@group);

			group.RemoveWhere(e => e is IUnitConnection);
		}
		
		public override void CollapseToEmbed(CollapseRequest request)
		{
			var graph = CollapseToGraph(request, out var ioConnections);
			var superUnit = new SuperUnit();
			superUnit.nest.SwitchToEmbed(graph);
			AddCollapsedUnit(request, superUnit, ioConnections);
		}

		public override IMacro CollapseToMacro(CollapseRequest request)
		{
			var graph = CollapseToGraph(request, out var ioConnections);
			var macro = ScriptableObject.CreateInstance<FlowMacro>();
			macro.graph = graph;
			var superUnit = new SuperUnit();
			superUnit.nest.SwitchToMacro(macro);
			AddCollapsedUnit(request, superUnit, ioConnections);
			return macro;
		}

		private FlowGraph CollapseToGraph(CollapseRequest request, out List<(IUnitPort, string)> externalIoConnections)
		{
			var defaultControlAxis = BoltFlow.Configuration.defaultControlAxis;
			BoltFlow.Configuration.defaultControlAxis = this.graph.controlAxis;
			var graph = FlowGraphFactory.WithInputOutput(out var input, out var output);
			BoltFlow.Configuration.defaultControlAxis = defaultControlAxis;
			graph.title = request.title;
			graph.summary = request.summary;

			var externalElements = request.elements;
			var externalElementsToInclude = new HashSet<IGraphElement>(externalElements);
			var ioDefinitionsByPort = new Dictionary<IUnitPort, IUnitPortDefinition>();
			var internalIoConnections = new List<(UnitPortReference, UnitPortReference)>();
			externalIoConnections = new List<(IUnitPort, string)>();

			foreach (var element in externalElements)
			{
				if (element is IUnit unit)
				{
					foreach (var _port in unit.ports)
					{
						var port = _port;

						foreach (var connectedPortByConnection in port.ConnectedPortsByConnection())
						{
							var portConnection = connectedPortByConnection.Key;
							var connectedPort = connectedPortByConnection.Value;
							var connectedUnit = connectedPort.unit;

							if (externalElements.Contains(connectedUnit))
							{
								externalElementsToInclude.Add(portConnection);
							}
							else
							{
								IUnitPortDefinition definition;

								if (!ioDefinitionsByPort.TryGetValue(port, out definition) &&
								    !ioDefinitionsByPort.TryGetValue(connectedPort, out definition))
								{
									definition = graph.AddCompatibleDefinition(ref port);
									ioDefinitionsByPort.Add(port, definition);
									ioDefinitionsByPort.Add(connectedPort, definition);
								}

								externalIoConnections.Add((connectedPort, definition.key));

								IUnit ioUnit;

								if (definition is IUnitInputPortDefinition)
								{
									ioUnit = input;
								}
								else if (definition is IUnitOutputPortDefinition)
								{
									ioUnit = output;
								}
								else
								{
									throw new InvalidImplementationException();
								}

								var ioPort = ioUnit.ports.Single(p => p.key == definition.key);

								internalIoConnections.Add((port.ToReference(), ioPort.ToReference()));
							}
						}
					}
				}
			}

			if (graph.controlInputDefinitions.Count == 1)
			{
				var enterDefinition = graph.controlInputDefinitions[0];
				enterDefinition.primary = true;
				enterDefinition.hideLabel = true;
			}

			if (graph.controlOutputDefinitions.Count == 1)
			{
				var exitDefinition = graph.controlOutputDefinitions[0];
				exitDefinition.primary = true;
				exitDefinition.hideLabel = true;
			}

			/*if (graph.valueInputDefinitions.Count == 1)
			{
				var inputDefinition = graph.valueInputDefinitions[0];
				inputDefinition.primary = true;
				inputDefinition.hideLabel = true;
			}

			if (graph.valueOutputDefinitions.Count == 1)
			{
				var outputDefinition = graph.valueOutputDefinitions[0];
				outputDefinition.primary = true;
				outputDefinition.hideLabel = true;
			}*/
			
			var internalElements = externalElementsToInclude.CloneViaSerializationPolicy().ToList();
			
			graph.elements.AddRangeByDependencies(internalElements);
			
			foreach (var ioConnection in internalIoConnections)
			{
				var port = ioConnection.Item1.FindPortEquivalent(graph);
				var ioPort = ioConnection.Item2.FindPortEquivalent(graph);

				port.ValidlyConnectTo(ioPort);
			}

			return graph;
		}

		private void AddCollapsedUnit(CollapseRequest request, SuperUnit superUnit, List<(IUnitPort, string)> externalIoConnectionKeys)
		{
			var area = request.masterGroup != null ? request.masterGroup.position : GraphGUI.CalculateArea(request.elements.Select(this.Widget));

			AddUnit(superUnit, area.center);
			
			foreach (var ioKeyByPort in externalIoConnectionKeys)
			{
				var port = ioKeyByPort.Item1;
				var ioKey = ioKeyByPort.Item2;
				var ioPort = superUnit.ports.Single(p => p.key == ioKey);
				port.ValidlyConnectTo(ioPort);
			}

			foreach (var element in request.elements)
			{
				graph.elements.Remove(element);
			}

			if (request.masterGroup != null)
			{
				graph.elements.Remove(request.masterGroup);
			}
			
			Cache();
			var superUnitWidget = this.Widget(superUnit);
			var superUnitPosition = superUnitWidget.position;
			superUnitPosition.x = area.x + (area.width - superUnitPosition.width) / 2;
			superUnitPosition.y = area.y + (area.height - superUnitPosition.height) / 2;
			superUnitWidget.position = superUnitPosition;
			superUnitWidget.Reposition();

			var internalReference = reference.ChildReference(superUnit, true);
			var internalContext = internalReference.Context<FlowGraphContext>();
			var internalCanvas = internalContext.canvas;
			var internalGraph = internalContext.graph;

			internalContext.BeginEdit();
			{
				internalCanvas.Cache();

				foreach (var widget in internalCanvas.elementWidgets)
				{
					widget.position = new Rect(widget.position.position - area.center, widget.position.size);
					widget.Reposition();
				}
				
				internalCanvas.CacheWidgetPositions();

				var input = internalGraph.units.OfType<GraphInput>().Single();
				var output = internalGraph.units.OfType<GraphOutput>().Single();
				var inputWidget = internalCanvas.Widget(input);
				var outputWidget = internalCanvas.Widget(output);

				var verticalIO = internalGraph.controlAxis == Axis2.Vertical && 
				                 (!internalGraph.definitions.Any() || internalGraph.definitions.OfType<IUnitControlPortDefinition>().Any());
				
				var centeredArea = new Rect(area.position - area.center, area.size);

				var ioPadding = 96;

				if (verticalIO)
				{
					float inputCenterX = centeredArea.center.x;
					float outputCenterX = centeredArea.center.x;

					if (input.controlOutputs.Count == 1)
					{
						var connectedPort = input.controlOutputs[0].connection.destination;
						var connectedPortWidget = internalCanvas.Widget<IUnitPortWidget>(connectedPort);
						inputCenterX = connectedPortWidget.handlePosition.center.x;
					}

					if (output.controlInputs.Count == 1 && output.controlInputs[0].connections.Count() == 1)
					{
						var connectedPort = output.controlInputs[0].connections.Single().source;
						var connectedPortWidget = internalCanvas.Widget<IUnitPortWidget>(connectedPort);
						outputCenterX = connectedPortWidget.handlePosition.center.x;
					}

					inputWidget.position = new Rect
					(
						inputCenterX - (inputWidget.position.width / 2),
						centeredArea.yMin - inputWidget.position.height - ioPadding,
						inputWidget.position.width,
						inputWidget.position.height
					);

					outputWidget.position = new Rect
					(
						outputCenterX - (outputWidget.position.width / 2),
						centeredArea.yMax + ioPadding,
						outputWidget.position.width,
						outputWidget.position.height
					);
				}
				else
				{
					inputWidget.position = new Rect
					(
						centeredArea.xMin - inputWidget.position.width - ioPadding,
						centeredArea.center.y - (inputWidget.position.height / 2),
						inputWidget.position.width,
						inputWidget.position.height
					);

					outputWidget.position = new Rect
					(
						centeredArea.xMax + ioPadding,
						centeredArea.center.y - (outputWidget.position.height / 2),
						outputWidget.position.width,
						outputWidget.position.height
					);
				}

				inputWidget.Reposition();
				outputWidget.Reposition();
				
				internalCanvas.CacheWidgetPositions();

				if (verticalIO)
				{
					var internalIoConnections = new HashSet<IUnitConnection>();

					foreach (var port in input.ports)
					{
						internalIoConnections.UnionWith(port.connections);
					}

					foreach (var port in output.ports)
					{
						internalIoConnections.UnionWith(port.connections);
					}

					foreach (var ioConnection in internalIoConnections)
					{
						if (ioConnection is ValueConnection)
						{
							internalCanvas.ReplaceConnectionByProxy(ioConnection);
						}
					}
				}
				
				internalCanvas.Cache();
			}
			internalContext.EndEdit();
		}

		#endregion




		public static class Styles
		{
			public static readonly float proxyConnectionSpacing = 26;
		}
	}
}