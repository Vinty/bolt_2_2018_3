﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public interface IUnitOption : IFuzzyOption
	{
		UnitOptionData data { get; set; }

		IUnit unit { get; }
		
		Type sourceType { get; }

		IUnit InstantiateUnit();
		void PreconfigureUnit(IUnit unit);
		
		int order { get; }
		UnitCategory category { get; }
		string key { get; }
		bool favoritable { get; }
		Type unitType { get; }

		#region Serialization

		void Deserialize(UnitOptionData data);
		UnitOptionData Serialize();

		#endregion
		
		#region Filtering

		int controlInputCount { get; }
		int controlOutputCount { get; }
		HashSet<Type> valueInputTypes { get; }
		HashSet<Type> valueOutputTypes { get; }

		#endregion
	}

	public static class XUnitOption
	{
		public static bool UnitIs(this IUnitOption option, Type type)
		{
			return type.IsAssignableFrom(option.unitType);
		}

		public static bool UnitIs<T>(this IUnitOption option)
		{
			return option.UnitIs(typeof(T));
		}

		public static bool HasCompatibleValueInput(this IUnitOption option, Type outputType)
		{
			Ensure.That(nameof(outputType)).IsNotNull(outputType);

			foreach (var valueInputType in option.valueInputTypes)
			{
				if (ConversionUtility.CanConvert(outputType, valueInputType, false))
				{
					return true;
				}
			}

			return false;
		}

		public static bool HasCompatibleValueOutput(this IUnitOption option, Type inputType)
		{
			Ensure.That(nameof(inputType)).IsNotNull(inputType);

			foreach (var valueOutputType in option.valueOutputTypes)
			{
				if (ConversionUtility.CanConvert(valueOutputType, inputType, false))
				{
					return true;
				}
			}

			return false;
		}
	}
}
