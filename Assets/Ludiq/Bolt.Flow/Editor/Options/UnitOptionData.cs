﻿using System;
using Ludiq;

namespace Bolt
{
	public sealed class UnitOptionData
	{
		public string optionType;
		public string unitType;
		public string labelHuman;
		public string labelProgrammer;
		public string category;
		public int order;
		public string haystackHuman;
		public string haystackProgrammer;
		public string key;
		public string tag1;
		public string tag2;
		public string tag3;
		public byte[] unit;
		public int controlInputCount;
		public int controlOutputCount;
		public string[] valueInputTypes;
		public string[] valueOutputTypes;
		
		public IUnitOption ToOption()
		{
			using (ProfilingUtility.SampleBlock("Unit Data to Option"))
			{
				var optionType = Codebase.DeserializeRootType(this.optionType);

				IUnitOption option;

				option = (IUnitOption)Activator.CreateInstance(optionType);

				using (ProfilingUtility.SampleBlock("Option Deserialize"))
				{
					option.Deserialize(this);
				}

				return option;
			}
		}
	}
}