﻿using System;
using System.Linq;

namespace Bolt
{
	public struct UnitPortReference
	{
		public readonly Guid unitGuid;

		public readonly string portKey;

		public readonly FlowGraph graph;

		public UnitPortReference(IUnitPort port)
		{
			unitGuid = port.unit.guid;
			portKey = port.key;
			graph = (FlowGraph)port.graph;
		}

		public IUnitPort FindPort()
		{
			return FindPortEquivalent(graph);
		}

		public IUnitPort FindPortEquivalent(FlowGraph graph)
		{
			var portKey = this.portKey;
			return graph.units[unitGuid].ports.Single(p => p.key == portKey);
		}
	}

	public static class XUnitPortReference
	{
		public static UnitPortReference ToReference(this IUnitPort port)
		{
			return new UnitPortReference(port);
		}
	}
}