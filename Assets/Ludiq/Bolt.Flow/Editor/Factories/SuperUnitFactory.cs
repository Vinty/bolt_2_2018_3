﻿using Ludiq;

namespace Bolt
{
	[GraphFactory(typeof(SuperUnit))]
	public class SuperUnitFactory : GraphFactory<SuperUnit, FlowGraph>
	{
		public SuperUnitFactory(SuperUnit parent) : base(parent) { }

		public override FlowGraph DefaultGraph()
		{
			return FlowGraphFactory.WithInputOutput();
		}

		public static SuperUnit WithInputOutput()
		{
			var superUnit = new SuperUnit();
			superUnit.nest.source = GraphSource.Embed;
			superUnit.nest.embed = FlowGraphFactory.WithInputOutput();
			return superUnit;
		}

		public static SuperUnit WithStartUpdate()
		{
			var superUnit = new SuperUnit();
			superUnit.nest.source = GraphSource.Embed;
			superUnit.nest.embed = FlowGraphFactory.WithStartUpdate();
			return superUnit;
		}
	}
}
