﻿using Ludiq;

namespace Bolt
{
	[GraphFactory(typeof(FlowMachine))]
	public class FlowMachineFactory : GraphFactory<FlowMachine, FlowGraph>
	{
		public FlowMachineFactory(FlowMachine parent) : base(parent) { }

		public override FlowGraph DefaultGraph()
		{
			return FlowGraphFactory.WithStartUpdate();
		}
	}
}
