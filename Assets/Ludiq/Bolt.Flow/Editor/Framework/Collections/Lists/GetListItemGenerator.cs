﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(GetListItem))]
	public class GetListItemGenerator : UnitGenerator<GetListItem>
	{
		public GetListItemGenerator(GetListItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.item)
			{
				return unit.list.GenerateExpression(context, typeof(IList)).Index(unit.index.GenerateExpression(context, typeof(int)));
			}
			throw new NotImplementedException();
		}
	}
}
