﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Bolt
{
	[Generator(typeof(LastItem))]
	public class LastItemGenerator : UnitGenerator<LastItem>
	{
		public LastItemGenerator(LastItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			var sourceType = unit.collection.GetSourceType();
			if (typeof(IList).IsAssignableFrom(sourceType))
			{
				var listLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "list", unit.collection.GenerateExpression(context, typeof(IList)));
				var lastItemLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.lastItem,
					CodeFactory.VarRef("list").Index(CodeFactory.VarRef("list").Field("Count").Subtract(CodeFactory.Primitive(1))));

				yield return listLocal;
				yield return lastItemLocal;
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.lastItem)
			{
				if (context.currentScope.TryGetValuePortLocal(valueOutput, out var lastItemLocal))
				{
					return CodeFactory.VarRef(lastItemLocal);
				}
				else
				{
					return unit.collection.GenerateExpression(context, typeof(IEnumerable)).Method("Last").Invoke();
				}
			}
			throw new NotImplementedException();
		}
	}
}
