﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Bolt
{
	[Generator(typeof(CountItems))]
	public class CountItemsGenerator : UnitGenerator<CountItems>
	{
		public CountItemsGenerator(CountItems unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.count)
			{
				var sourceType = unit.collection.GetSourceType();
				if (typeof(ICollection).IsAssignableFrom(sourceType))
				{
					return unit.collection.GenerateExpression(context, typeof(ICollection)).Field("Count");
				}
				else
				{
					return unit.collection.GenerateExpression(context, typeof(IEnumerable)).Method("Cast", CodeFactory.ObjectType).Invoke().Method("Count").Invoke();
				}
			}
			throw new NotImplementedException();
		}
	}
}
