﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(MergeDictionaries))]
	public class MergeDictionariesGenerator : UnitGenerator<MergeDictionaries>
	{
		public MergeDictionariesGenerator(MergeDictionaries unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.dictionary)
			{
				var inputExpressions = new List<CodeExpression>();
				foreach (var input in unit.multiInputs)
				{
					inputExpressions.Add(input.GenerateExpression(context, typeof(IDictionary)));
				}

				return CodeFactory.ObjectCreate(CodeFactory.TypeRef(typeof(List<object>))).Method("Merge").Invoke(inputExpressions);
			}
			throw new NotImplementedException();
		}
	}
}
