﻿using Ludiq;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[FuzzyOption(typeof(IUnitPortProxy))]
	public class UnitPortProxyOption : UnitOption<IUnitPortProxy>
	{
		public UnitPortProxyOption() : base() { }
		
		public UnitPortProxyOption(IUnitPortProxy unit) : base(unit)
		{

		}
		
		protected override string Label(bool human)
		{
			return "Create Proxy";
		}
	}
}