﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(Literal))]
	class LiteralGenerator : UnitGenerator<Literal>
	{
		enum LiteralKind
		{
			NullLiteral,
			ConstantExpression,
			FieldMember,
		}

		private LiteralKind literalKind;
		private IConstantGenerator constantGenerator = null;
		private string fieldMemberOriginalName = null;

		public LiteralGenerator(Literal unit) : base(unit)
		{
			if (unit.value == null)
			{
				literalKind = LiteralKind.NullLiteral;
			}
			else if (unit.value.TryGetGenerator(out constantGenerator))
			{
				literalKind = LiteralKind.ConstantExpression;
			}
			else
			{
				literalKind = LiteralKind.FieldMember;
				fieldMemberOriginalName = unit.value.GetType().NameWithoutGenericArity().FirstCharacterToLower();
			}
		}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			if (literalKind == LiteralKind.FieldMember)
			{
				context.DeclareMemberName(unit, fieldMemberOriginalName);
			}
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			if (literalKind == LiteralKind.FieldMember)
			{
				yield return new CodeFieldMember(CodeMemberModifiers.Public, CodeFactory.TypeRef(unit.value.GetType()), context.ExpectMemberName(unit, fieldMemberOriginalName));
			}
		}

		public override IEnumerable<CodeStatement> GenerateDeserializerStatements(FlowMethodGenerationContext context)
		{
			if (literalKind == LiteralKind.FieldMember)
			{
				yield return context.InstanceField(unit, fieldMemberOriginalName).Assign(
					CodeFactory.VarRef("value").Field("definition").Field("units")
						.Index(CodeFactory.TypeRef(typeof(Guid)).ObjectCreate(CodeFactory.Primitive(unit.guid.ToString())))
						.Cast(CodeFactory.TypeRef(typeof(Literal)))
						.Field("value")
						.Cast(CodeFactory.TypeRef(unit.value.GetType()))
				).Statement();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{				
				switch (literalKind)
				{
					case LiteralKind.NullLiteral: return CodeFactory.Primitive(null);
					case LiteralKind.ConstantExpression: return constantGenerator.GenerateExpression();
					case LiteralKind.FieldMember: return context.InstanceField(unit, fieldMemberOriginalName);
					default: throw new UnexpectedEnumValueException<LiteralKind>(literalKind);
				}
			}
			throw new NotImplementedException();
		}
	}
}