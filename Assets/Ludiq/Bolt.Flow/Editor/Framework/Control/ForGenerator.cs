﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(For))]
	class ForGenerator : UnitGenerator<For>
	{
		public ForGenerator(For unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var firstIndexExpression = unit.firstIndex.GenerateExpression(context, typeof(int));
				var lastIndexExpression = unit.lastIndex.GenerateExpression(context, typeof(int));
				var stepExpression = unit.step.GenerateExpression(context, typeof(int));
				var bodyStatements = unit.body.GenerateScopedStatements(context);

				if (firstIndexExpression is CodePrimitiveExpression firstIndexPrimitive
				&& lastIndexExpression is CodePrimitiveExpression lastIndexPrimitive)
				{
					var currentIndexLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.IntType, unit.currentIndex, firstIndexExpression);

					var conditional = (int) firstIndexPrimitive.Value <= (int) lastIndexPrimitive.Value
						? CodeFactory.VarRef(currentIndexLocal.Name).LessThan(lastIndexPrimitive)
						: CodeFactory.VarRef(currentIndexLocal.Name).GreaterThan(lastIndexPrimitive);
					var incrementExpression = GetIncrementExpression(currentIndexLocal.Name, stepExpression);
					
					if (unit.step.hasValidConnection)
					{
						incrementExpression.Bind(context, unit.step.connection.source.unit);
					}

					yield return new CodeForStatement(currentIndexLocal, conditional, incrementExpression, bodyStatements);
				}
				else
				{
					var firstIndexLocal = context.currentScope.DeclareLocal(CodeFactory.IntType, "firstIndex", firstIndexExpression);
					var lastIndexLocal = context.currentScope.DeclareLocal(CodeFactory.IntType, "lastIndex", lastIndexExpression);
					var ascendingLocal = context.currentScope.DeclareLocal(CodeFactory.BoolType, "ascending", CodeFactory.VarRef(firstIndexLocal.Name).LessThanOrEqual(CodeFactory.VarRef(lastIndexLocal.Name)));
					var currentIndexLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.IntType, unit.currentIndex, CodeFactory.VarRef(firstIndexLocal.Name));
					var conditional = CodeFactory.LogicalOr(
						CodeFactory.VarRef(ascendingLocal.Name).LogicalAnd(CodeFactory.VarRef(currentIndexLocal.Name).LessThan(CodeFactory.VarRef(lastIndexLocal.Name))),
						CodeFactory.VarRef(ascendingLocal.Name).LogicalNot().LogicalAnd(CodeFactory.VarRef(currentIndexLocal.Name).GreaterThan(CodeFactory.VarRef(lastIndexLocal.Name))));
					var incrementExpression = GetIncrementExpression(currentIndexLocal.Name, stepExpression);

					if (unit.step.hasValidConnection)
					{
						incrementExpression.Bind(context, unit.step.connection.source.unit);
					}

					yield return new CodeBlockStatement(new CodeStatement[] {
						firstIndexLocal,
						lastIndexLocal,
						ascendingLocal,
						new CodeForStatement(currentIndexLocal, conditional, incrementExpression, bodyStatements),
					});
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.currentIndex)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		private CodeExpression GetIncrementExpression(string indexLocal, CodeExpression stepExpression)
		{
			if (stepExpression is CodePrimitiveExpression stepPrimitive)
			{
				int step = (int) stepPrimitive.Value;
				if (step == -1 || step == +1)
				{
					return CodeFactory.VarRef(indexLocal).UnaryOp(step < 0 ? CodeUnaryOperatorType.PostDecrement : CodeUnaryOperatorType.PostIncrement);
				}
				else
				{
					return CodeFactory.VarRef(indexLocal).CompoundAssign(step < 0 ? CodeCompoundAssignmentOperatorType.Subtract : CodeCompoundAssignmentOperatorType.Add, CodeFactory.Primitive(Math.Abs(step)));
				}
			}
			else
			{
				return CodeFactory.VarRef(indexLocal).AddAssign(stepExpression);
			}
		}
	}
}
