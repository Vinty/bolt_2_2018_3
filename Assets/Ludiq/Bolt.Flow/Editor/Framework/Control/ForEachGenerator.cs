﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(ForEach))]
	class ForEachGenerator : UnitGenerator<ForEach>
	{
		public ForEachGenerator(ForEach unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				context.EnterScope();

				var collectionExpression = unit.collection.GenerateExpression(context);
				var bodyStatements = unit.body.GenerateStatements(context);

				CodeVariableDeclarationStatement indexLocal = null;
				CodeVariableDeclarationStatement loopEnumeratorLocal = null;

				if (unit.currentIndex.hasAnyConnection)
				{
					indexLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.IntType, unit.currentIndex, CodeFactory.Primitive(0));
					bodyStatements = bodyStatements.Concat(new[] { CodeFactory.VarRef(indexLocal.Name).PostIncrement().Statement() });
				}

				if (unit.dictionary)
				{
					loopEnumeratorLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "enumerator");
					bodyStatements = new[] {
						context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.currentKey, CodeFactory.VarRef(loopEnumeratorLocal.Name).Field("Key")),
						context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.currentItem, CodeFactory.VarRef(loopEnumeratorLocal.Name).Field("Value"))
					}.Concat(bodyStatements);
				}
				else
				{
					loopEnumeratorLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.currentItem);
				}

				var loop = new CodeForeachStatement(loopEnumeratorLocal, collectionExpression, bodyStatements);

				if (indexLocal != null)
				{
					yield return new CodeBlockStatement(new CodeStatement[] { indexLocal, loop });
				}
				else
				{
					yield return loop;
				}

				context.ExitScope();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.currentKey && unit.dictionary
			|| valueOutput == unit.currentItem
			|| valueOutput == unit.currentIndex)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
