﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(Throw))]
	class ThrowGenerator : UnitGenerator<Throw>
	{
		public ThrowGenerator(Throw unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				if (unit.custom)
				{
					yield return new CodeThrowStatement(unit.exception.GenerateExpression(context));
				}
				else
				{
					yield return new CodeThrowStatement(CodeFactory.TypeRef(typeof(Exception)).ObjectCreate(unit.message.GenerateExpression(context)));
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
