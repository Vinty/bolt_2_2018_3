﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SelectUnit))]
	public class SelectUnitGenerator : UnitGenerator<SelectUnit>
	{
		public SelectUnitGenerator(SelectUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				// Explicitly upcast to output type because ?: in C# does not attempt to find common base type for its branches.
				var ifTrueExpression = unit.ifTrue.GenerateExpression(context).Cast(CodeFactory.TypeRef(unit.selection.type));
				var ifFalseExpression = unit.ifFalse.GenerateExpression(context).Cast(CodeFactory.TypeRef(unit.selection.type));
				return CodeFactory.Conditional(unit.condition.GenerateExpression(context), ifTrueExpression, ifFalseExpression);
			}

			throw new NotImplementedException();
		}
	}
}
