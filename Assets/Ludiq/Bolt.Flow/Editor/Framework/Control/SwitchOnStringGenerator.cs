﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SwitchOnString))]
	class SwitchOnStringGenerator : SwitchUnitGenerator<SwitchOnString, string>
	{
		public SwitchOnStringGenerator(SwitchOnString unit) : base(unit) {}
	}
}
