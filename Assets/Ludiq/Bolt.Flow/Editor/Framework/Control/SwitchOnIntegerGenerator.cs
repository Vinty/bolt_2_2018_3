﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SwitchOnInteger))]
	class SwitchOnIntegerGenerator : SwitchUnitGenerator<SwitchOnInteger, int>
	{
		public SwitchOnIntegerGenerator(SwitchOnInteger unit) : base(unit) {}
	}
}
