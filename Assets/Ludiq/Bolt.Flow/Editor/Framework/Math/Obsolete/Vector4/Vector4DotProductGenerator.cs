﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4DotProduct))]
	public class Vector4DotProductGenerator : UnitGenerator<Vector4DotProduct>
	{ 
		public Vector4DotProductGenerator(Vector4DotProduct unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.dotProduct)
			{
				return CodeFactory.TypeRef(typeof(Vector4)).Expression().Method("Dot").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector4)),
					unit.b.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
