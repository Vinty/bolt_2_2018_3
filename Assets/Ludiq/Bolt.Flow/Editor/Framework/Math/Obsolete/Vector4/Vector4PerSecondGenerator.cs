﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4PerSecond))]
	public class Vector4PerSecondGenerator : UnitGenerator<Vector4PerSecond>
	{ 
		public Vector4PerSecondGenerator(Vector4PerSecond unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return unit.input.GenerateExpression(context, typeof(Vector4)).Multiply(CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
