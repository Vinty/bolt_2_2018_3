﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4MoveTowards))]
	public class Vector4MoveTowardsGenerator : UnitGenerator<Vector4MoveTowards>
	{ 
		public Vector4MoveTowardsGenerator(Vector4MoveTowards unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				var maxDeltaExpression = unit.maxDelta.GenerateExpression(context, typeof(float));
				if (unit.perSecond)
				{
					maxDeltaExpression.Multiply(CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"));
				}

				return CodeFactory.TypeRef(typeof(Vector4)).Expression().Method("MoveTowards").Invoke(
					unit.current.GenerateExpression(context, typeof(Vector4)),
					unit.target.GenerateExpression(context, typeof(Vector4)),
					maxDeltaExpression);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
