﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Divide))]
	public class Vector4DivideGenerator : UnitGenerator<Vector4Divide>
	{ 
		public Vector4DivideGenerator(Vector4Divide unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.quotient)
			{
				return unit.dividend.GenerateExpression(context, typeof(Vector4)).Method("Divide").Invoke(unit.divisor.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
