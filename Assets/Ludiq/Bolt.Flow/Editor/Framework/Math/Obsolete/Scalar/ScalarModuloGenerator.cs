﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(ScalarModulo))]
	public class ScalarModuloGenerator : UnitGenerator<ScalarModulo>
	{ 
		public ScalarModuloGenerator(ScalarModulo unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.remainder)
			{
				return unit.dividend.GenerateExpression(context, typeof(float)).Modulo(unit.divisor.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
