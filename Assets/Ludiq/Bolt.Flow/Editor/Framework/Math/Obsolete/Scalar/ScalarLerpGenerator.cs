﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarLerp))]
	public class ScalarLerpGenerator : UnitGenerator<ScalarLerp>
	{ 
		public ScalarLerpGenerator(ScalarLerp unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.interpolation)			
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Lerp").Invoke(
					unit.a.GenerateExpression(context, typeof(float)),
					unit.b.GenerateExpression(context, typeof(float)),
					unit.t.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
