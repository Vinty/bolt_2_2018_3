﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarRound))]
	public class ScalarRoundGenerator : UnitGenerator<ScalarRound>
	{ 
		public ScalarRoundGenerator(ScalarRound unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				string roundMethod;
				switch (unit.rounding)
				{
					case Round<float, int>.Rounding.Floor: roundMethod = "Floor"; break;
					case Round<float, int>.Rounding.AwayFromZero: roundMethod = "Round"; break;
					case Round<float, int>.Rounding.Ceiling: roundMethod = "Ceil"; break;
					default: throw new UnexpectedEnumValueException<Round<float, int>.Rounding>(unit.rounding);
				}

				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method(roundMethod).Invoke(unit.input.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
