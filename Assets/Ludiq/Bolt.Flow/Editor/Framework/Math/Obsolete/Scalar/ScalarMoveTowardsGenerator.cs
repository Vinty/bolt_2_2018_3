﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarMoveTowards))]
	public class ScalarMoveTowardsGenerator : UnitGenerator<ScalarMoveTowards>
	{ 
		public ScalarMoveTowardsGenerator(ScalarMoveTowards unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				var maxDeltaExpression = unit.maxDelta.GenerateExpression(context, typeof(float));
				if (unit.perSecond)
				{
					maxDeltaExpression.Multiply(CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"));
				}

				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("MoveTowards").Invoke(
					unit.current.GenerateExpression(context, typeof(float)),
					unit.target.GenerateExpression(context, typeof(float)),
					maxDeltaExpression);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
