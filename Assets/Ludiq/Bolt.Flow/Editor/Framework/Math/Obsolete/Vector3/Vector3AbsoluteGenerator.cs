﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Absolute))]
	public class Vector3AbsoluteGenerator : UnitGenerator<Vector3Absolute>
	{ 
		public Vector3AbsoluteGenerator(Vector3Absolute unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return unit.input.GenerateExpression(context, typeof(Vector3)).Method("Abs").Invoke();
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
