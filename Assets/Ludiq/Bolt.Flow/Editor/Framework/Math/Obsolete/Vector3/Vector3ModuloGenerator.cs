﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Modulo))]
	public class Vector3ModuloGenerator : UnitGenerator<Vector3Modulo>
	{ 
		public Vector3ModuloGenerator(Vector3Modulo unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.remainder)
			{
				return unit.dividend.GenerateExpression(context, typeof(Vector3)).Method("Modulo").Invoke(unit.divisor.GenerateExpression(context, typeof(Vector3)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
