﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Add))]
	public class Vector3AddGenerator : UnitGenerator<Vector3Add>
	{ 
		public Vector3AddGenerator(Vector3Add unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.sum)
			{
				return unit.a.GenerateExpression(context, typeof(Vector3)).Add(unit.b.GenerateExpression(context, typeof(Vector3)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
