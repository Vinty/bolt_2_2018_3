﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Project))]
	public class Vector2ProjectGenerator : UnitGenerator<Vector2Project>
	{ 
		public Vector2ProjectGenerator(Vector2Project unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.projection)
			{
				return unit.a.GenerateExpression(context, typeof(Vector2)).Method("Project").Invoke(unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
