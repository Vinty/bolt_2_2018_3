﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Add))]
	public class Vector2AddGenerator : UnitGenerator<Vector2Add>
	{ 
		public Vector2AddGenerator(Vector2Add unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.sum)
			{
				return unit.a.GenerateExpression(context, typeof(Vector2)).Add(unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
