﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2DotProduct))]
	public class Vector2DotProductGenerator : UnitGenerator<Vector2DotProduct>
	{ 
		public Vector2DotProductGenerator(Vector2DotProduct unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.dotProduct)
			{
				return CodeFactory.TypeRef(typeof(Vector2)).Expression().Method("Dot").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector2)),
					unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
