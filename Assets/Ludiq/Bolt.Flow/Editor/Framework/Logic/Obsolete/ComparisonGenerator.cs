﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Comparison))]
	public class ComparisonGenerator : UnitGenerator<Comparison>
	{ 
		public ComparisonGenerator(Comparison unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.numeric)
			{
				var expressionA = unit.a.GenerateExpression(context, typeof(float));
				var expressionB = unit.b.GenerateExpression(context, typeof(float));

				if (valueOutput == unit.aLessThanB) return expressionA.LessThan(expressionB);
				else if (valueOutput == unit.aLessThanOrEqualToB) return expressionA.Method("LessThanOrApproximately").Invoke(expressionB);
				else if (valueOutput == unit.aEqualToB) return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Approximately").Invoke(expressionA, expressionB);
				else if (valueOutput == unit.aNotEqualToB) return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Approximately").Invoke(expressionA, expressionB).LogicalNot();
				else if (valueOutput == unit.aGreaterThanOrEqualToB) return expressionA.Method("GreaterThanOrApproximately").Invoke(expressionB);
				else if (valueOutput == unit.aGreatherThanB) return expressionA.GreaterThan(expressionB);
			}
			else
			{
				if (valueOutput == unit.aLessThanB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.LessThan, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				else if (valueOutput == unit.aLessThanOrEqualToB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.LessThanOrEqual, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				else if (valueOutput == unit.aEqualToB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Equality, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				else if (valueOutput == unit.aNotEqualToB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Inequality, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				else if (valueOutput == unit.aGreaterThanOrEqualToB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.GreaterThanOrEqual, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				else if (valueOutput == unit.aGreatherThanB) return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.GreaterThan, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
			}

			throw new NotImplementedException();
		}
	}
}