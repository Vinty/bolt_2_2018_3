﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(And))]
	public class AndGenerator : UnitGenerator<And>
	{ 
		public AndGenerator(And unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				return unit.a.GenerateExpression(context, typeof(bool)).LogicalAnd(unit.b.GenerateExpression(context, typeof(bool)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
