﻿using System;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[FuzzyOption(typeof(Expose))]
	public class ExposeOption : UnitOption<Expose>
	{
		public ExposeOption() : base() { }

		public ExposeOption(Expose unit) : base(unit)
		{

		}

		public Type exposedType { get; private set; }
		
		public override Type sourceType => exposedType;
		
		protected override string Key()
		{
			return $"{exposedType.FullName}@expose";
		}

		protected override string Label(bool human)
		{
			return $"Expose";
		}

		protected override string Haystack(bool human)
		{
			return $"Expose {unit.type.SelectedName(human)}";
		}

		protected override bool ShowValueOutputsInFooter()
		{
			return false;
		}

		protected override void FillSerializable()
		{
			exposedType = unit.type;

			base.FillSerializable();
		}

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			exposedType = Codebase.DeserializeRootType(data.tag1);
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = Codebase.SerializeType(exposedType);

			return row;
		}
	}
}