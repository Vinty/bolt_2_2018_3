﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bolt
{
	[Analyser(typeof(TweenMember))]
	public class TweenMemberAnalyser : MemberUnitAnalyser
	{
		public TweenMemberAnalyser(GraphReference reference, TweenMember target) : base(reference, target) { }

		private TweenMember tweenMember => (TweenMember)target;

		protected override IEnumerable<Warning> Warnings()
		{
			foreach (var baseWarning in base.Warnings())
			{
				yield return baseWarning;
			}

			if (tweenMember.member != null && !TweenMember.tweenFactoriesByType.ContainsKey(tweenMember.member.type))
			{
				yield return Warning.Severe($"Member of type {tweenMember.member.type} does not support tweening.");
			}
		}
	}
}
