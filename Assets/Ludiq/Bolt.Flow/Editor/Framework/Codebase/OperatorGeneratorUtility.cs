﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public static class OperatorGeneratorUtility
	{
		public static readonly Dictionary<UnaryOperator, CodeUnaryOperatorType> unaryOperatorToCode = new Dictionary<UnaryOperator, CodeUnaryOperatorType>()
		{
			{UnaryOperator.BitwiseNegation, CodeUnaryOperatorType.LogicalNot},
			{UnaryOperator.Plus, CodeUnaryOperatorType.Positive},
			{UnaryOperator.NumericNegation, CodeUnaryOperatorType.Negative},
			{UnaryOperator.Increment, CodeUnaryOperatorType.PostIncrement},
			{UnaryOperator.Decrement, CodeUnaryOperatorType.PostDecrement},
		};

		public static readonly Dictionary<BinaryOperator, CodeBinaryOperatorType> binaryOperatorToCode = new Dictionary<BinaryOperator, CodeBinaryOperatorType>()
		{
			{BinaryOperator.Addition, CodeBinaryOperatorType.Add},
			{BinaryOperator.Subtraction, CodeBinaryOperatorType.Subtract},
			{BinaryOperator.Multiplication, CodeBinaryOperatorType.Multiply},
			{BinaryOperator.Division, CodeBinaryOperatorType.Divide},
			{BinaryOperator.Modulo, CodeBinaryOperatorType.Modulo},
			{BinaryOperator.And, CodeBinaryOperatorType.BitwiseAnd},
			{BinaryOperator.Or, CodeBinaryOperatorType.BitwiseOr},
			{BinaryOperator.ExclusiveOr, CodeBinaryOperatorType.BitwiseXor},
			{BinaryOperator.Equality, CodeBinaryOperatorType.Equality},
			{BinaryOperator.Inequality, CodeBinaryOperatorType.Inequality},
			{BinaryOperator.LessThan, CodeBinaryOperatorType.LessThan},
			{BinaryOperator.GreaterThan, CodeBinaryOperatorType.GreaterThan},
			{BinaryOperator.GreaterThanOrEqual, CodeBinaryOperatorType.GreaterThanOrEqual},
			{BinaryOperator.LessThanOrEqual, CodeBinaryOperatorType.LessThanOrEqual},
			{BinaryOperator.LeftShift, CodeBinaryOperatorType.BitwiseShiftLeft},
			{BinaryOperator.RightShift, CodeBinaryOperatorType.BitwiseShiftRight},
		};

		public static CodeExpression GenerateGenericUnaryOperatorExpression(FlowMethodGenerationContext context, UnaryOperator op, ValueInput input)
		{
			var handler = op.GetHandler();
			var type = input.GetSourceType();
			var sourceExpression = input.GenerateSourceExpression(context);

			if (handler.HasOperatorDefined(type))
			{
				return sourceExpression.UnaryOp(unaryOperatorToCode[op]);
			}
			else
			{
				return CodeFactory.TypeRef(typeof(OperatorUtility)).Expression().Method("Operate").Invoke(
					CodeFactory.TypeRef(typeof(UnaryOperator)).Expression().Field(op.ToString()),
					sourceExpression);
			}
		}

		public static CodeExpression GenerateGenericBinaryOperatorExpression(FlowMethodGenerationContext context, BinaryOperator op, ValueInput a, ValueInput b)
		{
			var handler = op.GetHandler();
			var typeA = a.GetSourceType();
			var typeB = b.GetSourceType();
			var sourceExpressionA = a.GenerateSourceExpression(context);
			var sourceExpressionB = b.GenerateSourceExpression(context);

			if (handler.HasOperatorDefined(typeA, typeB)
			|| op == BinaryOperator.Addition && (typeA == typeof(string) || typeB == typeof(string)))
			{
				return sourceExpressionA.BinaryOp(binaryOperatorToCode[op], sourceExpressionB);
			}
			else
			{
				return CodeFactory.TypeRef(typeof(OperatorUtility)).Expression().Method("Operate").Invoke(
					CodeFactory.TypeRef(typeof(BinaryOperator)).Expression().Field(op.ToString()),
					sourceExpressionA,
					sourceExpressionB);
			}
		}
	}
}
