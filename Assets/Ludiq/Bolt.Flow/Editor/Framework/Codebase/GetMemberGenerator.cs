﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(GetMember))]
	public class GetMemberGenerator : UnitGenerator<GetMember>
	{ 
		public GetMemberGenerator(GetMember unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.value)			
			{
				return unit.member.GenerateGet(context, unit.member.GenerateTargetExpression(context, unit.target));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
