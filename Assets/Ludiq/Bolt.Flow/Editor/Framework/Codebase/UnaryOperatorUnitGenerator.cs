﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(UnaryOperatorUnit))]
	public class UnaryOperatorUnitGenerator : UnitGenerator<UnaryOperatorUnit>
	{ 
		public UnaryOperatorUnitGenerator(UnaryOperatorUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				yield return context.currentScope.DeclareValuePortLocal(CodeFactory.TypeRef(unit.input.type), unit.input, unit.input.GenerateExpression(context, unit.type));
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)			
			{
				if (context.currentScope.TryGetValuePortLocal(valueOutput, out string local))
				{
					return CodeFactory.VarRef(local);
				}
				else
				{
					return unit.input.GenerateExpression(context, unit.type).UnaryOp(OperatorGeneratorUtility.unaryOperatorToCode[unit.@operator]);
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
