﻿using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(TweenMember))]
	public class TweenMemberOption : MemberUnitOption<TweenMember>
	{
		public TweenMemberOption() : base() { }

		public TweenMemberOption(TweenMember unit) : base(unit) { }

		protected override ActionDirection direction => ActionDirection.Any;

		protected override string Label(bool human)
		{
			return $"Tween {unit.member.info.SelectedName(human)}";
		}

		protected override string Key()
		{
			return $"{member.ToUniqueString()}@tween";
		}
	}
}