﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Analyser(typeof(MemberUnit))]
	public class MemberUnitAnalyser : UnitAnalyser<MemberUnit>
	{
		public MemberUnitAnalyser(GraphReference reference, MemberUnit target) : base(reference, target) { }

		private IEnumerable<Warning> TypeWarnings(Type type)
		{
			if (type != null)
			{
				if (type.IsGenericType)
				{
					var genericArguments = type.GetGenericArguments();

					foreach (var genericArgument in genericArguments)
					{
						if (!genericArgument.IsGenericParameter)
						{
							foreach (var warning in TypeWarnings(genericArgument))
							{
								yield return warning;
							}
						}
						else
						{
							yield return Warning.Severe("Missing type for parameter " + NameUtility.DisplayName(genericArgument) + ".");
						}
					}
				}
			}
		}

		protected override IEnumerable<Warning> Warnings()
		{
			foreach (var baseWarning in base.Warnings())
			{
				yield return baseWarning;
			}

			if (target.targetType != null)
			{
				foreach (var warning in TypeWarnings(target.targetType)) yield return warning;
			}

			if (target.member != null && target.member.isReflected)
			{
				var obsoleteAttribute = target.member.info.GetAttribute<ObsoleteAttribute>();

				if (obsoleteAttribute != null)
				{
					if (obsoleteAttribute.Message != null)
					{
						yield return Warning.Caution("Obsolete: " + obsoleteAttribute.Message);
					}
					else
					{
						yield return Warning.Caution("Member is obsolete.");
					}
				}
			}
		}
	}
}
