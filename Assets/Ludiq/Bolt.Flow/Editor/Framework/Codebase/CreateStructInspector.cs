﻿using System;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Inspector(typeof(CreateStruct))]
	public sealed class CreateStructInspector : Inspector
	{
		public CreateStructInspector(Metadata metadata) : base(metadata) { }

		private Metadata typeMetadata => metadata[nameof(CreateStruct.type)];
		
		private bool hasType => typeMetadata.value != null;

		protected override float GetHeight(float width, GUIContent label)
		{
			return LudiqGUI.GetInspectorHeight(this, typeMetadata, width, label);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			LudiqGUI.Inspector(typeMetadata, position, label);
		}

		public override float GetAdaptiveWidth()
		{
			return typeMetadata.Inspector().GetAdaptiveWidth();
		}
	}
}