﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(CreateStruct))]
	public class CreateStructOption : UnitOption<CreateStruct>
	{
		public CreateStructOption() : base() { }

		public CreateStructOption(CreateStruct unit) : base(unit) { }

		public Type structType { get; private set; }
		
		protected override void FillSerializable()
		{
			structType = unit.type;
			base.FillSerializable();
		}
		
		public override Type sourceType => structType;

		protected override string Label(bool human)
		{
			if (human)
			{
				return $"Create {structType.HumanName()} ()";
			}
			else
			{
				return $"new {structType.CSharpName()} ()";
			}
		}

		protected override string Haystack(bool human)
		{
			if (human)
			{
				return $"{structType.HumanName()}: Create {structType.HumanName()}";
			}
			else
			{
				return $"new {structType.CSharpName()}";
			}
		}
		
		public override string SearchResultLabel(string query)
		{
			return base.SearchResultLabel(query) + " ()";
		}

		protected override int Order()
		{
			return 0;
		}

		protected override string Key()
		{
			return $"{structType.FullName}@create";
		}

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			structType = Codebase.DeserializeRootType(data.tag1);
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = Codebase.SerializeType(structType);

			return row;
		}
	}
}