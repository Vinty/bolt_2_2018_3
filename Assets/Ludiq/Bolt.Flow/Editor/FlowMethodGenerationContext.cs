﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Ludiq;

namespace Bolt
{
	public sealed class FlowMethodGenerationContext : GraphMethodGenerationContext<FlowMethodGenerationContext, FlowMethodGenerationScope>
	{
		private CodeVariableDeclarationStatement flowVariablesLocal = null;

		public FlowMethodGenerationContext(GraphClassGenerationContext graphClassContext, bool coroutine)
			: base(graphClassContext, coroutine)
		{
		}

		public string RequireFlowVariablesLocal()
		{
			if (flowVariablesLocal == null)
			{
				flowVariablesLocal = scopeChain[0].DeclareLocal(
					CodeFactory.VarType,
					"flowVariables",
					CodeFactory.TypeRef(typeof(GenericPool<VariableDeclarations>)).Expression().Method("New").Invoke(
						CodeFactory.Lambda(
							Enumerable.Empty<string>(),
							CodeFactory.TypeRef(typeof(VariableDeclarations)).ObjectCreate())));

				enterStatements.Add(flowVariablesLocal);
				exitStatements.Add(CodeFactory.TypeRef(typeof(GenericPool<VariableDeclarations>)).Expression().Method("Free").Invoke(CodeFactory.VarRef(flowVariablesLocal.Name)).Statement());
			}

			return flowVariablesLocal.Name;
		}

		protected override FlowMethodGenerationScope CreateScope(IdentifierPool identifierPool)
		{
			return new FlowMethodGenerationScope(currentScope, identifierPool);
		}
	}
}
