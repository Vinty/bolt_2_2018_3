﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;
using System.Linq;

namespace Bolt
{
	[Generator(typeof(CustomEvent))]
	class CustomEventGenerator : GameObjectEventUnitGenerator<CustomEvent, CustomEventArgs>
	{
		public CustomEventGenerator(CustomEvent unit) : base(unit)			
		{
			if (!unit.name.hasAnyConnection && unit.name.hasDefaultValue)
			{
				var nameDefaultValue = unit.defaultValues[unit.name.key];
				if (nameDefaultValue is string nameString && !StringUtility.IsNullOrWhiteSpace(nameString))
				{
					triggerMethodOriginalName = nameString;
				}
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			var nameExpression = unit.name.GenerateExpression(context, typeof(string));
			if (nameExpression is CodePrimitiveExpression namePrimitive)
			{
				var name = (string) namePrimitive.Value;
				yield return new CodeIfStatement(
					CodeFactory.VarRef(argumentLocalName).Field("name").Method("Trim").Invoke().Method("Equals").Invoke(
						CodeFactory.Primitive(name.Trim()),
						CodeFactory.TypeRef(typeof(StringComparison)).Expression().Field("OrdinalIgnoreCase")
					),
					GenerateTriggerInnerStatements(context).ToList());
			}
			else
			{
				yield return new CodeIfStatement(
					CodeFactory.VarRef(argumentLocalName).Field("name").Method("Trim").Invoke().Method("Equals").Invoke(
						nameExpression.Method("Trim").Invoke(),
						CodeFactory.TypeRef(typeof(StringComparison)).Expression().Field("OrdinalIgnoreCase")
					),
					GenerateTriggerInnerStatements(context).ToList());
			}
		}

		private IEnumerable<CodeStatement> GenerateTriggerInnerStatements(FlowMethodGenerationContext context)
		{
			for (var i = 0; i < unit.argumentCount; i++)
			{
				yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.argumentPorts[i], CodeFactory.VarRef(argumentLocalName).Field("arguments").Index(CodeFactory.Primitive(i)));
			}

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}