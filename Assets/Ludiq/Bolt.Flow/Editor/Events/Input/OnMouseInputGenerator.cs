﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(OnMouseInput))]
	public class OnMouseInputGenerator : MachineEventUnitGenerator<OnMouseInput, EmptyEventArgs>
	{
		public OnMouseInputGenerator(OnMouseInput unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			var buttonExpression = unit.button.GenerateExpression(context, typeof(MouseButton));
			var actionExpression = unit.action.GenerateExpression(context, typeof(PressState));

			bool needsSwitchCase = true;

			if (actionExpression is CodeFieldReferenceExpression actionField
			&& actionField.TargetObject is CodeTypeReferenceExpression actionFieldTargetType
			&& actionFieldTargetType.Type.ResolveExpandedType() == typeof(PressState))
			{
				needsSwitchCase = false;

				var action = (PressState) Enum.Parse(typeof(PressState), actionField.FieldName);

				string methodName;
				switch (action)
				{
					case PressState.Down: methodName = "GetMouseButtonDown"; break;
					case PressState.Up: methodName = "GetMouseButtonUp"; break;
					case PressState.Hold: methodName = "GetMouseButton"; break;
					default: throw new UnexpectedEnumValueException<PressState>(action);
				}

				yield return new CodeIfStatement(CodeFactory.TypeRef(typeof(Input)).Expression().Method(methodName).Invoke(buttonExpression), unit.trigger.GenerateStatements(context).ToList());
			}
			
			if (needsSwitchCase)
			{
				var buttonLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "button", buttonExpression);
				var actionLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "action", actionExpression);
				var shouldTriggerLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "shouldTrigger", CodeFactory.Primitive(false));

				yield return buttonLocal;
				yield return actionLocal;
				yield return shouldTriggerLocal;

				yield return new CodeSwitchStatement(CodeFactory.VarRef(actionLocal.Name), new CodeStatement[] {
					new CodeCaseStatement(CodeFactory.TypeRef(typeof(PressState)).Expression().Field("Down"), new[] {
						CodeFactory.VarRef(shouldTriggerLocal.Name).Assign(CodeFactory.TypeRef(typeof(Input)).Expression().Method("GetMouseButtonDown").Invoke(CodeFactory.VarRef(buttonLocal.Name))).Statement()
					}),
					new CodeCaseStatement(CodeFactory.TypeRef(typeof(PressState)).Expression().Field("Up"), new[] {
						CodeFactory.VarRef(shouldTriggerLocal.Name).Assign(CodeFactory.TypeRef(typeof(Input)).Expression().Method("GetMouseButtonUp").Invoke(CodeFactory.VarRef(buttonLocal.Name))).Statement()
					}),
					new CodeCaseStatement(CodeFactory.TypeRef(typeof(PressState)).Expression().Field("Hold"), new[] {
						CodeFactory.VarRef(shouldTriggerLocal.Name).Assign(CodeFactory.TypeRef(typeof(Input)).Expression().Method("GetMouseButton").Invoke(CodeFactory.VarRef(buttonLocal.Name))).Statement()
					}),
					new CodeDefaultStatement(new[] {
						new CodeThrowStatement(CodeFactory.TypeRef(typeof(UnexpectedEnumValueException<PressState>)).ObjectCreate(CodeFactory.VarRef(actionLocal.Name))) 
					})
				});
				yield return new CodeIfStatement(CodeFactory.VarRef(shouldTriggerLocal.Name), unit.trigger.GenerateStatements(context).ToList());
			}
		}
	}
}
