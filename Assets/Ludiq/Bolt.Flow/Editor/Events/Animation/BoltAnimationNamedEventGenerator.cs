﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(BoltNamedAnimationEvent))]
	public class BoltNamedAnimationEventGenerator : MachineEventUnitGenerator<BoltNamedAnimationEvent, AnimationEvent>
	{
		public BoltNamedAnimationEventGenerator(BoltNamedAnimationEvent unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			var nameExpression = unit.name.GenerateExpression(context, typeof(string));
			if (nameExpression is CodePrimitiveExpression namePrimitive)
			{
				var name = (string) namePrimitive.Value;
				yield return new CodeIfStatement(
					CodeFactory.VarRef(argumentLocalName).Field("stringParameter").Method("Trim").Invoke().Method("Equals").Invoke(
						CodeFactory.Primitive(name.Trim()),
						CodeFactory.TypeRef(typeof(StringComparison)).Expression().Field("OrdinalIgnoreCase")
					),
					GenerateTriggerInnerStatements(context).ToList()
				);
			}
			else
			{
				yield return new CodeIfStatement(
					CodeFactory.VarRef(argumentLocalName).Field("stringParameter").Method("Trim").Invoke().Method("Equals").Invoke(
						nameExpression.Method("Trim").Invoke(),
						CodeFactory.TypeRef(typeof(StringComparison)).Expression().Field("OrdinalIgnoreCase")
					),
					GenerateTriggerInnerStatements(context).ToList()
				);
			}
		}

		private IEnumerable<CodeStatement> GenerateTriggerInnerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.floatParameter, CodeFactory.VarRef(argumentLocalName).Field("floatParameter"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.intParameter, CodeFactory.VarRef(argumentLocalName).Field("intParameter"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.objectReferenceParameter, CodeFactory.VarRef(argumentLocalName).Field("objectReferenceParameter"));

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
