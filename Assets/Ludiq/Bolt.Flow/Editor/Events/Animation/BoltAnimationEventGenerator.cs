﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(BoltAnimationEvent))]
	public class BoltAnimationEventGenerator : MachineEventUnitGenerator<BoltAnimationEvent, AnimationEvent>
	{
		public BoltAnimationEventGenerator(BoltAnimationEvent unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.stringParameter, CodeFactory.VarRef(argumentLocalName).Field("stringParameter"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.floatParameter, CodeFactory.VarRef(argumentLocalName).Field("floatParameter"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.intParameter, CodeFactory.VarRef(argumentLocalName).Field("intParameter"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.objectReferenceParameter, CodeFactory.VarRef(argumentLocalName).Field("objectReferenceParameter"));

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
