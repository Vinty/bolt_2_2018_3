﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(TriggerEventUnit))]
	public class TriggerEventUnitGenerator : GameObjectEventUnitGenerator<TriggerEventUnit, Collider>
	{
		public TriggerEventUnitGenerator(TriggerEventUnit unit) : base(unit) {}

		protected override string argumentLocalName => "other";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			context.currentScope.AliasValuePortLocal(unit.collider, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
