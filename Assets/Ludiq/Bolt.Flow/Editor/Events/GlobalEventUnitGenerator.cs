﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class GlobalEventUnitGenerator<TUnit, TArgs> : EventUnitGenerator<TUnit, TArgs>
		where TUnit : GlobalEventUnit<TArgs>
	{
		public GlobalEventUnitGenerator(TUnit unit) : base(unit)
		{
		}

		protected override string hookName => unit.hookName;
		protected override CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context) => CodeFactory.Primitive(null);
	}
}
