﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	public abstract class GameObjectEventUnitGenerator<TUnit, TArgs> : EventUnitGenerator<TUnit, TArgs>
		where TUnit : GameObjectEventUnit<TArgs>
	{
		public GameObjectEventUnitGenerator(TUnit unit) : base(unit)
		{
		}

		protected override string hookName => unit.hookName;
		protected override CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context) => unit.target.GenerateExpression(context, typeof(GameObject));

		public override IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context)
		{
			foreach (var statement in base.GenerateStartListeningStatements(context)) yield return statement;

			if (unit.eventProxyType != null)
			{
				var targetLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "target", context.InstanceField(unit, hookMemberOriginalName).Field("target").Cast(CodeFactory.TypeRef(typeof(GameObject))));

				yield return new CodeIfStatement(
					CodeFactory.TypeRef(typeof(UnityThread)).Expression().Field("allowsAPI"),
					new CodeStatement[] {
						targetLocal,
						new CodeIfStatement(
							CodeFactory.VarRef(targetLocal.Name).Method("GetComponent").Invoke(CodeFactory.TypeOf(CodeFactory.TypeRef(unit.eventProxyType))).Equal(CodeFactory.Primitive(null)), 
							new CodeStatement[] {
								CodeFactory.VarRef(targetLocal.Name).Method("AddComponent").Invoke(CodeFactory.TypeOf(CodeFactory.TypeRef(unit.eventProxyType))).Statement(),
							})
					});
			}
		}
	}
}
