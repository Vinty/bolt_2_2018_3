﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(CollisionEvent2DUnit))]
	public class CollisionEvent2DUnitGenerator : GameObjectEventUnitGenerator<CollisionEvent2DUnit, Collision2D>
	{
		public CollisionEvent2DUnitGenerator(CollisionEvent2DUnit unit) : base(unit) {}

		protected override string argumentLocalName => "collision";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.collider, CodeFactory.VarRef(argumentLocalName).Field("collider"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.contacts, CodeFactory.VarRef(argumentLocalName).Field("contacts"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.relativeVelocity, CodeFactory.VarRef(argumentLocalName).Field("relativeVelocity"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.enabled, CodeFactory.VarRef(argumentLocalName).Field("enabled"));
			context.currentScope.AliasValuePortLocal(unit.data, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
