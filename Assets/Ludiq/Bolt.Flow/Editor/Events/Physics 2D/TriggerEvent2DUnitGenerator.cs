﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(TriggerEvent2DUnit))]
	public class TriggerEvent2DUnitGenerator : GameObjectEventUnitGenerator<TriggerEvent2DUnit, Collider2D>
	{
		public TriggerEvent2DUnitGenerator(TriggerEvent2DUnit unit) : base(unit) {}

		protected override string argumentLocalName => "other";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			context.currentScope.AliasValuePortLocal(unit.collider, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
