﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(OnDrawGizmos))]
	public class OnDrawGizmosGenerator : ManualEventUnitGenerator<OnDrawGizmos, EmptyEventArgs>
	{
		public OnDrawGizmosGenerator(OnDrawGizmos unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();
		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context) => unit.trigger.GenerateStatements(context);
		protected override CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context) => CodeFactory.ThisRef.Field("machineScript");
	}
}
