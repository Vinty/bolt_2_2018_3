﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Bolt
{
	[Generator(typeof(OnDropdownValueChanged))]
	public class OnDropdownValueChangedGenerator : GameObjectEventUnitGenerator<OnDropdownValueChanged, int>
	{
		public OnDropdownValueChangedGenerator(OnDropdownValueChanged unit) : base(unit) {}

		protected override string argumentLocalName => "index";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			context.currentScope.AliasValuePortLocal(unit.index, argumentLocalName);
			context.currentScope.DeclareValuePortLocal(CodeFactory.TypeRef(typeof(string)), unit.text, unit.target.GenerateExpression(context, typeof(Dropdown)).Field("options").Index(CodeFactory.VarRef(argumentLocalName)).Field("text"));

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
