﻿using Ludiq;

namespace Bolt
{
	[Plugin(BoltFlow.ID)]
	public sealed class BoltFlowManifest : PluginManifest
	{
		private BoltFlowManifest(BoltFlow plugin) : base(plugin) { }

		public override string name => "Bolt Flow";
		public override string author => "Ludiq";
		public override string description => "Flow-graph based visual scripting.";
		public override SemanticVersion version => "2.0.0a3";
	}
}