﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq.CodeDom;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(FlowGraph))]
	public class FlowGraphGenerator : GraphGenerator<FlowGraph>
	{
		private static readonly List<CodeUsingImport> CommonUsings = new List<CodeUsingImport>() {

		};

		public FlowGraphGenerator(FlowGraph graph) : base(graph)
		{
		}

		protected override Type graphScriptType => typeof(FlowGraphScript);
		protected override Type machineScriptType => typeof(FlowMachineScript);

		protected override void PopulateGraphScript(GraphClassGenerationContext context)
		{
			using (ProfilingUtility.SampleBlock("Populate flow graph script"))
			{
				context.usings.AddRange(new[] {
					CodeFactory.Using("System"),
					CodeFactory.Using("System.Collections"),
					CodeFactory.Using("System.Collections.Generic"),
					CodeFactory.Using("System.Linq"),
					CodeFactory.Using("Bolt"),
					CodeFactory.Using("Ludiq"),
					CodeFactory.Using("UnityEngine")
				});

				var units = graph.units.ToList();
				units.Sort((a, b) => {
					int result = a.GetType().Name.CompareTo(b.GetType().Name);
					if (result != 0) return result;
					return a.guid.CompareTo(b.guid);
				});

				var generators = units.Select(unit => unit.Generator<IUnitGenerator>()).ToList();
				var listeningUnitGenerator = generators.OfType<IListeningUnitGenerator>().ToList();

				foreach (var generator in generators)
				{
					generator.DeclareGraphMemberNames(context);
				}
				foreach (var generator in generators)
				{
					context.classDeclaration.Members.AddRange(generator.GenerateMembers(context).Bind(context, generator.unit));
				}
				context.classDeclaration.Members.Add(GenerateConstructor(context, generators));
				context.classDeclaration.Members.Add(GenerateGraphDataProperty(context, generators));
				context.classDeclaration.Members.Add(GenerateStartListeningMethod(context, listeningUnitGenerator));
				context.classDeclaration.Members.Add(GenerateStopListeningMethod(context, listeningUnitGenerator));
			}
		}

		private CodeConstructorMember GenerateConstructor(GraphClassGenerationContext context, List<IUnitGenerator> generators)
		{
			return context.AddImplementationMember(new FlowMethodGenerationContext(context, false).GenerateConstructor(
				CodeMemberModifiers.Public,
				new[] {
					new CodeParameterDeclaration(CodeFactory.TypeRef(typeof(IMachineScript)), "machineScript")
				},
				new CodeConstructorInitializer(CodeConstructorInitializer.InitializerKind.Base, new[] {
					CodeFactory.VarRef("machineScript")
				}),
				methodGenerationContext => generators.SelectMany(generator => generator.GenerateConstructorStatements(methodGenerationContext))));
		}


		private CodePropertyMember GenerateGraphDataProperty(GraphClassGenerationContext context, List<IUnitGenerator> generators)
		{
			var getter = new CodeUserPropertyAccessor(default(CodeMemberModifiers), new[] { new CodeReturnStatement(CodeFactory.VarRef("_graphData")) });
			var setter = new FlowMethodGenerationContext(context, false).GeneratePropertyAccessor(
				default(CodeMemberModifiers),
				Enumerable.Empty<CodeParameterDeclaration>(),
				methodGenerationContext => CodeFactory.ThisRef.Field("_graphData").Assign(CodeFactory.VarRef("value")).Statement().Yield().Concat(
					generators.SelectMany(generator => generator.GenerateDeserializerStatements(methodGenerationContext))));
			return context.AddImplementationMember(new CodePropertyMember(CodeMemberModifiers.Public | CodeMemberModifiers.Override, CodeFactory.TypeRef(typeof(FlowGraphData)), "graphData", getter, setter));
		}

		private CodeMethodMember GenerateStartListeningMethod(GraphClassGenerationContext context, List<IListeningUnitGenerator> listeningUnitGenerators)
		{
			return context.AddImplementationMember(new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public | CodeMemberModifiers.Override,
				CodeFactory.TypeRef(typeof(void)), "StartListening",
				Enumerable.Empty<CodeParameterDeclaration>(),
				methodGenerationContext => listeningUnitGenerators.SelectMany(generator => generator.GenerateStartListeningStatements(methodGenerationContext))));
		}

		private CodeMethodMember GenerateStopListeningMethod(GraphClassGenerationContext context, List<IListeningUnitGenerator> listeningUnitGenerators)
		{
			return context.AddImplementationMember(new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public | CodeMemberModifiers.Override,
				CodeFactory.TypeRef(typeof(void)), "StopListening",
				Enumerable.Empty<CodeParameterDeclaration>(),
				methodGenerationContext => listeningUnitGenerators.SelectMany(generator => generator.GenerateStopListeningStatements(methodGenerationContext))));
		}

		protected override void PopulateMachineScript(GraphClassGenerationContext context, IEnumerable<string> requiredMachineEvents)
		{
			using (ProfilingUtility.SampleBlock("Populate flow machine script"))
			{
				context.usings.AddRange(new[] {
					CodeFactory.Using("System"),
					CodeFactory.Using("Ludiq"),
				});

				context.classDeclaration.Members.Add(new CodeConstructorMember(
					CodeMemberModifiers.Public,
					Enumerable.Empty<CodeParameterDeclaration>(),
					new[] {
						CodeFactory.ThisRef.Field("graphScript").Assign(context.graphScriptTypeReference.ObjectCreate(CodeFactory.ThisRef)).Statement()
					}));

				context.GenerateMachineEvents(requiredMachineEvents);
			}
		}
	}
}
