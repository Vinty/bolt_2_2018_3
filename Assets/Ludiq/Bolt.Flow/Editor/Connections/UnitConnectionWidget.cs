﻿using System.Collections.Generic;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public abstract class UnitConnectionWidget<TConnection> : GraphElementWidget<FlowCanvas, TConnection>, IUnitConnectionWidget
		where TConnection : class, IUnitConnection
	{
		protected UnitConnectionWidget(FlowCanvas canvas, TConnection connection) : base(canvas, connection) { }



		#region Model

		protected TConnection connection => element;

		protected IUnitConnectionDebugData connectionDebugData => GetDebugData<IUnitConnectionDebugData>();

		#endregion



		#region Lifecycle

		public override void BeforeFrame()
		{
			base.BeforeFrame();

			if (showDroplets)
			{
				GraphGUI.UpdateDroplets(canvas, droplets, connectionDebugData.lastInvokeFrame, ref lastInvokeTime, ref dropTime);
			}
		}

		#endregion



		#region Positioning

		public abstract Axis2 axis { get; }

		protected Edge sourceEdge { get; private set; }

		protected Edge destinationEdge { get; private set; }

		public override IEnumerable<IWidget> positionDependencies
		{
			get
			{
				yield return canvas.Widget(connection.source);
				yield return canvas.Widget(connection.destination);
			}
		}
		
		public Rect sourceHandlePosition { get; private set; }

		public Rect destinationHandlePosition { get; private set; }

		public Vector2 sourceHandleEdgeCenter { get; private set; }

		public Vector2 destinationHandleEdgeCenter { get; private set; }

		public Vector2 middlePosition;

		private Rect _position;

		private Rect _clippingPosition;

		public override Rect position
		{
			get { return _position; }
			set { }
		}

		public override Rect clippingPosition => _clippingPosition;

		public override void CachePosition()
		{
			base.CachePosition();

			sourceHandlePosition = canvas.Widget<IUnitPortWidget>(connection.source).handlePosition;
			destinationHandlePosition = canvas.Widget<IUnitPortWidget>(connection.destination).handlePosition;
			
			switch (axis)
			{
				case Axis2.Horizontal:
					sourceEdge = Edge.Right;
					destinationEdge = Edge.Left;
					break;

				case Axis2.Vertical:
					sourceEdge = Edge.Bottom;
					destinationEdge = Edge.Top;
					break;

				default:
					throw new UnexpectedEnumValueException<Axis2>(axis);
			}

			sourceHandleEdgeCenter = sourceHandlePosition.GetEdgeCenter(sourceEdge);
			destinationHandleEdgeCenter = destinationHandlePosition.GetEdgeCenter(destinationEdge);

			middlePosition = (sourceHandlePosition.center + destinationHandlePosition.center) / 2;

			_position = new Rect
			(
				middlePosition.x,
				middlePosition.y,
				0,
				0
			);

			_clippingPosition = _position.Encompass(sourceHandleEdgeCenter).Encompass(destinationHandleEdgeCenter);
		}

		#endregion



		#region Drawing

		protected virtual bool colorIfActive => true;

		public abstract Color color { get; }

		protected override bool dim => (LudiqGraphs.Configuration.dimIncompatibleNodes && canvas.isCreatingConnection) ||
		                               (LudiqGraphs.Configuration.dimInactiveNodes && !connection.destination.unit.Analysis<UnitAnalysis>(context).isEntered);
		
		protected override float dimAlphaTarget
		{
			get
			{
				var source = connection.source;
				var destination = connection.destination;
				var sourceUnit = source.unit;
				var destinationUnit = destination.unit;

				var isHiddenProxy = (sourceUnit is IUnitPortProxy sourceProxy && sourceProxy.targetType == UnitPortProxyTarget.Input) ||
				                    (destinationUnit is IUnitPortProxy destinationProxy && destinationProxy.targetType == UnitPortProxyTarget.Output);
			
				if (isHiddenProxy)
				{
					var sourceWidget = canvas.Widget<IUnitPortWidget>(source);
					var destinationWidget = canvas.Widget<IUnitPortWidget>(destination);
					var sourceUnitWidget = canvas.Widget<IUnitWidget>(sourceUnit);
					var destinationUnitWidget = canvas.Widget<IUnitWidget>(destinationUnit);

					var isHovered = sourceWidget.isMouseOver || destinationWidget.isMouseOver || sourceUnitWidget.isMouseOver || destinationUnitWidget.isMouseOver;
					var isSelected = sourceUnitWidget.isSelected || destinationUnitWidget.isSelected;
					var revealProxy = BoltFlow.Configuration.revealProxies.IsMet(isHovered, isSelected, e.alt);
					return revealProxy ? 0.5f : 0;
				}

				return base.dimAlphaTarget;
			}
		}

		public override void DrawBackground()
		{
			base.DrawBackground();

			BeginDim();

			DrawConnection();

			if (showDroplets)
			{
				DrawDroplets();
			}

			EndDim();
		}

		protected virtual void DrawConnection()
		{
			var color = this.color;

			var sourceWidget = canvas.Widget<IUnitPortWidget>(connection.source);
			var destinationWidget = canvas.Widget<IUnitPortWidget>(connection.destination);

			var highlight = !canvas.isCreatingConnection && (sourceWidget.isMouseOver || destinationWidget.isMouseOver);

			var willDisconnect = sourceWidget.willDisconnect || destinationWidget.willDisconnect;
			
			if (willDisconnect)
			{
				color = UnitConnectionStyles.disconnectColor;
			}
			else if (highlight)
			{
				color = UnitConnectionStyles.highlightColor;
			}
			else if (colorIfActive)
			{
				color = Color.Lerp(color, UnitConnectionStyles.activeColor, GraphGUI.GetActivity
				(
					connectionDebugData.lastInvokeFrame, 
					connectionDebugData.lastInvokeTime
				));
			}

			var thickness = 3;

			GraphGUI.DrawConnection
			(
				color, 
				sourceHandleEdgeCenter, 
				destinationHandleEdgeCenter, 
				sourceEdge, 
				destinationEdge, 
				cap: null, 
				capSize: Vector2.zero, 
				UnitConnectionStyles.relativeBend, 
				UnitConnectionStyles.minBend, 
				thickness
			);
		}

		#endregion



		#region Selecting

		public override bool canSelect => false;

		#endregion



		#region Dragging

		public override bool canDrag => false;

		#endregion



		#region Deleting

		public override bool canDelete => true;

		#endregion



		#region Droplets

		private readonly List<float> droplets = new List<float>();

		private float dropTime;

		private float lastInvokeTime;

		private const float handleAlignmentMargin = 0.1f;

		protected virtual bool showDroplets => true;

		protected abstract Vector2 GetDropletSize();

		protected abstract void DrawDroplet(Rect position);

		protected virtual void DrawDroplets()
		{
			foreach (var droplet in droplets)
			{
				Vector2 position;

				if (droplet < handleAlignmentMargin)
				{
					var t = droplet / handleAlignmentMargin;
					position = Vector2.Lerp(sourceHandlePosition.center, sourceHandleEdgeCenter, t);
				}
				else if (droplet > 1 - handleAlignmentMargin)
				{
					var t = (droplet - (1 - handleAlignmentMargin)) / handleAlignmentMargin;
					position = Vector2.Lerp(destinationHandleEdgeCenter, destinationHandlePosition.center, t);
				}
				else
				{
					var t = (droplet - handleAlignmentMargin) / (1 - 2 * handleAlignmentMargin);
					position = GraphGUI.GetPointOnConnection(t, sourceHandleEdgeCenter, destinationHandleEdgeCenter, sourceEdge, destinationEdge, UnitConnectionStyles.relativeBend, UnitConnectionStyles.minBend);
				}

				var size = GetDropletSize();

				using (LudiqGUI.color.Override(GUI.color * color))
				{
					DrawDroplet(new Rect(position.x - size.x / 2, position.y - size.y / 2, size.x, size.y));
				}
			}
		}

		#endregion
	}
}
