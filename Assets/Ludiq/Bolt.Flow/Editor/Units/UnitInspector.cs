﻿using Ludiq;

namespace Bolt
{
	[Inspector(typeof(IUnit))]
	public class UnitInspector : GraphElementInspector<FlowGraphContext>
	{
		public UnitInspector(Metadata metadata) : base(metadata) { }
	}
}