﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ludiq;

namespace Bolt
{
	[Widget(typeof(IMultiInputUnit))]
	public sealed class MultiInputUnitWidget : UnitWidget<IMultiInputUnit>
	{
		public MultiInputUnitWidget(FlowCanvas canvas, IMultiInputUnit unit) : base(canvas, unit) { }

		public override bool CanCreateCompatiblePort(IUnitPort port)
		{
			return port is ValueOutput valueOutput && unit.type.IsAssignableFrom(valueOutput.type) && unit.inputCount < unit.maxInputCount && unit.multiInputs.All(x => x.hasAnyConnection);
		}

		public override IUnitPort CreateCompatiblePort(ref IUnitPort port)
		{
			if (CanCreateCompatiblePort(port))
			{
				unit.inputCount++;
				unit.Define();

				var compatiblePort = unit.multiInputs[unit.multiInputs.Count - 1];
				
				return compatiblePort;
			}

			return base.CreateCompatiblePort(ref port);
		}
	}
}
