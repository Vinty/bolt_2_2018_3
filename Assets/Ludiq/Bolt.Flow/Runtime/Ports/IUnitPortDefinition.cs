﻿namespace Bolt
{
	public interface IUnitPortDefinition
	{
		string key { get; }
		string label { get; }
		string summary { get; }
		bool hideLabel { get; }
		bool primary { get; }
		bool isValid { get; }
	}
}