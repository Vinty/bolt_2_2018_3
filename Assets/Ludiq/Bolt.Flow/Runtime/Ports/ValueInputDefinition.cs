﻿using System;
using Ludiq;

namespace Bolt
{
	public sealed class ValueInputDefinition : ValuePortDefinition, IUnitInputPortDefinition
	{
		[SerializeAs(nameof(defaultValue))]
		private object _defaultvalue;

		[Inspectable]
		[DoNotSerialize]
		public override Type type
		{
			get
			{
				return base.type;
			}
			set
			{
				base.type = value;

				if (!type.IsAssignableFrom(defaultValue))
				{
					if (ValueInput.SupportsDefaultValue(type))
					{
						_defaultvalue = type.PseudoDefault();
					}
					else
					{
						hasDefaultValue = false;
						_defaultvalue = null;
					}
				}
			}
		}

		[Serialize]
		[Inspectable]
		public bool hasDefaultValue { get; set; }

		[DoNotSerialize]
		[InspectableIf(nameof(showDefaultValue))]
		[InspectorObjectType(nameof(type))]
		public object defaultValue
		{
			get => _defaultvalue;
			set
			{
				if (type == null)
				{
					throw new InvalidOperationException("A type must be defined before setting the default value.");
				}

				if (!ValueInput.SupportsDefaultValue(type))
				{
					throw new InvalidOperationException("The selected type does not support default values.");
				}

				Ensure.That(nameof(value)).IsOfType(value, type);

				_defaultvalue = value;
			}
		}

		[Serialize, InspectableIf(nameof(showNullMeansSelf)), InspectorLabel("Default to Self")]
		public bool nullMeansSelf { get; set; }

		[Serialize, InspectableIf(nameof(showAllowsNull))]
		public bool allowsNull { get; set; }
		
		private bool showDefaultValue => hasDefaultValue && type != null && ValueInput.SupportsDefaultValue(type);

		private bool showAllowsNull => type != null && type.IsNullable();

		private bool showNullMeansSelf => hasDefaultValue && type != null && ComponentHolderProtocol.IsComponentHolderType(type);
	}
}