using System;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Called when any particles in a particle system meet the conditions in the trigger module.
	/// </summary>
	[UnitCategory("Events/Physics")]
	public sealed class OnParticleTrigger : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnParticleTrigger;

		public override Type eventProxyType => typeof(ParticleTriggerEventProxy);
	}
}