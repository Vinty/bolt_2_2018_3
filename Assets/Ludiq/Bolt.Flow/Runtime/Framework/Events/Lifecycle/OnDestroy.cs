using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called before the machine is destroyed.
	/// </summary>
	[UnitCategory("Events/Lifecycle")]
	[UnitOrder(7)]
	public sealed class OnDestroy : MachineEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnDestroy;
	}
}