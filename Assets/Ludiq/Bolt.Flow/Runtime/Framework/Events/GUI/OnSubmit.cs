﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the submit button is pressed.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(24)]
	public sealed class OnSubmit : GenericGuiEventUnit
	{
		public override string hookName => EventHooks.OnSubmit;
		public override Type eventProxyType => typeof(SubmitEventProxy);
	}
}