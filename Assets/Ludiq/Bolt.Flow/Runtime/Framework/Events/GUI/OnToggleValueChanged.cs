﻿using Ludiq;
using System;
using UnityEngine.UI;

namespace Bolt
{
	/// <summary>
	/// Called when the current value of the toggle has changed.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[TypeIcon(typeof(Toggle))]
	[UnitOrder(5)]
	public sealed class OnToggleValueChanged : GameObjectEventUnit<bool>
	{
		public override string hookName => EventHooks.OnToggleValueChanged;

		public override Type eventProxyType => typeof(ToggleChangedEventProxy);

		/// <summary>
		/// The new boolean value of the toggle.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ValueOutput value { get; private set; }

		protected override void Definition()
		{
			base.Definition();
			
			value = ValueOutput<bool>(nameof(value));
		}

		protected override void AssignArguments(Flow flow, bool value)
		{
			flow.SetValue(this.value, value);
		}
	}
}