﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer presses the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(12)]
	public sealed class OnPointerDown : PointerEventUnit
	{
		public override string hookName => EventHooks.OnPointerDown;
		public override Type eventProxyType => typeof(PointerEventProxy);
	}
}