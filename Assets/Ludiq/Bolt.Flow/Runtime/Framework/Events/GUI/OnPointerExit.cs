﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer exits the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(15)]
	public sealed class OnPointerExit : PointerEventUnit
	{
		public override string hookName => EventHooks.OnPointerExit;
		public override Type eventProxyType => typeof(PointerEventProxy);
	}
}