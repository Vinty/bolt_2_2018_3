﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the cancel button is pressed.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(25)]
	public sealed class OnCancel : GenericGuiEventUnit
	{
		public override string hookName => EventHooks.OnCancel;
		public override Type eventProxyType => typeof(SubmitEventProxy);
	}
}