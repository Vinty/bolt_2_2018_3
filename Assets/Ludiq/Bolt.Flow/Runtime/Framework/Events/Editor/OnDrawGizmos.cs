﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Use to draw gizmos that are always drawn in the editor.
	/// </summary>
	[UnitCategory("Events/Editor")]
	public sealed class OnDrawGizmos : ManualEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnDrawGizmos;
	}
}