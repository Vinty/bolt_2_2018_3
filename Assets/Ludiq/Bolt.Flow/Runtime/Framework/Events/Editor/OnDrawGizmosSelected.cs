﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Use to draw gizmos that are drawn in the editor when the object is selected.
	/// </summary>
	[UnitCategory("Events/Editor")]
	public sealed class OnDrawGizmosSelected : ManualEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnDrawGizmosSelected;
	}
}