using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the renderer is no longer visible by any camera.
	/// </summary>
	[UnitCategory("Events/Rendering")]
	public sealed class OnBecameInvisible : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnBecameInvisible;
		public override Type eventProxyType => typeof(VisibilityEventProxy);
	}
}