using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the renderer became visible by any camera.
	/// </summary>
	[UnitCategory("Events/Rendering")]
	public sealed class OnBecameVisible : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnBecameVisible;
		public override Type eventProxyType => typeof(VisibilityEventProxy);
	}
}