using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the user has pressed the mouse button while over the GUI element or collider.
	/// </summary>
	[UnitCategory("Events/Input")]
	public sealed class OnMouseDown : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnMouseDown;
		public override Type eventProxyType => typeof(MouseEventProxy);
	}
}