using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the mouse enters the GUI element or collider.
	/// </summary>
	[UnitCategory("Events/Input")]
	public sealed class OnMouseEnter : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnMouseEnter;
		public override Type eventProxyType => typeof(MouseEventProxy);
	}
}