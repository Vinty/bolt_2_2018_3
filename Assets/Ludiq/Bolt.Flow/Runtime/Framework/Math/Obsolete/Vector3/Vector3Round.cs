#pragma warning disable 618

using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Rounds each component of a 3D vector.
	/// </summary>
	[UnitCategory("Math/Vector 3")]
	[UnitTitle("Round")]
	public sealed class Vector3Round : Round<Vector3, Vector3>
	{
		protected override Vector3 Floor(Vector3 input)
		{
			return input.Floor();
		}

		protected override Vector3 AwayFromZero(Vector3 input)
		{
			return input.Round();
		}

		protected override Vector3 Ceiling(Vector3 input)
		{
			return input.Ceil();
		}
	}
}