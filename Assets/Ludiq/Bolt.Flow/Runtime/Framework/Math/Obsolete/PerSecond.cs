using Ludiq;
using System;

namespace Bolt
{
	[UnitOrder(601)]
	[Obsolete("Multiply by Time.deltaTime instead.")]
	public abstract class PerSecond<T> : Unit
	{
		/// <summary>
		/// The input value.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueInput input { get; private set; }

		/// <summary>
		/// The framerate-normalized value (multiplied by delta time).
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueOutput output { get; private set; }

		protected override void Definition()
		{
			input = ValueInput(nameof(input), default(T));
			output = ValueOutput(nameof(output), Operation);

			Requirement(input, output);
		}

		public abstract T Operation(T input);

		public T Operation(Flow flow)
		{
			return Operation(flow.GetValue<T>(input));
		}
	}
}