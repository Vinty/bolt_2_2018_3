﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Returns the projection of a 2D vector on another.
	/// </summary>
	[UnitCategory("Math/Vector 2")]
	[UnitTitle("Project")]
	[Obsolete("Multiply the surface vector by the dot product of the two vectors instead.")]
	public sealed class Vector2Project : Project<UnityEngine.Vector2>
	{
		public override UnityEngine.Vector2 Operation(UnityEngine.Vector2 a, UnityEngine.Vector2 b)
		{
			return a.Project(b);
		}
	}
}