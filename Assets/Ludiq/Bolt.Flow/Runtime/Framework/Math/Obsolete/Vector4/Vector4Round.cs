#pragma warning disable 618

using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Rounds each component of a 4D vector.
	/// </summary>
	[UnitCategory("Math/Vector 4")]
	[UnitTitle("Round")]
	public sealed class Vector4Round : Round<Vector4, Vector4>
	{
		protected override Vector4 Floor(Vector4 input)
		{
			return input.Floor();
		}

		protected override Vector4 AwayFromZero(Vector4 input)
		{
			return input.Round();
		}

		protected override Vector4 Ceiling(Vector4 input)
		{
			return input.Ceil();
		}
	}
}