#pragma warning disable 618

using Ludiq;
using System.Collections.Generic;

namespace Bolt
{
	/// <summary>
	/// Returns the component-wise maximum between two or more 4D vectors.
	/// </summary>
	[UnitCategory("Math/Vector 4")]
	[UnitTitle("Maximum")]
	public sealed class Vector4Maximum : Maximum<UnityEngine.Vector4>
	{
		public override UnityEngine.Vector4 Operation(UnityEngine.Vector4 a, UnityEngine.Vector4 b)
		{
			return UnityEngine.Vector4.Max(a, b);
		}

		public override UnityEngine.Vector4 Operation(IEnumerable<UnityEngine.Vector4> values)
		{
			return MathUtility.Max(values);
		}
	}
}