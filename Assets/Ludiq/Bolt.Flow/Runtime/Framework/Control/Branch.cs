﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Branches flow by checking if a condition is true or false.
	/// </summary>
	[UnitCategory("Control")]
	[UnitOrder(0)]
	public sealed class Branch : Unit, IBranchUnit
	{
		/// <summary>
		/// The entry point for the branch.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		/// <summary>
		/// The condition to check.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueInput condition { get; private set; }

		/// <summary>
		/// The action to execute if the condition is true.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("True")]
		[UnitPortOrder(2, Axes2.Vertical)]
		[UnitPortOrder(1, Axes2.Horizontal)] // Legacy
		public ControlOutput ifTrue { get; private set; }

		/// <summary>
		/// The action to execute if the condition is false.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("False")]
		[UnitPortOrder(1, Axes2.Vertical)]
		[UnitPortOrder(2, Axes2.Horizontal)] // Legacy
		public ControlOutput ifFalse { get; private set; }

		protected override void Definition()
		{
			enter = ControlInput(nameof(enter), Enter);
			condition = ValueInput<bool>(nameof(condition));
			ifTrue = ControlOutput(nameof(ifTrue));
			ifFalse = ControlOutput(nameof(ifFalse));
			
			condition.SetDefaultValue(true);

			Requirement(condition, enter);
			Succession(enter, ifTrue);
			Succession(enter, ifFalse);
		}

		public ControlOutput Enter(Flow flow)
		{
			return flow.GetValue<bool>(condition) ? ifTrue : ifFalse;
		}
	}
}