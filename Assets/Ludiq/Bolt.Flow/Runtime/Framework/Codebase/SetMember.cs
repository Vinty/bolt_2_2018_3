﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Sets the value of a field or property via reflection.
	/// </summary>
	public sealed class SetMember : MemberUnit
	{
		private bool isValueType;

		public SetMember() : base() { }

		public SetMember(Member member) : base(member) { }

		/// <summary>
		/// Whether the target should be output to allow for chaining.
		/// </summary>
		[Serialize]
		[InspectableIf(nameof(supportsChaining))]
		public bool chainable { get; set; }

		[DoNotSerialize]
		public bool supportsChaining => canDefine && member.requiresTarget;

		[DoNotSerialize]
		[MemberFilter(Fields = true, Properties = true, ReadOnly = false)]
		public Member setter
		{
			get => member;
			set => member = value;
		}

		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput assign { get; private set; }

		[DoNotSerialize]
		[UnitPortLabel("Value")]
		[UnitPortLabelHidden]
		public ValueInput input { get; private set; }

		[DoNotSerialize]
		[UnitPortLabel("Value")]
		[UnitPortLabelHidden]
		public ValueOutput output { get; private set; }

		/// <summary>
		/// The target object used when setting the value.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("Target")]
		[UnitPrimaryPort]
		public ValueOutput targetOutput { get; private set; }

		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlOutput assigned { get; private set; }

		protected override void Definition()
		{
			base.Definition();
			
			assign = ControlInput(nameof(assign), Assign);
			assigned = ControlOutput(nameof(assigned));
			Succession(assign, assigned);
			
			if (supportsChaining && chainable)
			{
				targetOutput = ValueOutput(member.targetType, nameof(targetOutput));
				Assignment(assign, targetOutput);
			}

			output = ValueOutput(member.type, nameof(output));
			Assignment(assign, output);
		
			if (member.requiresTarget)
			{
				Requirement(target, assign);
			}

			input = ValueInput(member.type, nameof(input));
			Requirement(input, assign);

			if (member.allowsNull)
			{
				input.AllowsNull();
			}

			input.SetDefaultValue(member.type.PseudoDefault());

			isValueType = member.type.IsValueType;
		}

		protected override bool IsMemberValid(Member member)
		{
			return member.isAccessor && member.isSettable;
		}

		private ControlOutput Assign(Flow flow)
		{
			UpdateTarget(flow);

			var value = flow.GetConvertedValue(input);

			member.Set(value);

			flow.SetValue(output, value);

			if (supportsChaining && chainable)
			{
				flow.SetValue(targetOutput, member.target);
			}

			return assigned;
		}
	}
}