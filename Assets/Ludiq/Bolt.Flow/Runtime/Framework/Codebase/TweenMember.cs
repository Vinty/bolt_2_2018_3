﻿using Ludiq;
using DG;
using DG.Tweening;
using UnityEngine;
using System;
using System.Collections.Generic;
using DG.Tweening.Core;

namespace Bolt
{
	/// <summary>
	/// Tweens the value of a field or property via reflection. (Requires DOTween library.)
	/// </summary>
	public sealed class TweenMember : MemberUnit
	{
		public enum EasingMode
		{
			Predefined,
			AnimationCurve,
		}

		public TweenMember() { }

		public TweenMember(Member member) : base(member) { }

		[DoNotSerialize]
		[MemberFilter(Fields = true, Properties = true, ReadOnly = false, WriteOnly = false)]
		public Member field
		{
			get => member;
			set => member = value;
		}

		/// <summary>
		/// The kind of easing to use for this tween.
		/// </summary>
		[Serialize]
		[Inspectable]
		public EasingMode easingMode { get; set; } = EasingMode.Predefined;

		/// <summary>
		/// Whether or not to show advanced options for this tween.
		/// </summary>
		[Serialize]
		[Inspectable]
		public bool advanced { get; set; } = false;

		/// <summary>
		/// The entry point to start the tween.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		/// <summary>
		/// The value to tween towards.
		/// </summary>
		[UnitPortLabel("Target")]
		[DoNotSerialize]
		public ValueInput value { get; private set; }

		/// <summary>
		/// The duration of the tween to perform.
		/// </summary>
		[DoNotSerialize]
		public ValueInput duration { get; private set; }

		/// <summary>
		/// The duration of the tween to perform.
		/// </summary>
		[DoNotSerialize]
		public ValueInput ease { get; private set; }

		/// <summary>
		/// Number of times to loop the tween.
		/// </summary>
		[DoNotSerialize]
		public ValueInput loops { get; private set; }

		/// <summary>
		/// How to loop the tween.
		/// </summary>
		[DoNotSerialize]
		public ValueInput loopType { get; private set; }

		/// <summary>
		/// Whether or not this tween should relative.
		/// </summary>
		[DoNotSerialize]
		public ValueInput id { get; private set; }

		/// <summary>
		/// Whether or not this tween should relative.
		/// </summary>
		[DoNotSerialize]
		public ValueInput relative { get; private set; }

		/// <summary>
		/// Whether or not this tween should be auto-killed.
		/// </summary>
		[DoNotSerialize]
		public ValueInput autokill { get; private set; }

		/// <summary>
		/// Whether or not this tween is recyclable.
		/// </summary>
		[DoNotSerialize]
		public ValueInput recyclable { get; private set; }

		/// <summary>
		/// The type of update event to execute for this tween.
		/// </summary>
		[DoNotSerialize]
		public ValueInput updateType { get; private set; }
		
		/// <summary>
		/// The action to execute immediately after the tween has been started.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput exit { get; private set; }

		/// <summary>
		/// The action to execute when the tween is fully completed, after all loops.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onComplete { get; private set; }

		/// <summary>
		/// The action to execute when the tween is killed.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onKill { get; private set; }

		/// <summary>
		/// The action to execute when the tween is set to a playing state, or resumed from a paused state.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onPlay { get; private set; }

		/// <summary>
		/// The action to execute when the tween state changes from playing to paused.
		/// If the tween has autokill set to false, this is also called when the tween reaches completion.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onPause { get; private set; }

		/// <summary>
		/// The action to execute when the twewen is rewinded manually.
		/// Also fires when the tween reaches the start position when playing backwards.
		/// Not fired again on a tween that is already rewinded is asked to rewind.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onRewind { get; private set; }

		/// <summary>
		/// The action to execute when the tween actually starts.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onStart { get; private set; }

		/// <summary>
		/// The action to execute when a tween completes a single loop cycle.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput onStepComplete { get; private set; }

		/// <summary>
		/// The action to execute every time the tween gets updated.
		/// </summary>
		[DoNotSerialize]
		public ControlOutput update { get; private set; }

		/// <summary>
		/// The tween that is created after this unit has been entered.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort(Axes2.Both)]
		[UnitPortLabelHidden]
		public ValueOutput tween { get; private set; }

		public override bool canDefine => base.canDefine && member != null && tweenFactoriesByType.ContainsKey(member.type);

		protected override void Definition()
		{
			base.Definition();
			
			enter = ControlInput(nameof(enter), Enter);

			if (member.type == typeof(Quaternion))
			{
				value = ValueInput(typeof(Vector3), nameof(value));
			}
			else
			{
				value = ValueInput(member.type, nameof(value));
			}

			if (member.allowsNull)
			{
				value.AllowsNull();
			}

			value.SetDefaultValue(value.type.PseudoDefault());

			duration = ValueInput(nameof(duration), 1f);

			switch (easingMode)
			{
				case EasingMode.Predefined: ease = ValueInput(nameof(ease), Ease.Linear); break;
				case EasingMode.AnimationCurve: ease = ValueInput(nameof(ease), AnimationCurve.Linear(0, 0, 1, 1)); break;
				default: throw new UnexpectedEnumValueException<EasingMode>(easingMode);
			}

			exit = ControlOutput(nameof(exit));
			onComplete = ControlOutput(nameof(onComplete));
			tween = ValueOutput(typeof(Tween), nameof(tween));

			// TODO: mark other all pcontrol outputs as succession of enter? since you need to enter before any other event will be fired...
			// but on the other hand, only 'exit' is a true successor of 'enter', as in the very next thing to happen.

			Requirement(value, enter);
			Requirement(duration, enter);
			Requirement(ease, enter);
			Succession(enter, exit);
			Succession(enter, onComplete);
			Assignment(enter, tween);

			if (advanced)
			{
				loops = ValueInput(nameof(loops), 0);
				loopType = ValueInput(nameof(loopType), LoopType.Restart);
				id = ValueInput(nameof(id), "").AllowsNull();
				relative = ValueInput(nameof(relative), false);
				autokill = ValueInput(nameof(autokill), true);
				recyclable = ValueInput(nameof(recyclable), false);
				updateType = ValueInput(nameof(updateType), UpdateType.Normal);

				onKill = ControlOutput(nameof(onKill));
				onPlay = ControlOutput(nameof(onPlay));
				onPause = ControlOutput(nameof(onPause));
				onRewind = ControlOutput(nameof(onRewind));
				onStart = ControlOutput(nameof(onStart));
				onStepComplete = ControlOutput(nameof(onStepComplete));
				update = ControlOutput(nameof(update));

				Requirement(loops, enter);
				Requirement(loopType, enter);
				Requirement(id, enter);
				Requirement(relative, enter);
				Requirement(autokill, enter);
				Requirement(recyclable, enter);
				Requirement(updateType, enter);
			}

			if (member.requiresTarget)
			{
				Requirement(target, enter);
			}
		}

		protected override bool IsMemberValid(Member member)
		{
			return member.isAccessor && member.isGettable;
		}

		// csc seems to have trouble resolving the overloads in some cases here, making documentation generation fail.
		public static Dictionary<Type, Func<Member, object, float, Tweener>> tweenFactoriesByType = new Dictionary<Type, Func<Member, object, float, Tweener>> {
			{ typeof(float), (member, endValue, duration) => DOTween.To(member.Get<float>, x => member.Set(x), (float) endValue, duration) },
			{ typeof(double), (member, endValue, duration) => DOTween.To(member.Get<double>, x => member.Set(x), (double) endValue, duration) },
			{ typeof(int), (member, endValue, duration) => DOTween.To(member.Get<int>, x => member.Set(x), (int) endValue, duration) },
			{ typeof(uint), (member, endValue, duration) => DOTween.To(member.Get<uint>, x => member.Set(x), (uint) endValue, duration) },
			{ typeof(long), (member, endValue, duration) => DOTween.To(member.Get<long>, x => member.Set(x), (long) endValue, duration) },
			{ typeof(ulong), (member, endValue, duration) => DOTween.To(member.Get<ulong>, x => member.Set(x), (ulong) endValue, duration) },
			{ typeof(Vector2), (member, endValue, duration) => DOTween.To(member.Get<Vector2>, x => member.Set(x), (Vector2) endValue, duration) },
			{ typeof(Vector3), (member, endValue, duration) => DOTween.To((DOGetter<Vector3>)(member.Get<Vector3>), x => member.Set(x), (Vector3) endValue, duration) }, // Do not remove disambiguation
			{ typeof(Vector4), (member, endValue, duration) => DOTween.To(member.Get<Vector4>, x => member.Set(x), (Vector4) endValue, duration) },
			{ typeof(Color), (member, endValue, duration) => DOTween.To(member.Get<Color>, x => member.Set(x), (Color) endValue, duration) },
			{ typeof(Rect), (member, endValue, duration) => DOTween.To(member.Get<Rect>, x => member.Set(x), (Rect) endValue, duration) },
			{ typeof(RectOffset), (member, endValue, duration) => DOTween.To(member.Get<RectOffset>, x => member.Set(x), (RectOffset) endValue, duration) },
			{ typeof(Quaternion), (member, endValue, duration) => DOTween.To((DOGetter<Quaternion>)member.Get<Quaternion>, x => member.Set(x), (Vector3) endValue, duration) }, // Do not remove disambiguation
		};

		private ControlOutput Enter(Flow flow)
		{
			UpdateTarget(flow);

			var tween = tweenFactoriesByType[member.type].Invoke(member, flow.GetValue(value), flow.GetValue<float>(duration));

			var reference = flow.stack.ToReference();

			switch (easingMode)
			{
				case EasingMode.Predefined: tween.SetEase(flow.GetValue<Ease>(ease)); break;
				case EasingMode.AnimationCurve: tween.SetEase(flow.GetValue<AnimationCurve>(ease)); break;
				default: throw new UnexpectedEnumValueException<EasingMode>(easingMode);
			}

			if (onComplete.hasValidConnection)
			{
				tween.OnComplete(() => InvokeDelegate(reference, onComplete, tween));
			}

			if (advanced)
			{
				tween.SetLoops(flow.GetValue<int>(loops), flow.GetValue<LoopType>(loopType))					 
					 .SetRelative(flow.GetValue<bool>(relative))
					 .SetAutoKill(flow.GetValue<bool>(autokill))
					 .SetRecyclable(flow.GetValue<bool>(recyclable))
					 .SetUpdate(flow.GetValue<UpdateType>(updateType));

				var id = flow.GetValue<string>(this.id);
				if (string.IsNullOrEmpty(id))
				{
					tween.SetId(id);
				}

				if (onKill.hasValidConnection)
				{
					 tween.OnKill(() => InvokeDelegate(reference, onKill, tween));
				}

				if (onPlay.hasValidConnection)
				{
					 tween.OnPlay(() => InvokeDelegate(reference, onPlay, tween));
				}

				if (onPause.hasValidConnection)
				{
					 tween.OnPause(() => InvokeDelegate(reference, onPause, tween));
				}

				if (onRewind.hasValidConnection)
				{
					 tween.OnRewind(() => InvokeDelegate(reference, onRewind, tween));
				}

				if (onStart.hasValidConnection)
				{
					 tween.OnStart(() => InvokeDelegate(reference, onStart, tween));
				}

				if (onStepComplete.hasValidConnection)
				{
					 tween.OnStepComplete(() => InvokeDelegate(reference, onStepComplete, tween));
				}

				if (update.hasValidConnection)
				{
					 tween.OnUpdate(() => InvokeDelegate(reference, update, tween));
				}
			}

			flow.SetValue(this.tween, tween);

			return exit;
		}

		private void InvokeDelegate(GraphReference reference, ControlOutput controlOutput, Tween tween)
		{
			using (var flow = Flow.New(reference))
			{
				flow.SetValue(this.tween, tween);
				flow.Invoke(controlOutput);
			}
		}

		private object Value(Flow flow)
		{
			UpdateTarget(flow);

			return member.Get();
		}
	}
}