﻿using System.Collections;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Delays flow by waiting while a condition is true.
	/// </summary>
	[UnitTitle("Wait While")]
	[UnitShortTitle("Wait While")]
	[UnitOrder(3)]
	public class WaitWhileUnit : WaitUnit
	{
		/// <summary>
		/// The condition to check.
		/// </summary>
		[DoNotSerialize]
		public ValueInput condition { get; private set; }

		protected override void Definition()
		{
			base.Definition();

			condition = ValueInput<bool>(nameof(condition));
			Requirement(condition, enter);
		}

		protected override IEnumerator Await(Flow flow)
		{
			var reference = flow.stack.ToReference();

			yield return new WaitWhile(() => Flow.FetchValue<bool>(condition, reference));
			
			yield return exit;
		}
	}
}