﻿using System;
using System.Collections;
using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Removes the specified item from a list.
	/// </summary>
	[UnitCategory("Collections/Lists")]
	[UnitSurtitle("List")]
	[UnitShortTitle("Remove Item")]
	[UnitOrder(4)]
	public sealed class RemoveListItem : Unit
	{
		/// <summary>
		/// The entry point for the node.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		/// <summary>
		/// The list.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("List")]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueInput listInput { get; private set; }

		/// <summary>
		/// The list without the removed item.
		/// Note that the input list is modified directly and then returned,
		/// except if it is an array, in which case a new array without the item
		/// is returned instead.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("List")]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueOutput listOutput { get; private set; }

		/// <summary>
		/// The item to remove.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ValueInput item { get; private set; }

		/// <summary>
		/// The action to execute once the item has been removed.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlOutput exit { get; private set; }

		protected override void Definition()
		{
			enter = ControlInput(nameof(enter), Remove);
			listInput = ValueInput<IList>(nameof(listInput));
			listOutput = ValueOutput<IList>(nameof(listOutput));
			item = ValueInput<object>(nameof(item));
			exit = ControlOutput(nameof(exit));

			Requirement(listInput, enter);
			Requirement(item, enter);
			Assignment(enter, listOutput);
			Succession(enter, exit);
		}

		public ControlOutput Remove(Flow flow)
		{
			var list = flow.GetValue<IList>(listInput);
			var item = flow.GetValue<object>(this.item);

			flow.SetValue(listOutput, ListUtility.Remove(list, item));

			return exit;
		}
	}
}