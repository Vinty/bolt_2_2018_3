﻿using System;
using Ludiq;

namespace Bolt
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class UnitPortOrderAttribute : Attribute
	{
		public UnitPortOrderAttribute(int order, Axes2 axes = Axes2.Both)
		{
			this.order = order;
			this.axes = axes;
		}

		public int order { get; }
		public Axes2 axes { get; }
	}
}