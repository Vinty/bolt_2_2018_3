﻿using System;
using Ludiq;

namespace Bolt
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public sealed class UnitPrimaryPortAttribute : Attribute
	{
		public UnitPrimaryPortAttribute(Axes2 axes = Axes2.Both, bool showLabel = false)
		{
			this.axes = axes;
			this.showLabel = showLabel;
		}

		public Axes2 axes { get; }
		public bool showLabel { get; }
	}
}