﻿using System;

namespace Bolt
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class UnitTitleHiddenAttribute : Attribute
	{ }
}