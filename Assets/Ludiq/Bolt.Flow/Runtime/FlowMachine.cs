﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[AddComponentMenu("Bolt/Flow Machine")]
	[RequireComponent(typeof(Variables))]
	[DisableAnnotation]
	public sealed class FlowMachine : EventMachine<FlowGraph, FlowMacro>
	{
		protected override void OnEnable()
		{
			if (hasGraph)
			{
				using (var stack = reference.ToStackPooled())
				{
					graph.StartListening(stack);
				}
			}

			base.OnEnable();
		}

		protected override void OnDisable()
		{
			if (destroyedDuringSwap)
			{
				return;
			}

			base.OnDisable();

			if (hasGraph)
			{
				using (var stack = reference.ToStackPooled())
				{
					graph.StopListening(stack);
				}
			}
		}

		[ContextMenu("Show Data...")]
		protected override void ShowData()
		{
			base.ShowData();
		}
	}
}
