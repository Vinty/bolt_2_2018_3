﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public sealed class FlowMacro : Macro<FlowGraph>
	{
		[ContextMenu("Show Data...")]
		protected override void ShowData()
		{
			base.ShowData();
		}
	}
}