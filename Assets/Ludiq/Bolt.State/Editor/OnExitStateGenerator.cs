﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(OnExitState))]
	public class OnExitStateGenerator : ManualEventUnitGenerator<OnExitState, EmptyEventArgs>
	{
		public OnExitStateGenerator(OnExitState unit) : base(unit) {}

		protected override CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context) => CodeFactory.ThisRef.Method("GetRootFlowGraphScript").Invoke();
		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();
		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context) => unit.trigger.GenerateStatements(context);
	}
}
