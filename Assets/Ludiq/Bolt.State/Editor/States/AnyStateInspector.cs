﻿using System.Reflection;
using Ludiq;

namespace Bolt
{
	[Inspector(typeof(AnyState))]
	public class AnyStateInspector : StateInspector
	{
		public AnyStateInspector(Metadata metadata) : base(metadata) { }

		protected override bool Include(MemberInfo m)
		{
			return false;
		}
	}
}