﻿using Ludiq;

namespace Bolt
{
	[Inspector(typeof(IState))]
	public class StateInspector : GraphElementInspector<StateGraphContext>
	{
		public StateInspector(Metadata metadata) : base(metadata) { }
	}
}