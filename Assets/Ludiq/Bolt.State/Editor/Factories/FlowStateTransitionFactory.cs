﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[GraphFactory(typeof(FlowStateTransition))]
	public class FlowStateTransitionFactory : GraphFactory<FlowStateTransition, FlowGraph>
	{
		public FlowStateTransitionFactory(FlowStateTransition parent) : base(parent) { }
		
		public override FlowGraph DefaultGraph()
		{
			return GraphWithDefaultTrigger();
		}
		
		public static FlowStateTransition WithDefaultTrigger(IState source, IState destination)
		{
			var flowStateTransition = new FlowStateTransition(source, destination);
			flowStateTransition.nest.source = GraphSource.Embed;
			flowStateTransition.nest.embed = GraphWithDefaultTrigger();
			return flowStateTransition;
		}

		public static FlowGraph GraphWithDefaultTrigger()
		{
			var graph = new FlowGraph();

			graph.controlAxis = BoltFlow.Configuration.defaultControlAxis;

			var transition = new TriggerStateTransition();

			graph.units.Add(transition);

			switch (graph.controlAxis)
			{
				case Axis2.Horizontal:
					transition.position = new Vector2(100, -50);
					break;
				case Axis2.Vertical:
					transition.position = new Vector2(-80, 60);
					break;
				default:
					throw new UnexpectedEnumValueException<Axis2>(graph.controlAxis);
			}

			return graph;
		}
	}
}
