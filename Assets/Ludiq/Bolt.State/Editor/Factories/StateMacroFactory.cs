﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[GraphFactory(typeof(StateMacro))]
	public class StateMacroFactory : GraphFactory<StateMacro, StateGraph>
	{
		public StateMacroFactory(StateMacro parent) : base(parent) { }

		public override StateGraph DefaultGraph()
		{
			return StateGraphFactory.WithStart();
		}
		
		[MenuItem("Assets/Create/Bolt/State Macro")]
		public static void CreateStateMacroInProject()
		{
			var macro = ScriptableObject.CreateInstance<StateMacro>();
			macro.graph = StateGraphFactory.WithStart();
			ProjectWindowUtil.CreateAsset(macro, "New State Macro.asset");
		}
	}
}
