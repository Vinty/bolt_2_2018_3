﻿using UnityEngine;

namespace Bolt
{
	public static class StateGraphFactory
	{
		public static StateGraph Blank()
		{
			return new StateGraph();
		}

		public static StateGraph WithStart()
		{
			var stateGraph = Blank();

			var startState = FlowStateFactory.WithEnterUpdateExit();
			startState.isStart = true;
			startState.nest.embed.title = "Start";
			startState.position = new Vector2(-86, -15);

			stateGraph.states.Add(startState);

			return stateGraph;
		}
	}
}
