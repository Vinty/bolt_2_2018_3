﻿using Ludiq;

namespace Bolt
{
	[GraphFactory(typeof(StateUnit))]
	public class StateUnitFactory : GraphFactory<StateUnit, StateGraph>
	{
		public StateUnitFactory(StateUnit parent) : base(parent) { }

		public override StateGraph DefaultGraph()
		{
			return StateGraphFactory.WithStart();
		}

		public static StateUnit WithStart()
		{
			var stateUnit = new StateUnit();
			stateUnit.nest.source = GraphSource.Embed;
			stateUnit.nest.embed = StateGraphFactory.WithStart();
			return stateUnit;
		}
	}
}
