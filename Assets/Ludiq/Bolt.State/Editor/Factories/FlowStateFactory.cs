﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[GraphFactory(typeof(FlowState))]
	public class FlowStateFactory : GraphFactory<FlowState, FlowGraph>
	{
		public FlowStateFactory(FlowState parent) : base(parent) { }

		public override FlowGraph DefaultGraph()
		{
			return GraphWithEnterUpdateExit();
		}
		
		public static FlowState WithEnterUpdateExit()
		{
			var flowState = new FlowState();
			flowState.nest.source = GraphSource.Embed;
			flowState.nest.embed = GraphWithEnterUpdateExit();
			return flowState;
		}

		public static FlowGraph GraphWithEnterUpdateExit()
		{
			var graph = new FlowGraph();

			graph.controlAxis = BoltFlow.Configuration.defaultControlAxis;

			var enter = new OnEnterState();
			var update = new Update();
			var exit = new OnExitState();

			graph.units.Add(enter);
			graph.units.Add(update);
			graph.units.Add(exit);

			switch (graph.controlAxis)
			{
				case Axis2.Horizontal:
					enter.position = new Vector2(-205, -215);
					update.position = new Vector2(-161, -38);
					exit.position = new Vector2(-205, 145);
					break;
				case Axis2.Vertical:
					enter.position = new Vector2(-285, -30);
					update.position = new Vector2(-50, -30);
					exit.position = new Vector2(135, -30);
					break;
				default:
					throw new UnexpectedEnumValueException<Axis2>(graph.controlAxis);
			}

			return graph;
		}
	}
}
