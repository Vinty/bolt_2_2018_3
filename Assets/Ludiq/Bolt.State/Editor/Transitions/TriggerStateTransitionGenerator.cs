﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(TriggerStateTransition))]
	public class TriggerStateTransitionGenerator : UnitGenerator<TriggerStateTransition>
	{
		public TriggerStateTransitionGenerator(TriggerStateTransition unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.trigger)
			{
				yield return CodeFactory.ThisRef.Field("parent").Cast(CodeFactory.TypeRef(typeof(StateTransitionScript))).Method("Branch").Invoke().Statement();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
