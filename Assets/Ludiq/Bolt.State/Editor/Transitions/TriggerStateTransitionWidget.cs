﻿using Ludiq;

namespace Bolt
{
	[Widget(typeof(TriggerStateTransition))]
	public sealed class TriggerStateTransitionWidget : UnitWidget<TriggerStateTransition>
	{
		public TriggerStateTransitionWidget(FlowCanvas canvas, TriggerStateTransition unit) : base(canvas, unit) { }
		
		protected override NodeColorMix baseColor => NodeColorMix.TealReadable;
		
		protected override void OnDoubleClick()
		{
			if (unit.graph.zoom == 1)
			{
				var parentReference = reference.ParentReference(false);

				if (parentReference != null)
				{
					if (e.ctrlOrCmd)
					{
						GraphWindow.OpenTab(parentReference);
					}
					else
					{
						window.reference = parentReference;
					}
				}

				e.Use();
			}
			else
			{
				base.OnDoubleClick();
			}
		}
	}
}