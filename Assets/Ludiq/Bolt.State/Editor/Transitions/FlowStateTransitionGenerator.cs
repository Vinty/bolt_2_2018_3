﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt.States
{
	[Generator(typeof(FlowStateTransition))]
	public class FlowStateTransitionGenerator : StateTransitionGenerator<FlowStateTransition>
	{
		public FlowStateTransitionGenerator(FlowStateTransition transition) : base(transition) {}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			var generatedChildGraph = context.generationSystem.GenerateGraph(transition);

			context.requiredMachineEvents.AddRange(generatedChildGraph.graphScript.requiredMachineEvents);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context)
		{
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(FlowStateTransitionScript)), context.ExpectMemberName(transition, originalMemberName));
		}

		public override IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(transition);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.InstanceField(transition, originalMemberName).Assign(CodeFactory.TypeRef(typeof(FlowStateTransitionScript)).ObjectCreate(
				CodeFactory.TypeRef(childGraphScriptClassName).ObjectCreate(CodeFactory.VarRef(context.argumentLocals["machineScript"].Name)),
				context.InstanceField(transition.source, "stateScript"),
				context.InstanceField(transition.destination, "stateScript")
			)).Statement();

			yield return context.InstanceField(transition.source, "stateScript").Field("outgoingTransitionScripts").Method("Add").Invoke(context.InstanceField(transition, originalMemberName)).Statement();
			yield return context.InstanceField(transition.destination, "stateScript").Field("incomingTransitionScripts").Method("Add").Invoke(context.InstanceField(transition, originalMemberName)).Statement();
		}

		public override IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(transition);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.InstanceField(transition, originalMemberName).Field("graphData").Assign(
				CodeFactory.VarRef("value").Method("CreateChildGraphData").Invoke(
					CodeFactory.VarRef("value").Field("definition").Field("elements")
						.Index(CodeFactory.TypeRef(typeof(Guid)).ObjectCreate(CodeFactory.Primitive(transition.guid.ToString())))
						.Cast(CodeFactory.TypeRef(typeof(FlowStateTransition)))
				).Cast(CodeFactory.TypeRef(typeof(FlowGraphData)))
			).Statement();
		}
	}
}
