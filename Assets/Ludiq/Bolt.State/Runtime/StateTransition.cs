using System;
using Ludiq;

namespace Bolt
{
	public abstract class StateTransition : GraphElement<StateGraph>, IStateTransition
	{
		public class DebugData : IStateTransitionDebugData
		{
			public Exception runtimeException { get; set; }

			public int? lastBranchFrame { get;set; }

			public float? lastBranchTime { get;set; }
		}
		
		protected StateTransition() { }

		protected StateTransition(IState source, IState destination)
		{
			Ensure.That(nameof(source)).IsNotNull(source);
			Ensure.That(nameof(destination)).IsNotNull(destination);

			if (source.graph != destination.graph)
			{
				throw new NotSupportedException("Cannot create transitions across state graphs.");
			}

			this.source = source;
			this.destination = destination;
		}

		public IGraphElementDebugData CreateDebugData()
		{
			return new DebugData();
		}

		public override int dependencyOrder => 1;

		[Serialize]
		public IState source { get; internal set; }

		[Serialize]
		public IState destination { get; internal set; }

		protected override void StartListening(GraphReference instance)
		{
			if (this is IGraphEventListener listener && instance.GetElementData<State.Data>(source).isActive)
			{
				listener.StartListening(instance);
			}
		}

		protected override void StopListening(GraphReference instance)
		{
			if (this is IGraphEventListener listener)
			{
				listener.StopListening(instance);
			}
		}



		#region Lifecycle
		
		public void Branch(Flow flow)
		{
			if (flow.enableDebug)
			{
				var editorData = flow.stack.GetElementDebugData<DebugData>(this);

				editorData.lastBranchFrame = EditorTimeBinding.frame;
				editorData.lastBranchTime = EditorTimeBinding.time;
			}

			try
			{
				source.OnExit(flow);
			}
			catch (Exception ex)
			{
				source.HandleException(flow.stack, ex);
				throw;
			}

			source.OnBranchTo(flow, destination);

			try
			{
				destination.OnEnter(flow);
			}
			catch (Exception ex)
			{
				destination.HandleException(flow.stack, ex);
				throw;
			}
		}

		public abstract void BeforeEnter(Flow flow);

		public abstract void OnEnter(Flow flow);

		public abstract void BeforeExit(Flow flow);

		public abstract void OnExit(Flow flow);

		#endregion
	}
}