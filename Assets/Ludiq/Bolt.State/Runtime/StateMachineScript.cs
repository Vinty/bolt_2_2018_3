﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	public abstract class StateMachineScript : MachineScript<StateGraph, StateGraphData, StateMachineScript, StateGraphScript>
	{
		public StateMachineScript() {}

		protected override void OnEnable() 
		{
			graphScript.Start();
		}

		protected override void OnDisable()
		{
			graphScript.Stop();
		}
	}
}
