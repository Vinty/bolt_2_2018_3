using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public abstract class State : GraphElement<StateGraph>, IState
	{
		public class Data : IGraphElementData
		{
			public bool isActive;
		}

		public class DebugData : IStateDebugData
		{
			public int? lastEnterFrame { get; set; }
			
			public float? lastExitTime { get; set; }
			
			public Exception runtimeException { get; set; }
		}

		public IGraphElementData CreateData()
		{
			return new Data();
		}

		public IGraphElementDebugData CreateDebugData()
		{
			return new DebugData();
		}
		
		[Serialize]
		public bool isStart { get; set; }

		/// <summary>
		/// If enabled, the state will receive Enter events
		/// even if it was already active before.
		/// </summary>
		[Serialize, Inspectable]
		public bool canReenter { get; set; }

		[DoNotSerialize]
		public virtual bool canToggleStart => true;

		[DoNotSerialize]
		public virtual bool canBeSource => true;

		[DoNotSerialize]
		public virtual bool canBeDestination => true;
		
		public override void BeforeRemove()
		{
			base.BeforeRemove();

			Disconnect();
		}
		
		protected override void StartListening(GraphReference instance)
		{
			if (this is IGraphEventListener listener && instance.GetElementData<Data>(this).isActive)
			{
				listener.StartListening(instance);
			}
		}

		protected override void StopListening(GraphReference instance)
		{
			if (this is IGraphEventListener listener)
			{
				listener.StopListening(instance);
			}
		}



		#region Poutine

		protected void CopyFrom(State source)
		{
			base.CopyFrom(source);

			isStart = source.isStart;
			width = source.width;
		}

		#endregion

		#region Transitions

		public IEnumerable<IStateTransition> outgoingTransitions => graph?.transitions.WithSource(this) ?? Enumerable.Empty<IStateTransition>();

		public IEnumerable<IStateTransition> incomingTransitions => graph?.transitions.WithDestination(this) ?? Enumerable.Empty<IStateTransition>();

		protected List<IStateTransition> outgoingTransitionsNoAlloc => graph?.transitions.WithSourceNoAlloc(this) ?? Empty<IStateTransition>.list;

		public IEnumerable<IStateTransition> transitions => LinqUtility.Concat<IStateTransition>(outgoingTransitions, incomingTransitions);

		public void Disconnect()
		{
			foreach (var transition in transitions.ToArray())
			{
				graph.transitions.Remove(transition);
			}
		}

		#endregion

		#region Lifecycle

		public virtual void OnEnter(Flow flow)
		{
			var data = flow.stack.GetElementData<Data>(this);

			if (data.isActive && !canReenter)
			{
				return;
			}

			data.isActive = true;

			foreach (var transition in outgoingTransitionsNoAlloc)
			{
				try
				{
					transition.BeforeEnter(flow);
				}
				catch (Exception ex)
				{
					transition.HandleException(flow.stack, ex);
					throw;
				}
			}

			if (flow.enableDebug)
			{
				var editorData = flow.stack.GetElementDebugData<DebugData>(this);

				editorData.lastEnterFrame = EditorTimeBinding.frame;
			}

			OnEnterImplementation(flow);

			foreach (var transition in outgoingTransitionsNoAlloc)
			{
				try
				{
					transition.OnEnter(flow);
				}
				catch (Exception ex)
				{
					transition.HandleException(flow.stack, ex);
					throw;
				}
			}
		}
		
		public virtual void OnExit(Flow flow)
		{
			var data = flow.stack.GetElementData<Data>(this);

			if (!data.isActive)
			{
				return;
			}

			foreach (var transition in outgoingTransitionsNoAlloc)
			{
				try
				{
					transition.BeforeExit(flow);
				}
				catch (Exception ex)
				{
					transition.HandleException(flow.stack, ex);
					throw;
				}
			}

			OnExitImplementation(flow);

			data.isActive = false;
			
			if (flow.enableDebug)
			{
				var editorData = flow.stack.GetElementDebugData<DebugData>(this);

				editorData.lastExitTime = EditorTimeBinding.time;
			}

			foreach (var transition in outgoingTransitionsNoAlloc)
			{
				try
				{
					transition.OnExit(flow);
				}
				catch (Exception ex)
				{
					transition.HandleException(flow.stack, ex);
					throw;
				}
			}
		}

		protected virtual void OnEnterImplementation(Flow flow) { }

		// FIXME: andy: do we need this? not referenced anywhere outside this class
		protected virtual void UpdateImplementation(Flow flow) { }

		// FIXME: andy: do we need this? not referenced anywhere outside this class.
		protected virtual void FixedUpdateImplementation(Flow flow) { }

		// FIXME: andy: do we need this? not referenced anywhere outside this class
		protected virtual void LateUpdateImplementation(Flow flow) { }

		protected virtual void OnExitImplementation(Flow flow) { }

		public virtual void OnBranchTo(Flow flow, IState destination) { }

		#endregion

		#region Widget

		public const float DefaultWidth = 170;

		[Serialize]
		public Vector2 position { get; set; }

		[Serialize]
		public float width { get; set; } = DefaultWidth;

		#endregion
	}
}