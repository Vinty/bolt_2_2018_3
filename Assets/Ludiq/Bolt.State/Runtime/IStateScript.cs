﻿using System.Collections.Generic;

namespace Bolt
{
	public interface IStateScript
	{
		bool isActive { get; }
		bool canReenter { get; }
		List<IStateTransitionScript> incomingTransitionScripts { get; }
		List<IStateTransitionScript> outgoingTransitionScripts { get; }

		void OnEnter();
		void OnExit();
		void OnBranchTo(IStateScript destination);
	}
}
