﻿using System;
using System.Collections.Generic;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	[DisableAnnotation]
	public abstract class Macro<TGraph> : LudiqScriptableObject, IMacro
		where TGraph : class, IGraph, new()
	{
		[SerializeAs(nameof(graph))]
		private TGraph _graph = new TGraph();

		[DoNotSerialize]
		public TGraph graph
		{
			get => _graph;
			set
			{
				if (value == null)
				{
					throw new InvalidOperationException("Macros must have a graph.");
				}

				if (value == graph)
				{
					return;
				}

				_graph = value;
			}
		}

		[DoNotSerialize]
		IGraph IMacro.graph
		{
			get => graph;
			set => graph = (TGraph)value;
		}

		[DoNotSerialize]
		IGraph IGraphParent.childGraph => graph;

		[DoNotSerialize]
		public IEnumerable<object> aotStubs => graph.aotStubs;

		[DoNotSerialize]
		bool IGraphParent.isSerializationRoot => true;

		[DoNotSerialize]
		UnityObject IGraphParent.serializedObject => this;

		public bool isDescriptionValid
		{
			get => true;
			set { }
		}

		protected override void OnAfterDeserialize()
		{
			base.OnAfterDeserialize();

			Serialization.NotifyDependencyDeserialized(this);
		}
	}
}