﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public sealed class MergedGraphElementCollection : MergedKeyedCollection<Guid, IGraphElement>, INotifyCollectionChanged<IGraphElement>
	{
		public event Action<IGraphElement> ItemAdded;

		public event Action<IGraphElement> ItemRemoved;

		public event Action CollectionChanged;

		public override void Include<TSubItem>(IKeyedCollection<Guid, TSubItem> collection)
		{
			base.Include(collection);

			var graphElementCollection = collection as IGraphElementCollection<TSubItem>;

			if (graphElementCollection != null)
			{
				graphElementCollection.ItemAdded += (element) => ItemAdded?.Invoke(element);
				graphElementCollection.ItemRemoved += (element) => ItemRemoved?.Invoke(element);
				graphElementCollection.CollectionChanged += () => CollectionChanged?.Invoke();
			}
		}

		public void AddRangeByDependencies(List<IGraphElement> _elements)
		{
			// Has to be lean and fast because we're doing this during serialization

			// _elements.OrderBy(e => e.dependencyOrder)
			var sortedElements = ListPool<IGraphElement>.New();
			foreach (var element in _elements)
			{
				sortedElements.Add(element);
			}
			sortedElements.Sort((a, b) => a.dependencyOrder.CompareTo(b.dependencyOrder));

			foreach (var element in sortedElements)
			{
				try
				{
					if (!element.HandleDependencies())
					{
						continue;
					}

					Add(element);
				}
				catch (Exception ex)
				{
					Debug.LogWarning($"Failed to add element to graph: {element}\n{ex}");
				}
			}

			ListPool<IGraphElement>.Free(sortedElements);
		}
	}
}
