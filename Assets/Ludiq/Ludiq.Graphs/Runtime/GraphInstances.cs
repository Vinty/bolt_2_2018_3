﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ludiq
{
	public static class GraphInstances
	{
		private static readonly object @lock = new object();
		
		private static readonly Dictionary<IGraph, HashSet<GraphReference>> byGraph = new Dictionary<IGraph, HashSet<GraphReference>>();

		private static readonly Dictionary<IGraphParent, HashSet<GraphReference>> byParent = new Dictionary<IGraphParent, HashSet<GraphReference>>();

		public static void Instantiate(GraphReference instance)
		{
			lock (@lock)
			{
				Ensure.That(nameof(instance)).IsNotNull(instance);

				instance.graph.Instantiate(instance);

				if (!byGraph.TryGetValue(instance.graph, out var instancesWithGraph))
				{
					instancesWithGraph = new HashSet<GraphReference>();
					byGraph.Add(instance.graph, instancesWithGraph);
				}

				instancesWithGraph.Add(instance);

				// Debug.Log($"Added graph instance mapping:\n{instance.graph} => {instance}");

				if (!byParent.TryGetValue(instance.parent, out var instancesWithParent))
				{
					instancesWithParent = new HashSet<GraphReference>();
					byParent.Add(instance.parent, instancesWithParent);
				}

				instancesWithParent.Add(instance);

				// Debug.Log($"Added parent instance mapping:\n{instance.parent.ToSafeString()} => {instance}");
			}
		}

		public static void Uninstantiate(GraphReference instance)
		{
			lock (@lock)
			{
				instance.graph.Uninstantiate(instance);

				if (!byGraph.TryGetValue(instance.graph, out var instancesWithGraph))
				{
					throw new InvalidOperationException("Graph instance not found via graph.");
				}

				instancesWithGraph.Remove(instance);

				// Debug.Log($"Removed graph instance mapping:\n{instance.graph} => {instance}");

				if (!byParent.TryGetValue(instance.parent, out var instancesWithParent))
				{
					throw new InvalidOperationException("Graph instance not found via parent.");
				}

				instancesWithParent.Remove(instance);

				// Debug.Log($"Removed parent instance mapping:\n{instance.parent.ToSafeString()} => {instance}");
			}
		}
		
		public static HashSet<GraphReference> OfPooled(IGraph graph)
		{
			Ensure.That(nameof(graph)).IsNotNull(graph);

			lock (@lock)
			{
				if (byGraph.TryGetValue(graph, out var instances))
				{
					// Debug.Log($"Found {instances.Count} instances of {graph}\n{instances.ToLineSeparatedString()}");

					return instances.ToHashSetPooled();
				}
				else
				{
					// Debug.Log($"Found no instances of {graph}.\n");

					return HashSetPool<GraphReference>.New();
				}
			}
		}

		public static HashSet<GraphReference> ChildrenOfPooled(IGraphParent parent)
		{
			Ensure.That(nameof(parent)).IsNotNull(parent);

			lock (@lock)
			{
				if (byParent.TryGetValue(parent, out var instances))
				{
					// Debug.Log($"Found {instances.Count} instances of {parent.ToSafeString()}\n{instances.ToLineSeparatedString()}");

					return instances.ToHashSetPooled();
				}
				else
				{
					// Debug.Log($"Found no instances of {parent.ToSafeString()}.\n");
					
					return HashSetPool<GraphReference>.New();
				}
			}
		}
	}
}
