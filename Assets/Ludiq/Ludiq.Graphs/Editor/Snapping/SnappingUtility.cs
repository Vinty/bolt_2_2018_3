﻿using UnityEngine;

namespace Ludiq
{
	public static class SnappingUtility
	{
		public static float Distance(Vector2 a, Vector2 b, Axis2 axis)
		{
			switch (axis)
			{
				case Axis2.Horizontal: return Mathf.Abs(a.x - b.x);
				case Axis2.Vertical: return Mathf.Abs(a.y - b.y);
				default: throw new UnexpectedEnumValueException<Axis2>(axis);
			}
		}
	}
}
