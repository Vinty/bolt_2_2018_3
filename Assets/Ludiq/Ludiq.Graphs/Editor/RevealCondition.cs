﻿namespace Ludiq
{
	public enum RevealCondition
	{
		Never,
		Always,
		OnHover,
		WithAlt,
		OnHoverOrWithAlt,
		OnHoverWithAlt,
		WhenSelected,
		OnHoverOrSelected,
		OnHoverWithAltOrSelected,
		OnHoverOrWithAltOrSelected,
	}

	public static class XRevealCondition
	{
		public static bool IsMet(this RevealCondition condition, bool isHovered, bool isSelected, bool isAltDown)
		{
			switch (condition)
			{
				case RevealCondition.Always: return true;
				case RevealCondition.Never: return false;
				case RevealCondition.OnHover: return isHovered;
				case RevealCondition.WithAlt: return isAltDown;
				case RevealCondition.OnHoverOrWithAlt: return isHovered || isAltDown;
				case RevealCondition.OnHoverWithAlt: return isHovered && isAltDown;
				case RevealCondition.WhenSelected: return isSelected;
				case RevealCondition.OnHoverOrSelected: return isHovered || isSelected;
				case RevealCondition.OnHoverWithAltOrSelected: return (isHovered && isAltDown) || isSelected;
				case RevealCondition.OnHoverOrWithAltOrSelected: return isHovered || isAltDown || isSelected;
				default: throw new UnexpectedEnumValueException<RevealCondition>(condition);
			}
		}
	}
}