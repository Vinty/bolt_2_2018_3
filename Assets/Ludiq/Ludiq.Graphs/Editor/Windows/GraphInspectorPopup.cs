﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public sealed class GraphInspectorPopup : LudiqEditorWindow
	{
		public static GraphInspectorPopup instance { get; private set; }

		private IGraphContext context;
		private Metadata metadata;
		
		public static void Open(IGraphContext context, Metadata metadata, Rect activator)
		{
			Ensure.That(nameof(context)).IsNotNull(context);
			Ensure.That(nameof(metadata)).IsNotNull(metadata);

			if (instance == null)
			{
				instance = CreateInstance<GraphInspectorPopup>();
			}

			instance.context = context;
			instance.metadata = metadata;

			var width = 275;
			var height = Mathf.Min(instance.GetHeight(width), 320);

			activator.position = GUIUtility.GUIToScreenPoint(activator.position);
			
			instance.ShowAsDropDown(activator, new Vector2(width, height));
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			instance = this;
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			instance = null;
		}

		protected override void Update()
		{
			base.Update();
			Repaint();
		}

		protected override void OnGUI()
		{
			base.OnGUI();

			LudiqGUIUtility.BeginScrollableWindow(position, GetHeight, out var innerPosition, ref scroll);
		
			EditorGUIUtility.hierarchyMode = true; // For the label width to be correct, like in the inspector

			context.BeginEdit();

			LudiqGUI.Editor(metadata, innerPosition);

			context.EndEdit();

			LudiqGUIUtility.EndScrollableWindow();

			if (e.type == EventType.Repaint)
			{
				LudiqGUI.DrawEmptyRect(new Rect(Vector2.zero, this.position.size), ColorPalette.unityBackgroundVeryDark);
			}

			if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Q)
			{
				Close();
			}
		}
		
		public float GetHeight(float width)
		{
			EditorGUIUtility.hierarchyMode = true; // For the label width to be correct, like in the inspector

			var height = 0f;

			context.BeginEdit();

			height += LudiqGUI.GetEditorHeight(null, metadata, width);

			context.EndEdit();

			return height;
		}
	}
}
