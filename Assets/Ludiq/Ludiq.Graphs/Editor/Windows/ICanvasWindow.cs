﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public interface ICanvasWindow
	{
		GraphReference reference { get; set; }
		
		bool maximized { get; set; }

		Vector2 UnclipPoint(Vector2 p);
		Vector2 UnclipVector(Vector2 v);
		Rect Unclip(Rect r);
	}

	public static class XCanvasWindow
	{
		public static bool IsFocused(this ICanvasWindow window)
		{
			return EditorWindow.focusedWindow == (EditorWindow)window;
		}

		public static void Focus(this ICanvasWindow window)
		{
			((EditorWindow)window).Focus();
		}
	}
}