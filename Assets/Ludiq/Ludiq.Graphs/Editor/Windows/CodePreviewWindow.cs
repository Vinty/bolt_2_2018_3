﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Ludiq.CodeDom;
using UnityEditor;
using UnityEngine;
using AssetDatabase = UnityEditor.AssetDatabase;
using SystemTask = System.Threading.Tasks.Task;

namespace Ludiq
{
	public sealed class CodePreviewWindow : GraphContextualWindow
	{
		public static CodePreviewWindow instance { get; private set; }

		[MenuItem("Window/Bolt/C# Preview", priority = 4)]
		public static void Open()
		{
			if (instance == null)
			{
				GetWindow<CodePreviewWindow>().Show();
			}
			else
			{
				FocusWindowIfItsOpen<CodePreviewWindow>();
			}
		}
		
		private int currentColumns = 80;

		private int requestedColumns = 80;

		private Vector2 scrollPosition;

		private CodePreviewResult latestResult;

		private Exception latestFailure;

		private CodePreviewResult result;

		private Exception failure;

		private readonly object selectionInvalidationLock = new object();

		private bool selectionInvalidated;

		private readonly List<Rect> selectionRegions = new List<Rect>();

		private SystemTask task;

		private CancellationTokenSource cancellation;

		private bool retryRequested = false;

		private EventWrapper codePreviewEvent;

		protected override void OnEnable()
		{
			base.OnEnable();
			instance = this;
			minSize = new Vector2(275, 200);
			titleContent = new GUIContent("C# Preview", LudiqGraphs.Icons.codePreviewWindow?[IconSize.Small]);
			
			EditorApplication.delayCall += Request;

			if (context != null)
			{
				StartWatchingContext(context);
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			instance = null;

			if (context != null)
			{
				StopWatchingContext(context);
			}
		}
		
		protected override void _OnContextChange(IGraphContext previousContext, IGraphContext newContext)
		{
			base._OnContextChange(previousContext, newContext);

			scrollPosition = Vector2.zero;

			Cancel();

			if (previousContext != null)
			{
				StopWatchingContext(previousContext);
			}

			Request();

			if (newContext != null)
			{
				StartWatchingContext(newContext);
			}
		}

		private void StartWatchingContext(IGraphContext context)
		{
			context.onEdited += Request;
			context.selection.changed += InvalidateSelection;
		}

		private void StopWatchingContext(IGraphContext context)
		{
			context.onEdited -= Request;
			context.selection.changed -= InvalidateSelection;
		}
		
		protected override void _OnEnterEditMode()
		{
			base._OnEnterEditMode();

			Fonts.Reload();
			Styles.code.font = Fonts.font;
		}

		protected override void Update()
		{
			base.Update();

			if (task != null && task.IsCompleted && retryRequested)
			{
				Request();
			}

			if (result != latestResult)
			{
				result = latestResult;
				InvalidateSelection();
			}

			if (failure != latestFailure)
			{
				failure = latestFailure;
			}

			ComputeSelection(result);

			Repaint();
		}

		protected override void OnGUI()
		{
			base.OnGUI();

			if (PluginContainer.anyVersionMismatch)
			{
				LudiqGUI.BeginVertical();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.BeginHorizontal();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.VersionMismatchShieldLayout();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndHorizontal();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndHorizontal();

				return;
			}

			var layout = EditorGUILayout.BeginHorizontal();
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

			if (failure != null)
			{
				EditorGUI.BeginDisabledGroup(true);
			}

			if (result != null)
			{
				var lines = new StringBuilder();
				var lineCount = Regex.Matches(result.syntaxHighlightingResult.FormattedText, "\n").Count;

				for (int i = 0; i < lineCount; i++)
				{
					lines.AppendLine(i.ToString());
				}

				var linesContent = new GUIContent(lines.ToString());
				var codeContent = new GUIContent(result.syntaxHighlightingResult.FormattedText);
				
				LudiqGUI.BeginHorizontal();
				var linesPosition = GUILayoutUtility.GetRect(linesContent, Styles.lines).PixelPerfect();
				var codePosition = GUILayoutUtility.GetRect(codeContent, Styles.code).PixelPerfect();
				LudiqGUI.EndHorizontal();

				if (e.type == EventType.Repaint)
				{
					int newColumns = (int) ((layout.width - linesPosition.width - Styles.code.padding.left - Styles.code.padding.right) /
					                 Fonts.fontDimensions.x);

					if (currentColumns != newColumns)
					{
						currentColumns = newColumns;
					}

					if (requestedColumns != currentColumns)
					{
						Request();
					}

					Styles.linesBackground.Draw(linesPosition, false, false, false, false);
				}

				GUI.Label(linesPosition, linesContent, Styles.lines);
				
				if (e.type == EventType.Repaint)
				{
					Styles.codeBackground.Draw(codePosition, false, false, false, false);
				}

				var padding = new Vector2(Styles.code.padding.left, Styles.code.padding.top - 2);

				foreach (var selectionRegion in selectionRegions)
				{
					var rect = selectionRegion;
					rect.position += codePosition.position + padding;
					EditorGUI.DrawRect(rect, Styles.selectionColor);
				}

				GUI.Label(codePosition, codeContent, Styles.code);
			}

			if (failure != null)
			{
				EditorGUI.EndDisabledGroup();

				var failureWidth = Mathf.Min(Styles.failureWidth, position.width - (Styles.failurePadding * 2));

				var failurePosition = new Rect
				(
					position.width - Styles.failurePadding - failureWidth,
					Styles.failurePadding,
					failureWidth,
					LudiqGUIUtility.GetHelpBoxHeight(failure.Message, MessageType.Error, failureWidth)
				);

				EditorGUI.HelpBox(failurePosition, failure.Message, MessageType.Error);

				if (GUI.Button(failurePosition, GUIContent.none, GUIStyle.none))
				{
					Debug.LogException(failure);
				}
			}

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndHorizontal();
		}

		private void InvalidateSelection()
		{
			lock (selectionInvalidationLock)
			{
				selectionInvalidated = true;
			}
		}

		private void ComputeSelection(CodePreviewResult codePreviewResult)
		{
			bool invalidated;

			lock (selectionInvalidationLock)
			{
				invalidated = selectionInvalidated;

				selectionInvalidated = false;
			}

			if (invalidated)
			{
				selectionRegions.Clear();

				if (codePreviewResult == null)
				{
					return;
				}

				var unsortedSelectedTokens = new HashSet<Token>();

				var graphScript = codePreviewResult.graphScript;

				foreach (var graphElement in context.selection)
				{
					foreach (var codeElement in graphScript.CodeFor(graphElement))
					{
						if (codePreviewResult.tokensByElements.TryGetValue(codeElement, out var tokens))
						{
							unsortedSelectedTokens.AddRange(tokens);
						}
					}
				}

				var tokenEntries = codePreviewResult.syntaxHighlightingResult.TokenEntries;
				var sortedSelectedTokens = unsortedSelectedTokens.Where(token =>
					token.Type != TokenType.Newline
					&& tokenEntries.ContainsKey(token)
				).ToList();

				sortedSelectedTokens.Sort((a, b) =>
				{
					var entryA = tokenEntries[a];
					var entryB = tokenEntries[b];
					var comparison = entryA.StartPosition.CompareTo(entryB.StartPosition);

					if (comparison == 0)
					{
						comparison = entryA.EndPosition.CompareTo(entryB.EndPosition);
					}

					return comparison;
				});

				SyntaxHighlightingTokenEntry previousEntry = null;
				for (int i = 0; i < sortedSelectedTokens.Count;)
				{
					var token = sortedSelectedTokens[i];
					var entry = tokenEntries[token];

					if (token.Type == TokenType.Indentation
					    && (previousEntry == null
					        || i == sortedSelectedTokens.Count - 1
					        || entry.StartPosition.LineIndex - previousEntry.EndPosition.LineIndex > 2))
					{
						sortedSelectedTokens.RemoveAt(i);
					}
					else
					{
						previousEntry = entry;
						i++;
					}
				}

				foreach (var token in sortedSelectedTokens)
				{
					var entry = tokenEntries[token];
					var currentPosition = entry.StartPosition;
					var endPosition = entry.EndPosition;
					do
					{
						if (currentPosition.LineIndex == endPosition.LineIndex)
						{
							selectionRegions.Add(new Rect(
								currentPosition.ColumnIndex * Fonts.fontDimensions.x,
								currentPosition.LineIndex * Fonts.fontDimensions.y,
								(endPosition.ColumnIndex - currentPosition.ColumnIndex) * Fonts.fontDimensions.x,
								Fonts.fontDimensions.y));
						}
						else
						{
							// TODO: get width of current line. (probably need a list of line lengths, so we can quickly look them up.)
							selectionRegions.Add(new Rect(
								currentPosition.ColumnIndex * Fonts.fontDimensions.x,
								currentPosition.LineIndex * Fonts.fontDimensions.y,
								200 * Fonts.fontDimensions.x,
								Fonts.fontDimensions.y));
						}

						currentPosition.LineIndex++;
						currentPosition.ColumnIndex = 0;
					} while (currentPosition.LineIndex <= endPosition.LineIndex);
				}
			}
		}

		private void Request()
		{
			Cancel();
			cancellation = new CancellationTokenSource();

			requestedColumns = currentColumns;

			task = new SystemTask(Generate, cancellation.Token);
			task.Start();
		}

		private void Generate()
		{
			var wasRetryRequested = retryRequested;
			retryRequested = false;

			var reference = context?.reference;

			if (reference == null)
			{
				latestResult = null;
				return;
			}

			try
			{
				GeneratorProvider.instance.FreeAll();

				var graphGenerationSystem = new GraphGenerationSystem(GraphGenerationMode.Preview);
				var writerSystem = new TokenCodeWriterSystem();
				var generatedGraph = graphGenerationSystem.GenerateGraph(reference.parent);
				var graphScript = generatedGraph.graphScript;
				graphGenerationSystem.WriteClasses(writerSystem);
				var tokensByElements = writerSystem.GetTokensByElements(graphScript.classDeclaration.Name);

				var fieldTokens = new List<Token>();
				var nonfieldTokens = new List<Token>();

				foreach (var member in graphScript.classDeclaration.Members)
				{
					if (!graphScript.IsImplementationMember(member))
					{
						switch (member)
						{
							case CodeFieldMember _:
							{
								fieldTokens.AddRange(tokensByElements[member]);
								break;
							}
							case CodeBasicPropertyMember _:
							case CodeBasicMethodMember _:
							{
								if (nonfieldTokens.Count > 0)
								{
									nonfieldTokens.Add(new Token(null, TokenType.Newline, Environment.NewLine, 0));
								}

								nonfieldTokens.AddRange(tokensByElements[member]);
								break;
							}
						}
					}
				}

				var sourceTokens = new List<Token>();
				sourceTokens.AddRange(fieldTokens.Trimmed());

				if (sourceTokens.Count > 0)
				{
					sourceTokens.Add(new Token(null, TokenType.Newline, Environment.NewLine, 0));
					sourceTokens.Add(new Token(null, TokenType.Newline, Environment.NewLine, 0));
				}

				sourceTokens.AddRange(nonfieldTokens.Trimmed());

				var formattedResult = SyntaxHighlightingScheme.DefaultScheme.FormatTokens(sourceTokens, 2, requestedColumns);
				
				latestResult = new CodePreviewResult(graphScript, tokensByElements, formattedResult);
				latestFailure = null;
			}
			catch (TimeoutException)
			{
				Debug.LogWarning("Timed out during code preview generation (this can happen when switching to/from Play Mode). Trying again...");
				retryRequested = true;
			}			
			catch (Exception ex)
			{
				latestFailure = ex;
			}
		}

		private void Cancel()
		{
			cancellation?.Cancel();
		}
		
		private class CodePreviewResult
		{
			public CodePreviewResult(
				GraphClassGenerationContext graphScript,
				Dictionary<CodeElement, List<Token>> tokensByElements,
				SyntaxHighlightingResult syntaxHighlightingResult)
			{
				this.graphScript = graphScript;
				this.tokensByElements = tokensByElements;
				this.syntaxHighlightingResult = syntaxHighlightingResult;
			}

			public GraphClassGenerationContext graphScript { get; }
			public Dictionary<CodeElement, List<Token>> tokensByElements { get; }
			public SyntaxHighlightingResult syntaxHighlightingResult { get; }
		}

		public static class Fonts
		{
			static Fonts()
			{
				Reload();
			}

			public static void Reload()
			{
				//font = Font.CreateDynamicFontFromOSFont(new[] {"Consolas"}, fontSize);
				font = LudiqCore.Resources.LoadAsset<Font>("Fonts/SourceCodePro-Regular.otf", true);
				
			}

			public const int fontSize = 12;
			public static readonly Vector2 fontDimensions = new Vector2(7, 15.1f);
			public static Font font = null;
		}

		public static class Styles
		{
			static Styles()
			{
				code = new GUIStyle()
				{
					richText = true,
					stretchWidth = true,
					stretchHeight = true,
					padding = new RectOffset(8, 8, 8, 8),
					margin = new RectOffset(),
					wordWrap = false,
					font = Fonts.font,
					fontSize = Fonts.fontSize,
					normal =
					{
						textColor = Color.white
					}
				};

				codeBackground = ColorPalette.unityGraphBackground.CreateBackground();

				lines = new GUIStyle(code);
				lines.alignment = TextAnchor.UpperRight;
				lines.stretchWidth = false;
				lines.padding.left = 16;

				linesBackground = new SkinnedColor(ColorPalette.unityBackgroundVeryDark, ColorPalette.unityBackgroundMid).CreateBackground();

				selectionColor = ColorPalette.unitySelectionHighlight.WithAlpha(0.75f);
			}

			public static readonly GUIStyle code;
			public static readonly GUIStyle codeBackground;
			public static readonly GUIStyle lines;
			public static readonly GUIStyle linesBackground;
			public static readonly SkinnedColor selectionColor;
			public static readonly float failurePadding = 16;
			public static readonly float failureWidth = 300;
		}
	}
}