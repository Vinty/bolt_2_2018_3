﻿using UnityEngine;

namespace Ludiq
{
	[Plugin(LudiqGraphs.ID)]
	public sealed class LudiqGraphsConfiguration : PluginConfiguration
	{
		private LudiqGraphsConfiguration(LudiqGraphs plugin) : base(plugin) { }

		#region Editor Prefs

		/// <summary>
		/// Whether the graph window should show the background grid.
		/// </summary>
		[EditorPref]
		public bool showGrid { get; set; } = true;

		/// <summary>
		/// Whether graph elements should intelligently snap to each other.
		/// </summary>
		[EditorPref]
		public bool snapping { get; set; } = true;

		/// <summary>
		/// The pixel threshold under which snapping should be trigger.
		/// </summary>
		[EditorPref]
		public float snapThreshold { get; set; } = 16;

		/// <summary>
		/// Whether graph elements should intelligently carry their related elements.
		/// </summary>
		[EditorPref]
		public bool carrying { get; set; } = false;
		
		/// <summary>
		/// The speed at which the mouse scroll pans the graph.
		/// </summary>
		[EditorPref]
		[InspectorRange(1, 20)]
		public float panSpeed { get; set; } = 5;

		/// <summary>
		/// The speed at which dragged elements pan the graph when at the edge.
		/// </summary>
		[EditorPref]
		[InspectorRange(0, 10)]
		public float dragPanSpeed { get; set; } = 5;

		/// <summary>
		/// The speed at which the mouse wheel zooms the graph.
		/// </summary>
		[EditorPref]
		[InspectorRange(0.01f, 0.1f)]
		public float zoomSpeed { get; set; } = 0.025f;

		/// <summary>
		/// The duration for graph overview. Set to zero to disable smoothing.
		/// </summary>
		[EditorPref]
		[InspectorRange(0, 1)]
		public float overviewSmoothing { get; set; } = 0.25f;
		
		/// <summary>
		/// Whether inactive graph nodes should be dimmed.
		/// </summary>
		[EditorPref]
		public bool dimInactiveNodes { get; set; } = true;

		/// <summary>
		/// Whether incompatible graph nodes should be dimmed.
		/// </summary>
		[EditorPref]
		public bool dimIncompatibleNodes { get; set; } = true;

		/// <summary>
		/// Whether the playmode tint should be removed in the graph window.
		/// </summary>
		[EditorPref]
		public bool disablePlaymodeTint { get; set; } = true;

		/// <summary>
		/// Whether additional helpers should be shown in graphs for debugging.
		/// </summary>
		[EditorPref(visibleCondition = nameof(developerMode))]
		public bool debug { get; set; } = false;
		
		/// <summary>
		/// The control scheme to use for pan and zoom.
		/// Unity: pan with [MMB], zoom with [Ctrl + Scroll Wheel].
		/// Unreal: pan with [MMB] or [Alt + LMB], zoom with [Scroll Wheel].
		/// </summary>
		[EditorPref]
		public CanvasControlScheme controlScheme { get; set; } = CanvasControlScheme.Unreal;
		
		/// <summary>
		/// Whether the graph window and inspector should be cleared when
		/// the selection does not provide a graph. When disabled,
		/// the last graph will stay selected.
		/// </summary>
		[EditorPref]
		public bool clearGraphSelection { get; set; } = false;
		
		/// <summary>
		/// Whether the graph generation should include legacy root embed graphs.
		/// This will slow down generation process because all scenes will have to be loaded.
		/// </summary>
		[ProjectSetting]
		public bool generateRootEmbedGraphs { get; set; } = false;

		#endregion
	}
}