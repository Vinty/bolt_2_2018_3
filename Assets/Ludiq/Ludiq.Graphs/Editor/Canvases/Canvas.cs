using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public abstract class Canvas<TGraph> : ICanvas
		where TGraph : class, IGraph
	{
		public WidgetProvider widgetProvider { get; }

		public GraphSelection selection { get; }

		public ICanvasWindow window { get; set; }

		protected EventWrapper e { get; }

		public TGraph graph { get; }

		IGraph ICanvas.graph => graph;

		protected Canvas(TGraph graph)
		{
			Ensure.That(nameof(graph)).IsNotNull(graph);

			this.graph = graph;

			e = new EventWrapper(GetType());

			selection = new GraphSelection();

			widgetProvider = new WidgetProvider(this);

			graph.elements.CollectionChanged += OnElementsChanged;

			widgets = new WidgetList<IWidget>(this);
			elementWidgets = new WidgetList<IGraphElementWidget>(this);
			widgetsByAscendingZ = new WidgetList<IWidget>(this);
			visibleWidgetsByAscendingZ = new WidgetList<IWidget>(this);
			visibleWidgetsByDescendingZ = new WidgetList<IWidget>(this);

			RegisterSnappingSets(snapping);
		}

		public virtual void Dispose()
		{
			graph.elements.CollectionChanged -= OnElementsChanged;

			widgetProvider.FreeAll();
		}


		#region Context Shortcuts

		protected IGraphContext context => LudiqGraphsEditorUtility.editedContext.value;

		protected GraphReference reference => context.reference;

		#endregion


		public void Cache()
		{
			CacheWidgetCollections();
			CacheWidgetItems();
			CacheWidgetPositions();
			CacheWidgetVisibility();
		}


		#region Model

		public void CacheWidgetItems()
		{
			foreach (var widget in widgets)
			{
				widget.CacheItem();
			}
		}

		#endregion


		#region Widgets

		IEnumerable<IWidget> ICanvas.widgets => widgets;

		protected readonly WidgetList<IWidget> widgets;

		protected readonly WidgetList<IGraphElementWidget> elementWidgets;

		private IEnumerable<IWidget> GetWidgets()
		{
			foreach (var element in graph.elements)
			{
				foreach (var widget in GetWidgetsRecursive(this.Widget(element)))
				{
					yield return widget;
				}
			}
		}

		private IEnumerable<IWidget> GetWidgetsRecursive(IWidget widget)
		{
			yield return widget;

			foreach (var subWidget in widget.subWidgets)
			{
				foreach (var subWidgetRecursive in GetWidgetsRecursive(subWidget))
				{
					yield return subWidgetRecursive;
				}
			}
		}

		private bool collectionsAreValid;

		public void OnElementsChanged()
		{
			collectionsAreValid = false;
		}

		public void CacheWidgetCollections()
		{
			// Dispose widgets that are no longer within the graph
			// so that they unregister any event handler
			widgetProvider.FreeInvalid();

			// Remove invalid widgets from the drag group
			dragGroup.RemoveWhere(element => !widgetProvider.IsValid(element));

			// Rebuild the widget collection
			widgets.Clear();
			widgets.AddRange(GetWidgets());
			elementWidgets.Clear();
			elementWidgets.AddRange(widgets.OfType<IGraphElementWidget>());

			// Normalize and cache the z-ordering
			var zIndex = 0;

			foreach (var widget in elementWidgets.OrderBy(widget => widget.zIndex))
			{
				widget.zIndex = zIndex++;
			}

			widgetsByAscendingZ.Clear();

			// Convoluted way to avoid allocation while sorting
			var _widgetsByAscendingZ = ListPool<IWidget>.New();

			foreach (var widget in widgets)
			{
				_widgetsByAscendingZ.Add(widget);
			}

			_widgetsByAscendingZ.Sort((a, b) => a.zIndex.CompareTo(b.zIndex));

			foreach (var widget in _widgetsByAscendingZ)
			{
				widgetsByAscendingZ.Add(widget);
			}

			_widgetsByAscendingZ.Free();

			// Mark as complete
			collectionsAreValid = true;

			// Send the event to widgets
			foreach (var widget in widgets)
			{
				widget.OnElementsChanged();
			}
		}

		#endregion


		#region Lifecycle

		public virtual void Open() { }

		public virtual void Close()
		{
			isLassoing = false;
			isDragging = false;

			foreach (var dragged in dragGroup)
			{
				if (widgetProvider.IsValid(dragged))
				{
					this.Widget(dragged).EndDrag();
				}
			}

			dragGroup.Clear();
		}

		public void RegisterControls()
		{
			e.RegisterControl(FocusType.Keyboard);

			foreach (var widget in widgets)
			{
				widget.RegisterControl();
			}
		}

		public void Update()
		{
			foreach (var widget in widgets)
			{
				widget.Update();
			}
		}

		public void BeforeFrame()
		{
			if (!collectionsAreValid)
			{
				// Fetch the list of all widgets
				CacheWidgetCollections();
			}

			// Update the widgets with the data from their model
			CacheWidgetItems();

			// Send a before frame event to widgets
			foreach (var widget in widgets)
			{
				widget.BeforeFrame();
			}

			// Calculate the positions for models that have been repositioned
			CacheWidgetPositions();

			lastFrameTime = DateTime.UtcNow;
		}

		public void OnGUI()
		{
			// Cache the mouse position across different windows
			HandleMouseMovement();

			// Determine the hovered widget first to determine if the mouse is over the background
			DetermineHoveredWidget();

			if (e.IsRepaint)
			{
				// Cache the list of visible widgets
				CacheWidgetVisibility();
			}

			// Handle automated edge panning
			// Needed in repaint too because idling
			// near the edges should cause a pan too
			HandleEdgePanning();

			if (!e.IsRepaint)
			{
				// Handle mouse / keyboard capture
				HandleEventCapture();

				// Handle panning and zoom
				HandleViewportInput();

				// Handle any input that takes priority over widgets
				HandleHighPriorityInput();
			}

			// Draw the canvas background
			DrawBackground();

			// Draw the widgets
			DrawWidgetsBackground();
			DrawWidgetsForeground();
			DrawWidgetsOverlay();

			// Draw the canvas overlay
			DrawOverlay();

			// Draw the drag and drop preview
			if (e.IsRepaint)
			{
				dragAndDropHandler?.DrawDragAndDropPreview();
			}

			if (!e.IsRepaint)
			{
				// Handle the widgets' input
				HandleWidgetInput();

				// Handle any input that is less important than the widget's
				HandleLowPriorityInput();

				// Handle mouse / keyboard release
				HandleEventRelease();
			}

			// Update timing for deltas
			lastEventTime = DateTime.UtcNow;

			if (e.IsRepaint)
			{
				lastRepaintTime = DateTime.UtcNow;
			}
		}

		protected void HandleEventCapture()
		{
			foreach (var widget in visibleWidgetsByDescendingZ)
			{
				widget.HandleCapture();
			}

			e.HandleCapture(isMouseOverBackground, isMouseOver);
		}

		protected virtual void HandleHighPriorityInput() { }

		protected void HandleWidgetInput()
		{
			foreach (var widget in visibleWidgetsByDescendingZ)
			{
				widget.HandleInput();
			}
		}

		protected void HandleEventRelease()
		{
			foreach (var widget in widgets)
			{
				widget.HandleRelease();
			}

			e.HandleRelease();
		}

		protected virtual void HandleLowPriorityInput()
		{
			HandleInspecting();

			HandleClipboard();

			HandleCollapsing();

			HandleLassoing();

			HandleSelecting();

			HandleDeleting();

			HandleContext();

			HandleDragAndDrop();

			HandleMaximization();
		}

		protected virtual void HandleMaximization()
		{
			if (e.IsMouseDown(MouseButton.Left) && e.clickCount == 2)
			{
				ToggleMaximized();
			}
		}

		protected void ToggleMaximized()
		{
			window.maximized = !window.maximized;
			GUIUtility.hotControl = 0;
			GUIUtility.keyboardControl = 0;
			GUIUtility.ExitGUI();
		}

		#endregion


		#region Viewing

		public float zoom { get; set; }

		public Vector2 pan { get; set; }

		private Rect _viewport;

		public Rect viewport
		{
			get => _viewport;
			set
			{
				if (value == viewport)
				{
					return;
				}

				_viewport = value;
				OnViewportChange();
			}
		}

		public virtual Rect fullViewport => GraphGUI.CalculateArea(widgets.OfType<IGraphElementWidget>());

		private float viewTweenDuration;

		private Vector2 viewTweenStartPan;

		private Vector2 viewTweenEndPan;

		private float viewTweenStartZoom;

		private float viewTweenEndZoom;

		private DateTime viewTweenStart;

		private bool isTweeningView;

		public Vector2 mousePosition { get; private set; }

		private void HandleMouseMovement()
		{
			if (isMouseOver)
			{
				mousePosition = e.mousePosition;
			}
		}

		public bool isMouseOver => viewport.Contains(e.mousePosition);

		public bool isMouseOverBackground => isMouseOver && hoveredWidget == null;

		private readonly RectOffset dragPanOffset = new RectOffset(20, 20, 20, 20);

		private bool isResizing
		{
			get
			{
				foreach (var widget in widgets)
				{
					if ((widget as IGraphElementWidget)?.isResizing ?? false)
					{
						return true;
					}
				}

				return false;
			}
		}

		protected virtual bool shouldEdgePan => isDragging || isLassoing || isResizing;

		protected readonly WidgetList<IWidget> widgetsByAscendingZ;

		protected readonly WidgetList<IWidget> visibleWidgetsByAscendingZ;

		protected readonly WidgetList<IWidget> visibleWidgetsByDescendingZ;

		public void UpdateViewport()
		{
			if (isTweeningView)
			{
				var t = (float)(DateTime.UtcNow - viewTweenStart).TotalSeconds / viewTweenDuration;

				if (t >= 1)
				{
					isTweeningView = false;
				}
				else
				{
					pan = Vector2.Lerp(viewTweenStartPan, viewTweenEndPan, t);
					zoom = Mathf.Lerp(viewTweenStartZoom, viewTweenEndZoom, t);
				}
			}
			else
			{
				pan = graph.pan;
				zoom = graph.zoom;
			}
		}

		public void TweenViewport(Vector2 pan, float zoom, float duration)
		{
			if (context == null)
			{
				return;
			}

			isTweeningView = true;
			viewTweenDuration = duration;
			viewTweenStartPan = this.pan;
			viewTweenStartZoom = this.zoom;
			viewTweenEndPan = pan;
			viewTweenEndZoom = zoom;
			viewTweenStart = DateTime.UtcNow;

			graph.pan = pan;
			graph.zoom = zoom;
		} 

		public void TweenViewport(Rect viewport, float duration)
		{
			var pan = viewport.center;
			var zoomX = (this.viewport.width * graph.zoom) / (viewport.width);
			var zoomY = (this.viewport.height * graph.zoom) / (viewport.height);
			var zoom = Mathf.Clamp(Mathf.Min(zoomX, zoomY), GraphGUI.MinZoom, GraphGUI.MaxZoom);

			TweenViewport(pan, zoom, duration);
		}

		public void CacheWidgetVisibility()
		{
			visibleWidgetsByAscendingZ.Clear();
			visibleWidgetsByDescendingZ.Clear();

			for (int i = 0; i < widgetsByAscendingZ.Count; i++)
			{
				var ascending = widgetsByAscendingZ[i];
				var descending = widgetsByAscendingZ[widgetsByAscendingZ.Count - i - 1];

				ascending.isVisible = IsVisible(ascending);
				descending.isVisible = IsVisible(descending);

				if (ascending.isVisible)
				{
					visibleWidgetsByAscendingZ.Add(ascending);
				}

				if (descending.isVisible)
				{
					visibleWidgetsByDescendingZ.Add(descending);
				}
			}
		}

		private void OnViewportChange()
		{
			foreach (var widget in widgets)
			{
				widget.OnViewportChange();
			}
		}

		private void HandleViewportInput()
		{
			if (isTweeningView)
			{
				return;
			}

			var controlScheme = LudiqGraphs.Configuration.controlScheme;

			if (isMouseOver && e.controlType == EventType.ScrollWheel)
			{
				bool zooming;
				bool scrolling;

				if (controlScheme == CanvasControlScheme.Unity)
				{
					zooming = e.ctrlOrCmd;
					scrolling = !zooming;
				}
				else if (controlScheme == CanvasControlScheme.Unreal)
				{
					zooming = true;
					scrolling = false;
				}
				else
				{
					throw new UnexpectedEnumValueException<CanvasControlScheme>(controlScheme);
				}

				if (zooming)
				{
					var zoomDelta = MathfEx.NearestMultiple(-e.mouseDelta.y * LudiqGraphs.Configuration.zoomSpeed, GraphGUI.ZoomSteps);
					zoomDelta = Mathf.Clamp(graph.zoom + zoomDelta, GraphGUI.MinZoom, GraphGUI.MaxZoom) - graph.zoom;

					if (zoomDelta != 0)
					{
						if (zoomDelta > 0)
						{
							var oldZoom = graph.zoom;
							var newZoom = graph.zoom + zoomDelta;
							var matrix = MathfEx.ScaleAroundPivot(mousePosition, (oldZoom / newZoom) * Vector3.one);
							graph.pan = matrix.MultiplyPoint(graph.pan);
							graph.zoom = newZoom;
						}
						else if (zoomDelta < 0)
						{
							graph.zoom += zoomDelta;
						}
					}

					e.Use();
				}

				if (scrolling)
				{
					var panDelta = e.mouseDelta * LudiqGraphs.Configuration.panSpeed;

					if (panDelta.magnitude != 0)
					{
						graph.pan += panDelta;
					}

					e.Use();
				}
			}

			if (e.controlType == EventType.MouseDown || e.controlType == EventType.MouseDrag)
			{
				bool panning;

				if (controlScheme == CanvasControlScheme.Unity)
				{
					panning = e.mouseButton == MouseButton.Middle;
				}
				else if (controlScheme == CanvasControlScheme.Unreal)
				{
					panning = (e.mouseButton == MouseButton.Middle) || (e.alt && e.mouseButton == (int)MouseButton.Left);
				}
				else
				{
					throw new UnexpectedEnumValueException<CanvasControlScheme>(controlScheme);
				}

				if (panning)
				{
					var panDelta = -e.mouseDelta;

					if (panDelta.magnitude != 0)
					{
						graph.pan += panDelta;
					}

					e.Use();
				}
			}

			if (e.IsKeyDown(KeyCode.F))
			{
				FrameSelection();
				e.Use();
			}
		}

		private void HandleEdgePanning()
		{
			if (isTweeningView || !window.IsFocused() || !shouldEdgePan)
			{
				return;
			}

			var mousePosition = e.mousePosition;

			var dragSpeedMultiplier = 120 * LudiqGraphs.Configuration.dragPanSpeed;

			var dragLeftSpeed = 0f;
			var dragRightSpeed = 0f;
			var dragTopSpeed = 0f;
			var dragBottomSpeed = 0f;

			if (mousePosition.x < viewport.x)
			{
				dragLeftSpeed = 3;
			}
			else if (mousePosition.x < viewport.x + dragPanOffset.left)
			{
				dragLeftSpeed = 1;
			}

			if (mousePosition.x > viewport.xMax)
			{
				dragRightSpeed = 3;
			}
			else if (mousePosition.x > viewport.xMax - dragPanOffset.right)
			{
				dragRightSpeed = 1;
			}

			if (mousePosition.y < viewport.y)
			{
				dragTopSpeed = 3;
			}
			else if (mousePosition.y < viewport.y + dragPanOffset.top)
			{
				dragTopSpeed = 1;
			}

			if (mousePosition.y > viewport.yMax)
			{
				dragBottomSpeed = 3;
			}
			else if (mousePosition.y > viewport.yMax - dragPanOffset.bottom)
			{
				dragBottomSpeed = 1;
			}

			var delta = new Vector2(dragRightSpeed - dragLeftSpeed, dragBottomSpeed - dragTopSpeed) * eventDeltaTime * dragSpeedMultiplier;

			if (delta != Vector2.zero)
			{
				graph.pan += delta;

				if (isDragging)
				{
					foreach (var draggedElement in dragGroup)
					{
						this.Widget(draggedElement).FreeDrag(delta);
					}
				}
			}
		}

		private void Overview()
		{
			FrameElements(graph.elements);
		}

		private void FrameSelection()
		{
			FrameElements(selection.Count > 0 ? (IEnumerable<IGraphElement>)selection : graph.elements);
		}

		public void FrameElements(IEnumerable<IGraphElement> elements)
		{
			var viewport = GraphGUI.CalculateArea(elements.Select(this.Widget)).ExpandBy(GraphGUI.Styles.viewportMargin);

			TweenViewport(viewport, LudiqGraphs.Configuration.overviewSmoothing);
		}

		public bool IsVisible(IWidget widget)
		{
			//if (isScreenshotting && !screenshotGroup.Contains(widget))
			{
				// Disabling for now because it's kind of weird. Even if we 
				// exclude connections for example, the port widgets will
				// show as connected.
				// return false;
			}

			return !widget.canClip || viewport.Overlaps(widget.clippingPosition);
		}

		#endregion


		#region Positioning

		private readonly List<IWidget> widgetsToReposition = new List<IWidget>();

		public void RepositionAll()
		{
			foreach (var widget in widgets)
			{
				widget.Reposition();
			}
		}

		public void CacheWidgetPositions()
		{
			widgetsToReposition.Clear();

			widgetsToReposition.AddRange(widgets.Where(widget => !widget.isPositionValid).OrderByDependencies(widget => widget.positionDependencies));

			using (LudiqGUI.matrix.Override(Matrix4x4.identity))
			{
				foreach (var widget in widgetsToReposition)
				{
					widget.CachePositionFirstPass();
				}

				foreach (var widget in widgetsToReposition)
				{
					widget.CachePosition();
					widget.isPositionValid = true;
				}
			}
		}

		#endregion


		#region Hovering

		public IWidget hoveredWidget { get; private set; }

		private void DetermineHoveredWidget()
		{
			hoveredWidget = null;

			foreach (var widget in visibleWidgetsByDescendingZ)
			{
				if (widget.isMouseThrough)
				{
					hoveredWidget = widget;

					break;
				}
			}
		}

		#endregion


		#region Lassoing

		private Vector2 lassoOrigin;

		public bool isLassoing { get; private set; }

		public Rect lassoArea
		{
			get
			{
				return new Rect()
				{
					xMin = Mathf.Min(lassoOrigin.x, mousePosition.x),
					xMax = Mathf.Max(lassoOrigin.x, mousePosition.x),
					yMin = Mathf.Min(lassoOrigin.y, mousePosition.y),
					yMax = Mathf.Max(lassoOrigin.y, mousePosition.y),
				};
			}
		}

		private void HandleLassoing()
		{
			if (e.IsMouseDrag(MouseButton.Left))
			{
				if (!isLassoing)
				{
					lassoOrigin = mousePosition;
					isLassoing = true;
				}
			}
			else if (e.IsMouseUp(MouseButton.Left))
			{
				if (isSelecting)
				{
					isLassoing = false;
				}

				if (isGrouping)
				{
					isLassoing = false;

					var group = new GraphGroup() {position = groupArea};
					UndoUtility.RecordEditedObject("Create Graph Group");
					graph.elements.Add(group);
					selection.Select(group);
					this.Widget<GraphGroupWidget>(group).FocusLabel();
					GUI.changed = true;
				}
			}
		}

		#endregion


		#region Selecting

		public bool isSelecting => isLassoing && !isGrouping;

		public Rect selectionArea => lassoArea;

		private void HandleSelecting()
		{
			if (e.IsValidateCommand("SelectAll"))
			{
				e.ValidateCommand();
			}
			else if (e.IsExecuteCommand("SelectAll"))
			{
				selection.Select(elementWidgets.Where(widget => widget.canSelect).Select(widget => widget.element));
				e.Use();
			}
			else if (e.IsMouseDown(MouseButton.Left))
			{
				selection.Clear();
			}
			else if (e.IsMouseDrag(MouseButton.Left))
			{
				if (isSelecting)
				{
					// In selection mode, include any widget that overlaps the lasso
					selection.Select(elementWidgets.Where(widget => widget.canSelect && selectionArea.Overlaps(widget.hotArea)).Select(widget => widget.element));
					e.Use();
				}
				else if (isGrouping)
				{
					// In grouping mode, only inlude widgets that are encompassed by the lasso
					selection.Select(elementWidgets.Where(widget => widget.canSelect && groupArea.Encompasses(widget.position)).Select(widget => widget.element));
					e.Use();
				}
			}
		}

		#endregion


		#region Grouping

		public bool isGrouping => isLassoing && (e.ctrlOrCmd && graph.elements.Includes<GraphGroup>() && lassoArea.size.x >= minGroupSize.x && lassoArea.size.y >= minGroupSize.y);

		private Vector2 minGroupSize
		{
			get
			{
				var groupBackground = GraphGroupWidget.Styles.group.normal.background;

				return new Vector2(groupBackground.width, groupBackground.height);
			}
		}

		public Rect groupArea
		{
			get
			{
				var groupBackground = GraphGroupWidget.Styles.group.normal.background;

				var groupRect = selectionArea;

				groupRect.y -= GraphGroupWidget.Styles.headerHeight;
				groupRect.height += GraphGroupWidget.Styles.headerHeight;

				if (groupRect.width < groupBackground.width)
				{
					groupRect.width = groupBackground.width;
				}

				if (groupRect.height < groupBackground.height)
				{
					groupRect.height = groupBackground.height;
				}

				return groupRect;
			}
		}

		#endregion


		#region Dragging

		private static readonly Vector2 HorizontalDrag = new Vector2(1, 0);

		private static readonly Vector2 VerticalDrag = new Vector2(0, 1);

		private static readonly Vector2 FreeDrag = new Vector2(1, 1);

		private Vector2 dragConstraint = FreeDrag;

		private readonly SnappingSystem snapping = new SnappingSystem();

		public bool isDragging { get; private set; }

		private readonly HashSet<IGraphElement> dragGroup = new HashSet<IGraphElement>();

		private readonly HashSet<IGraphElement> snapGroup = new HashSet<IGraphElement>();

		private SnappingPair? horizontalSnapPair;
		private SnappingPair? verticalSnapPair;
		private Vector2 snapOffset;

		protected virtual void RegisterSnappingSets(SnappingSystem snapping) { }

		private void UpdateSnapping()
		{
			snapping.Clear();

			snapGroup.Clear();

			foreach (var dragged in dragGroup)
			{
				var draggedWidget = this.Widget(dragged);

				snapGroup.Add(draggedWidget.element);

				draggedWidget.ExpandSnapGroup(snapGroup);
			}

			foreach (var visibleWidget in visibleWidgetsByAscendingZ)
			{
				if (visibleWidget is IGraphElementWidget visibleElementWidget)
				{
					var type = snapGroup.Contains(visibleElementWidget.element) ? SnappingAnchorType.Snappee : SnappingAnchorType.Snapper;

					visibleElementWidget.RegisterSnappingAnchors(snapping, type);
				}
			}

			horizontalSnapPair = snapping.FindClosestPair(Axis2.Horizontal, LudiqGraphs.Configuration.snapThreshold);
			verticalSnapPair = snapping.FindClosestPair(Axis2.Vertical, LudiqGraphs.Configuration.snapThreshold);

			snapOffset = Vector2.zero;

			if (horizontalSnapPair != null)
			{
				snapOffset += horizontalSnapPair.Value.parallelOffset;
			}

			if (verticalSnapPair != null)
			{
				snapOffset += verticalSnapPair.Value.parallelOffset;
			}
		}

		private void DrawSnappingPair(SnappingPair pair, Color color)
		{
			Handles.color = color;
			DrawAxis(pair.snappee.position, pair.perpendicularAxis);
			DrawAxis(pair.snapper.position, pair.perpendicularAxis);
			Handles.DrawLine(pair.snappee.position, pair.snapper.position);
		}

		private void DrawAxis(Vector2 position, Axis2 axis)
		{
			var start = position - axis.UnaryVector() * 250;
			var end = position + axis.UnaryVector() * 250;
			Handles.DrawLine(start, end);
		}

		private void DebugSnapping()
		{
			if (horizontalSnapPair != null)
			{
				DrawSnappingPair(horizontalSnapPair.Value, Color.green);
			}

			if (verticalSnapPair != null)
			{
				DrawSnappingPair(verticalSnapPair.Value, Color.red);
			}
		}

		public void BeginDrag(EventWrapper e)
		{
			if (isDragging)
			{
				return;
			}

			UndoUtility.RecordEditedObject("Drag Graph Elements");

			isDragging = true;

			dragGroup.Clear();

			dragGroup.UnionWith(selection);

			foreach (var selected in selection)
			{
				this.Widget(selected).ExpandDragGroup(dragGroup);
			}

			foreach (var widget in elementWidgets)
			{
				if (widget.AddToDragGroup(dragGroup))
				{
					dragGroup.Add(widget.element);
				}
			}

			LockDragOrigin();

			foreach (var dragged in dragGroup)
			{
				this.Widget(dragged).BeginDrag();
			}

			e.Use();
		}

		public void Drag(EventWrapper e)
		{
			if (!isDragging)
			{
				return;
			}

			if (e.shift)
			{
				if (dragConstraint == FreeDrag)
				{
					LockDragOrigin();

					if (Mathf.Abs(e.mouseDelta.x) > Mathf.Abs(e.mouseDelta.y))
					{
						dragConstraint = HorizontalDrag;
					}
					else if (Mathf.Abs(e.mouseDelta.y) > Mathf.Abs(e.mouseDelta.x))
					{
						dragConstraint = VerticalDrag;
					}
				}
			}
			else
			{
				dragConstraint = FreeDrag;
			}

			// Update the internal free drag
			foreach (var dragged in dragGroup)
			{
				this.Widget(dragged).FreeDrag(e.mouseDelta);
			}

			// Cache the free positions because the snapping system will use them
			CacheWidgetPositions();

			if (LudiqGraphs.Configuration.snapping)
			{
				// Update the snapping system
				UpdateSnapping();
			}
			else
			{
				snapOffset = Vector2.zero;
			}

			// Apply the final snapped and locked positions
			foreach (var dragged in dragGroup)
			{
				this.Widget(dragged).ApplyDrag(dragConstraint, snapOffset);
			}

			e.Use();
		}

		public void EndDrag(EventWrapper e)
		{
			if (!isDragging)
			{
				return;
			}

			UndoUtility.RecordEditedObject("Drag Graph Elements");

			isDragging = false;

			foreach (var dragged in dragGroup)
			{
				this.Widget(dragged).EndDrag();
			}

			dragGroup.Clear();

			dragConstraint = FreeDrag;
			horizontalSnapPair = null;
			verticalSnapPair = null;
			snapOffset = Vector2.zero;

			e.Use();
		}

		private void LockDragOrigin()
		{
			foreach (var dragged in dragGroup)
			{
				this.Widget(dragged).LockDragOrigin();
			}
		}

		#endregion


		#region Deleting

		public void DeleteSelection()
		{
			var deleted = false;

			var deleteGroup = new HashSet<IGraphElement>();

			foreach (var element in selection)
			{
				deleteGroup.Add(element);
				this.Widget(element).ExpandDeleteGroup(deleteGroup);
			}

			foreach (var element in deleteGroup.OrderByDescending(e => e.dependencyOrder))
			{
				if (this.Widget(element).canDelete)
				{
					UndoUtility.RecordEditedObject("Delete Graph Element");
					graph.elements.Remove(element);
					selection.Remove(element);
					deleted = true;
				}
			}

			if (deleted)
			{
				GUI.changed = true;
				e.TryUse();
			}
		}

		private void HandleDeleting()
		{
			if (e.IsValidateCommand("Delete"))
			{
				if (selection.Count > 0)
				{
					e.ValidateCommand();
				}
			}
			else if (e.IsExecuteCommand("Delete"))
			{
				DeleteSelection();
			}
			else if (e.IsKeyDown(KeyCode.Delete))
			{
				DeleteSelection();
			}
		}

		#endregion


		#region Inspecting

		protected virtual void HandleInspecting()
		{
			if (e.IsKeyDown(KeyCode.Q))
			{
				IGraphElementWidget inspectedWidget = null;

				if (hoveredWidget is IGraphElementWidget hoveredElementWidget)
				{
					inspectedWidget = hoveredElementWidget;
				}
				else if (selection.Count == 1)
				{
					inspectedWidget = this.Widget(selection.Single());
				}

				if (inspectedWidget != null)
				{
					inspectedWidget.ShowInspector();
				}
				else
				{
					ShowInspector();
				}

				e.Use();
			}
		}

		protected virtual bool isInspectable => context.graphMetadata != null;

		public void ShowInspector()
		{
			if (!isInspectable)
			{
				return;
			}

			delayCall += () => GraphInspectorPopup.Open(context, context.graphMetadata, new Rect(e.mousePosition, Vector2.zero));
		}

		#endregion


		#region Clipboard

		public virtual void ShrinkCopyGroup(HashSet<IGraphElement> copyGroup) { }

		private DateTime lastPasteTime;

		private void HandleClipboard()
		{
			if (e.IsValidateCommand("Copy"))
			{
				if (GraphClipboard.canCopySelection)
				{
					e.ValidateCommand();
				}
			}
			else if (e.IsExecuteCommand("Copy"))
			{
				GraphClipboard.CopySelection();
			}

			if (e.IsValidateCommand("Cut"))
			{
				if (GraphClipboard.canCopySelection)
				{
					e.ValidateCommand();
				}
			}
			else if (e.IsExecuteCommand("Cut"))
			{
				GraphClipboard.CutSelection();
			}

			if (e.IsValidateCommand("Paste"))
			{
				if (GraphClipboard.canPaste && (DateTime.UtcNow - lastPasteTime).TotalSeconds >= 0.25)
				{
					e.ValidateCommand();
				}
			}
			else if (e.IsExecuteCommand("Paste"))
			{
				GraphClipboard.Paste();
				lastPasteTime = DateTime.UtcNow;
			}

			if (e.IsValidateCommand("Duplicate"))
			{
				if (GraphClipboard.canDuplicateSelection && (DateTime.UtcNow - lastPasteTime).TotalSeconds >= 0.25)
				{
					e.Use();
				}
			}
			else if (e.IsExecuteCommand("Duplicate"))
			{
				GraphClipboard.DuplicateSelection();
				lastPasteTime = DateTime.UtcNow;
			}
		}

		#endregion


		#region Context

		protected IEnumerable<DropdownOption> GetExtendedContextOptions()
		{
			foreach (var item in context.extensions.SelectMany(extension => extension.contextMenuItems).OrderBy(item => item.label))
			{
				yield return new DropdownOption(item.action, item.label);
			}
		}

		protected virtual IEnumerable<DropdownOption> GetContextOptions()
		{
			yield return new DropdownOption((Action)ShowInspector, "Inspect Graph...");

			foreach (var extendedOption in GetExtendedContextOptions())
			{
				yield return extendedOption;
			}

			if (canCollapseSelection)
			{
				yield return new DropdownOption((Action)CollapseSelection, "Collapse Selection...");
			}

			if (GraphClipboard.canCopySelection)
			{
				yield return new DropdownOption((Action)GraphClipboard.CopySelection, "Copy Selection");
				yield return new DropdownOption((Action)GraphClipboard.CutSelection, "Cut Selection");
			}

			if (GraphClipboard.canDuplicateSelection)
			{
				yield return new DropdownOption((Action)GraphClipboard.DuplicateSelection, "Duplicate Selection");
			}

			if (selection.Count > 0)
			{
				yield return new DropdownOption((Action)DeleteSelection, "Delete Selection");
			}

			if (GraphClipboard.canPasteOutside)
			{
				yield return new DropdownOption((Action<Vector2>)((position) => GraphClipboard.PasteOutside(true, position)), "Paste");
			}
		}

		protected virtual void OnContext()
		{
			var contextOptions = GetContextOptions().ToArray();

			delayCall += () =>
			{
				var _mousePosition = mousePosition;

				LudiqGUI.Dropdown
				(
					e.mousePosition,
					delegate(object _action)
					{
						delayCall += () =>
						{
							if (_action is Action action)
							{
								action.Invoke();
							}
							else if (_action is Action<Vector2> positionedAction)
							{
								positionedAction.Invoke(_mousePosition);
							}
						};
					},
					contextOptions,
					null
				);
			};
		}

		private void HandleContext()
		{
			if (e.IsContextClick)
			{
				OnContext();
				e.Use();
			}
		}

		#endregion


		#region Layout

		public IEnumerable<IGraphElementWidget> alignableAndDistributable
		{
			get { return selection.Select(element => this.Widget(element)).Where(element => element.canAlignAndDistribute); }
		}

		public void Align(AlignOperation operation)
		{
			UndoUtility.RecordEditedObject("Align Graph Elements");

			var alignable = alignableAndDistributable;

			var area = GraphGUI.CalculateArea(alignable);

			foreach (var widget in alignable)
			{
				var position = widget.position;
				var width = position.width;
				var height = position.height;

				switch (operation)
				{
					case AlignOperation.AlignLeftEdges:
						position.xMin = area.xMin;

						break;

					case AlignOperation.AlignCenters:
						position.xMin = area.xMin + (area.width / 2) - (width / 2);

						break;

					case AlignOperation.AlignRightEdges:
						position.xMin = area.xMax - width;

						break;

					case AlignOperation.AlignTopEdges:
						position.yMin = area.yMin;

						break;

					case AlignOperation.AlignMiddles:
						position.yMin = area.yMin + (area.height / 2) - (height / 2);

						break;

					case AlignOperation.AlignBottomEdges:
						position.yMin = area.yMax - height;

						break;
				}

				position.width = width;
				position.height = height;
				widget.position = position.PixelPerfect();
				widget.Reposition();
			}
		}

		public void Distribute(DistributeOperation operation)
		{
			UndoUtility.RecordEditedObject("Distribute Graph Elements");

			var distributable = alignableAndDistributable;

			switch (operation)
			{
				case DistributeOperation.DistributeLeftEdges:
					distributable = distributable.OrderBy(editor => editor.position.xMin);

					break;

				case DistributeOperation.DistributeCenters:
					distributable = distributable.OrderBy(editor => editor.position.center.x);

					break;

				case DistributeOperation.DistributeRightEdges:
					distributable = distributable.OrderBy(editor => editor.position.xMax);

					break;

				case DistributeOperation.DistributeHorizontalGaps:
					distributable = distributable.OrderBy(editor => editor.position.xMin);

					break;

				case DistributeOperation.DistributeTopEdges:
					distributable = distributable.OrderBy(editor => editor.position.yMin);

					break;

				case DistributeOperation.DistributeMiddles:
					distributable = distributable.OrderBy(editor => editor.position.center.y);

					break;

				case DistributeOperation.DistributeBottomEdges:
					distributable = distributable.OrderBy(editor => editor.position.yMax);

					break;

				case DistributeOperation.DistributeVerticalGaps:
					distributable = distributable.OrderBy(editor => editor.position.yMin);

					break;
			}

			var index = 0;
			var count = distributable.Count();
			var first = distributable.First();
			var last = distributable.Last();
			var currentX = first.position.xMin;
			var currentY = first.position.yMin;
			var innerTotalWidth = distributable.Skip(1).Take(count - 2).Sum(innerEditor => innerEditor.position.width);
			var innerTotalHeight = distributable.Skip(1).Take(count - 2).Sum(innerEditor => innerEditor.position.height);
			var horizontalGap = ((last.position.xMin - first.position.xMax) - innerTotalWidth) / (count - 1);
			var verticalGap = ((last.position.yMin - first.position.yMax) - innerTotalHeight) / (count - 1);

			foreach (var widget in distributable)
			{
				var ratio = (float)index / (count - 1);

				var position = widget.position;
				var width = position.width;
				var height = position.height;

				switch (operation)
				{
					case DistributeOperation.DistributeLeftEdges:
						position.xMin = first.position.xMin + (ratio * (last.position.xMin - first.position.xMin));
						position.xMax = position.xMin + width;

						break;

					case DistributeOperation.DistributeCenters:
						position.xMin = first.position.center.x + (ratio * (last.position.center.x - first.position.center.x)) - (width / 2);
						position.xMax = position.xMin + width;

						break;

					case DistributeOperation.DistributeRightEdges:
						position.xMax = last.position.xMax - ((1 - ratio) * (last.position.xMax - first.position.xMax));
						position.xMin = position.xMax - width;

						break;

					case DistributeOperation.DistributeHorizontalGaps:
						position.xMin = currentX;
						position.xMax = position.xMin + width;
						currentX = position.xMax + horizontalGap;

						break;

					case DistributeOperation.DistributeTopEdges:
						position.yMin = first.position.yMin + (ratio * (last.position.yMin - first.position.yMin));
						position.yMax = position.yMin + height;

						break;

					case DistributeOperation.DistributeMiddles:
						position.yMin = first.position.center.y + (ratio * (last.position.center.y - first.position.center.y)) - (height / 2);
						position.yMax = position.yMin + height;

						break;

					case DistributeOperation.DistributeBottomEdges:
						position.yMax = last.position.yMax - ((1 - ratio) * (last.position.yMax - first.position.yMax));
						position.yMin = position.yMax - height;

						break;

					case DistributeOperation.DistributeVerticalGaps:
						position.yMin = currentY;
						position.yMax = position.yMin + height;
						currentY = position.yMax + verticalGap;

						break;
				}

				widget.position = position.PixelPerfect();
				widget.Reposition();

				index++;
			}
		}

		#endregion


		#region Drawing

		protected virtual void DrawBackground()
		{
			if (LudiqCore.Configuration.developerMode && LudiqGraphs.Configuration.debug)
			{
				if (e.controlsMouse && e.controlsKeyboard)
				{
					EditorGUI.DrawRect(viewport, Color.cyan.WithAlpha(0.25f));
				}
				else if (e.controlsMouse)
				{
					EditorGUI.DrawRect(viewport, Color.green.WithAlpha(0.25f));
				}
				else if (e.controlsKeyboard)
				{
					EditorGUI.DrawRect(viewport, Color.blue.WithAlpha(0.25f));
				}
			}
		}

		protected void DrawWidgetsBackground()
		{
			foreach (var widget in visibleWidgetsByAscendingZ)
			{
				if (e.IsRepaint || widget.backgroundRequiresInput)
				{
					widget.DrawBackground();
				}
			}
		}

		protected void DrawWidgetsForeground()
		{
			foreach (var widget in visibleWidgetsByAscendingZ)
			{
				if (e.IsRepaint || widget.foregroundRequiresInput)
				{
					widget.DrawForeground();
				}
			}
		}

		protected void DrawWidgetsOverlay()
		{
			foreach (var widget in visibleWidgetsByAscendingZ)
			{
				if (e.IsRepaint || widget.overlayRequiresInput)
				{
					widget.DrawOverlay();
				}
			}
		}

		protected virtual void DrawOverlay()
		{
			if (isSelecting)
			{
				if (e.IsRepaint)
				{
					((GUIStyle)"SelectionRect").Draw(selectionArea, false, false, false, false);
				}
			}

			if (isGrouping)
			{
				if (e.IsRepaint)
				{
					using (LudiqGUI.color.Override(GraphGroupWidget.Styles.AdjustColor(GraphGroup.defaultColor, true)))
					{
						GraphGroupWidget.Styles.group.Draw(groupArea, false, false, true, false);
					}
				}
			}

			if (LudiqCore.Configuration.developerMode && LudiqGraphs.Configuration.debug)
			{
				EditorGUI.DrawRect(new Rect(graph.pan.x, graph.pan.y - 2, 1, 5), Color.white);
				EditorGUI.DrawRect(new Rect(graph.pan.x - 2, graph.pan.y, 5, 1), Color.white);
				GUI.Label(new Rect(graph.pan, new Vector2(300, 48)), graph.pan.ToString("0"), EditorStyles.whiteLabel);
				GUI.Label(new Rect(mousePosition + new Vector2(16, 0), new Vector2(300, 16)), mousePosition.ToString("0"), EditorStyles.whiteLabel);
				GUI.Label(new Rect(mousePosition + new Vector2(16, 16), new Vector2(300, 16)), GUIUtility.GUIToScreenPoint(mousePosition).ToString("0"), EditorStyles.whiteLabel);
				GUI.Label(new Rect(mousePosition + new Vector2(16, 32), new Vector2(300, 16)), $"Widgets: {widgets.Where(IsVisible).Count()} / {widgets.Count()}", EditorStyles.whiteLabel);

				if (selection.Count > 0)
				{
					var selectionRect = GraphGUI.CalculateArea(selection.Select(e => this.Widget(e)));
					LudiqGUI.DrawEmptyRect(selectionRect, Color.cyan.WithAlpha(0.5f));
					GUI.Label(new Rect(selectionRect.center, new Vector2(300, 16)), selectionRect.center.ToString(), EditorStyles.whiteLabel);
				}

				DebugSnapping();
			}
		}

		#endregion


		#region Drag & Drop

		private IEnumerable<IDragAndDropHandler> potentialDragAndDropHandlers
		{
			get
			{
				if (hoveredWidget != null && hoveredWidget is IDragAndDropHandler)
				{
					yield return (IDragAndDropHandler)hoveredWidget;
				}

				yield return this;

				foreach (var extension in context.extensions)
				{
					yield return extension;
				}
			}
		}

		private IDragAndDropHandler dragAndDropHandler { get; set; }

		private void HandleDragAndDrop()
		{
			if (e.controlType == EventType.DragPerform || e.controlType == EventType.DragUpdated)
			{
				foreach (var potentialDragAndDropHandler in potentialDragAndDropHandlers)
				{
					if (potentialDragAndDropHandler.AcceptsDragAndDrop())
					{
						dragAndDropHandler = potentialDragAndDropHandler;

						break;
					}
				}

				if (dragAndDropHandler != null)
				{
					DragAndDrop.visualMode = dragAndDropHandler.dragAndDropVisualMode;

					if (e.controlType == EventType.DragPerform)
					{
						dragAndDropHandler.PerformDragAndDrop();

						// Don't wait another frame (shows visually), exit right away
						dragAndDropHandler.ExitDragAndDrop();
						dragAndDropHandler = null;
					}
					else if (e.controlType == EventType.DragUpdated)
					{
						dragAndDropHandler.UpdateDragAndDrop();
					}
				}
				else
				{
					DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
				}

				e.Use();
			}

			if (e.controlType == EventType.DragExited && dragAndDropHandler != null)
			{
				dragAndDropHandler.ExitDragAndDrop();
				dragAndDropHandler = null;
				e.Use();
			}
		}

		public virtual DragAndDropVisualMode dragAndDropVisualMode => DragAndDropVisualMode.Generic;

		public virtual bool AcceptsDragAndDrop()
		{
			return false;
		}

		public virtual void PerformDragAndDrop() { }

		public virtual void UpdateDragAndDrop() { }

		public virtual void DrawDragAndDropPreview() { }

		public virtual void ExitDragAndDrop() { }

		#endregion


		#region Timing

		private DateTime lastFrameTime;

		private DateTime lastEventTime;

		private DateTime lastRepaintTime;

		[DoNotSerialize]
		public float frameDeltaTime => (float)(DateTime.UtcNow - lastFrameTime).TotalSeconds;

		[DoNotSerialize]
		public float eventDeltaTime => (float)(DateTime.UtcNow - lastEventTime).TotalSeconds;

		[DoNotSerialize]
		public float repaintDeltaTime => (float)(DateTime.UtcNow - lastRepaintTime).TotalSeconds;

		#endregion


		#region Window

		private static class Contents
		{
			public static readonly GUIContent dimCommand = new GUIContent("Dim", LudiqGraphs.Icons.dimCommand[1], "Dim");
			public static readonly GUIContent snapCommand = new GUIContent("Snap", LudiqGraphs.Icons.snapCommand[1], "Snap");
			public static readonly GUIContent carryCommand = new GUIContent("Carry", LudiqGraphs.Icons.carryCommand[1], "Carry");
			public static readonly GUIContent alignCommand = new GUIContent("Align", LudiqGraphs.Icons.alignCommand[1], "Align");
			public static readonly GUIContent distributeCommand = new GUIContent("Distribute", LudiqGraphs.Icons.distributeCommand[1], "Distribute");
			public static readonly GUIContent overviewCommand = new GUIContent("Overview", LudiqGraphs.Icons.overviewCommand[1], "Overview");
			public static readonly GUIContent fullScreenCommand = new GUIContent("Full Screen", LudiqGraphs.Icons.fullScreenCommand[1], "Full Screen");
			public static readonly GUIContent debugCommand = new GUIContent("Debug", LudiqGraphs.Icons.debugCommand[1], "Debug");
			public static readonly GUIContent clearErrorsCommand = new GUIContent("Clear Errors", LudiqGraphs.Icons.clearErrorsCommand[1], "Clear Errors");
			public static readonly GUIContent collapseCommand = new GUIContent("Collapse", LudiqGraphs.Icons.collapseCommand[1], "Collapse");
			public static readonly GUIContent shareCommand = new GUIContent("Share", LudiqGraphs.Icons.shareCommand[1], "Share");
		}

		public virtual float GetToolbarWidth()
		{
			var width = 0f;

			// Clear Errors

			var debugData = reference.debugData;

			var erroredElementsDebugData = ListPool<IGraphElementDebugData>.New();

			foreach (var elementDebugData in debugData.elementsData)
			{
				if (elementDebugData.runtimeException != null)
				{
					width += LudiqStyles.commandButtonWidth;
					width += LudiqStyles.spaceBetweenCommandToolbars;
					break;
				}
			}

			erroredElementsDebugData.Free();

			// Action
			width += GetActionToolbarWidth();

			width += LudiqStyles.spaceBetweenCommandToolbars;
			
			// Display
			width += GetDisplayToolbarWidth();

			if (LudiqCore.Configuration.developerMode)
			{
				// Debug
				width += LudiqStyles.spaceBetweenCommandToolbars;
				width += LudiqStyles.commandButtonWidth;
			}

			return width;
		}

		protected virtual float GetActionToolbarWidth()
		{
			var width = 0f;

			// Collapse
			width += LudiqStyles.commandButtonWidth;

			// Align
			width += LudiqStyles.commandButtonWidth;

			// Distribute
			width += LudiqStyles.commandButtonWidth;

			// Share
			width += LudiqStyles.commandButtonWidth;

			return width;
		}

		protected virtual float GetDisplayToolbarWidth()
		{
			var width = 0f;

			// Dim
			width += LudiqStyles.commandButtonWidth;

			// Carry
			width += LudiqStyles.commandButtonWidth;

			// Snap
			width += LudiqStyles.commandButtonWidth;

			// Overview
			width += LudiqStyles.commandButtonWidth;

			// Full Screen
			width += LudiqStyles.commandButtonWidth;

			return width;
		}

		protected virtual void OnActionToolbarGUI(bool closeLeft, bool closeRight)
		{
			// Collapse

			var collapseElements = context.selection;

			EditorGUI.BeginDisabledGroup(!CanCollapse(collapseElements));

			if (GUILayout.Button(Contents.collapseCommand, LudiqStyles.CommandButtonSoft(closeLeft, false)))
			{
				CollapseSelection();
			}
			
			EditorGUI.EndDisabledGroup();

			EditorGUI.BeginDisabledGroup(alignableAndDistributable.Count() < 2);

			// Align

			var alignPosition = GUILayoutUtility.GetRect(Contents.alignCommand, LudiqStyles.commandButtonMidSoft);
			
			if (GUI.Button(alignPosition, Contents.alignCommand, LudiqStyles.commandButtonMidSoft))
			{
				LudiqGUI.FuzzyDropdown
				(
					alignPosition,
					EnumOptionTree.For<AlignOperation>(),
					(object)null,
					(operation) => Align((AlignOperation)operation)
				);
			}

			// Distribute

			var distributePosition = GUILayoutUtility.GetRect(Contents.distributeCommand, LudiqStyles.commandButtonMidSoft);
			
			if (GUI.Button(distributePosition, Contents.distributeCommand, LudiqStyles.commandButtonMidSoft))
			{
				LudiqGUI.FuzzyDropdown
				(
					distributePosition,
					EnumOptionTree.For<DistributeOperation>(),
					(object)null,
					(operation) => Distribute((DistributeOperation)operation)
				);
			}

			EditorGUI.EndDisabledGroup();

			// Share

			if (GUILayout.Button(Contents.shareCommand, LudiqStyles.CommandButtonSoft(false, closeRight)))
			{
				GraphSharingUtility.Share(context);
			}
		}

		protected virtual void OnDisplayToolbarGUI(bool closeLeft, bool closeRight)
		{
			EditorGUI.BeginChangeCheck();

			LudiqGraphs.Configuration.dimInactiveNodes = GUILayout.Toggle(LudiqGraphs.Configuration.dimInactiveNodes, Contents.dimCommand, LudiqStyles.CommandButtonSoft(closeLeft, false));

			LudiqGraphs.Configuration.snapping = GUILayout.Toggle(LudiqGraphs.Configuration.snapping, Contents.snapCommand, LudiqStyles.commandButtonMidSoft);

			if (e.ctrlOrCmd)
			{
				GUILayout.Toggle(LudiqGraphs.Configuration.carrying != e.ctrlOrCmd, Contents.carryCommand, LudiqStyles.commandButtonMidSoft);
			}
			else
			{
				LudiqGraphs.Configuration.carrying = GUILayout.Toggle(LudiqGraphs.Configuration.carrying, Contents.carryCommand, LudiqStyles.commandButtonMidSoft);
			}

			if (EditorGUI.EndChangeCheck())
			{
				LudiqGraphs.Configuration.Save();
			}

			if (GUILayout.Button(Contents.overviewCommand, LudiqStyles.commandButtonMidSoft))
			{
				Overview();
			}

			if (GUILayout.Toggle(window.maximized, Contents.fullScreenCommand, LudiqStyles.CommandButtonSoft(false, closeRight)) != window.maximized)
			{
				ToggleMaximized();
			}
		}

		public virtual void OnToolbarGUI()
		{
			// Clear Errors

			var debugData = reference.debugData;

			var erroredElementsDebugData = ListPool<IGraphElementDebugData>.New();

			foreach (var elementDebugData in debugData.elementsData)
			{
				if (elementDebugData.runtimeException != null)
				{
					erroredElementsDebugData.Add(elementDebugData);
				}
			}

			if (erroredElementsDebugData.Count > 0)
			{
				if (GUILayout.Button(Contents.clearErrorsCommand, LudiqStyles.commandButtonSoft))
				{
					foreach (var erroredElementDebugData in erroredElementsDebugData)
					{
						erroredElementDebugData.runtimeException = null;
					}
				}

				GUILayout.Space(LudiqStyles.spaceBetweenCommandToolbars);
			}

			erroredElementsDebugData.Free();
			
			// Action

			OnActionToolbarGUI(true, true);

			GUILayout.Space(LudiqStyles.spaceBetweenCommandToolbars);

			// Display

			OnDisplayToolbarGUI(true, true);
			
			// Debug

			if (LudiqCore.Configuration.developerMode)
			{
				GUILayout.Space(LudiqStyles.spaceBetweenCommandToolbars);

				EditorGUI.BeginChangeCheck();

				LudiqGraphs.Configuration.debug = GUILayout.Toggle(LudiqGraphs.Configuration.debug, Contents.debugCommand, LudiqStyles.commandButtonSoft);

				if (EditorGUI.EndChangeCheck())
				{
					LudiqGraphs.Configuration.Save();
				}
			}
		}

		public Queue<Action> delayedCalls { get; } = new Queue<Action>();

		public event Action delayCall
		{
			add
			{
				lock (delayedCalls)
				{
					delayedCalls.Enqueue(value);
				}
			}
			remove { }
		}

		#endregion


		#region Collapsing

		private bool canCollapseSelection => CanCollapse(selection);

		private void CollapseSelection()
		{
			CollapseDialog.Open(context, GetSelectionCollapseRequest());
		}

		public virtual CollapseRequest GetSelectionCollapseRequest()
		{
			var collapseGroup = new HashSet<IGraphElement>(selection);
			ExpandCollapseGroup(collapseGroup);
			FilterCollapseGroup(collapseGroup);

			var masterGroup = FindMasterGroup(selection);

			if (masterGroup != null)
			{
				return new CollapseRequest(collapseGroup)
				{
					title = masterGroup.label,
					summary = masterGroup.comment,
					masterGroup = masterGroup
				};
			}
			else
			{
				return new CollapseRequest(collapseGroup);
			}
		}

		protected virtual void ExpandCollapseGroup(HashSet<IGraphElement> group)
		{
			var masterGroup = FindMasterGroup(selection);

			if (masterGroup != null)
			{
				group.Clear();
				group.UnionWith(this.Widget<GraphGroupWidget>(masterGroup).elements);
			}
		}

		protected virtual void FilterCollapseGroup(HashSet<IGraphElement> group) { }

		public virtual bool CanCollapse(IEnumerable<IGraphElement> elements)
		{
			return elements.Any();
		}

		public abstract void CollapseToEmbed(CollapseRequest request);

		public abstract IMacro CollapseToMacro(CollapseRequest request);

		protected virtual void HandleCollapsing()
		{
			if (e.IsKeyDown(KeyCode.G) && e.ctrlOrCmd && canCollapseSelection)
			{
				CollapseSelection();
			}
		}

		private GraphGroup FindMasterGroup(ICollection<IGraphElement> elements)
		{
			if (elements.Count == 1 && elements.Single() is GraphGroup onlyGroup)
			{
				return onlyGroup;
			}

			foreach (var element in elements)
			{
				if (element is GraphGroup group)
				{
					var allContainedInGroup = true;

					foreach (var otherElement in elements)
					{
						if (otherElement == group)
						{
							continue;
						}

						var widget = this.Widget(otherElement);

						if (!group.position.Encompasses(widget.position))
						{
							allContainedInGroup = false;
							break;
						}
					}

					if (allContainedInGroup)
					{
						return group;
					}
				}
			}

			return null;
		}

		#endregion


		#region Screenshotting

		private readonly HashSet<IWidget> screenshotGroup = new HashSet<IWidget>();

		public bool isScreenshotting { get; private set; }

		public void BeginScreenshot(IEnumerable<IGraphItem> graphItems)
		{
			screenshotGroup.Clear();

			var widgets = graphItems.Select(this.Widget);

			screenshotGroup.UnionWith(widgets);

			foreach (var widget in widgets)
			{
				widget.ExpandScreenshotGroup(screenshotGroup);
			}

			isScreenshotting = true;

			viewport = GraphGUI.CalculateArea(screenshotGroup).ExpandBy(GraphGUI.Styles.viewportMargin);
		}

		public void EndScreenshot()
		{
			screenshotGroup.Clear();
			isScreenshotting = false;
		}

		#endregion
	}
}