﻿using Ludiq;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Ludiq
{
	public class IdentifierPool
	{
		private static readonly Regex DigitSuffixPattern = new Regex(@"\d+$");

		private IdentifierPool parent;
		private Dictionary<string, int> identifierSuffixes = new Dictionary<string, int>();

		public IdentifierPool(IdentifierPool parent = null)
		{
			this.parent = parent;
		}

		private string ReserveUniqueName(string name)
		{
			if (identifierSuffixes.TryGetValue(name, out int counter))
			{
				counter++;
				identifierSuffixes[name] = counter;
				name += counter;
			}
			else
			{
				identifierSuffixes[name] = 1;
			}

			return name;
		}

		public string CreateIdentifier(string originalName)
		{
			return ReserveUniqueName(DigitSuffixPattern.Replace(originalName.RemoveNonAlphanumeric(), ""));
		}

		public void ReserveMembers(Type type)
		{
			foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				ReserveUniqueName(property.Name);
			}
			foreach (var method in type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				ReserveUniqueName(method.Name);
			}
			foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				ReserveUniqueName(field.Name);
			}
		}
	}
}
