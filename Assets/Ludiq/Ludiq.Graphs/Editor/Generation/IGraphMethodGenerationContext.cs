﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq.CodeDom;

namespace Ludiq
{
	public interface IGraphMethodGenerationContext
	{
		IGraphClassGenerationContext graphClassContext { get; }

		void BindCode(IGraphElement graphElement, IEnumerable<CodeElement> codeElements);
	}
}

