﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public static class GraphGeneratorUtility
	{
		public static TElement Bind<TElement>(this TElement codeElement, IGraphClassGenerationContext context, IGraphElement graphElement)
			where TElement : CodeElement
		{
			context.BindCode(graphElement, codeElement.Yield<CodeElement>());

			return codeElement;
		}

		public static IEnumerable<TElement> Bind<TElement>(this IEnumerable<TElement> elements, IGraphClassGenerationContext context, IGraphElement graphElement)
			where TElement : CodeElement
		{
			foreach (var element in elements)
			{
				yield return element.Bind(context, graphElement);
			}
		}

		public static TElement Bind<TElement>(this TElement codeElement, IGraphMethodGenerationContext context, IGraphElement graphElement)
			where TElement : CodeElement
		{
			context.BindCode(graphElement, codeElement.Yield<CodeElement>());

			return codeElement;
		}

		public static IEnumerable<TElement> Bind<TElement>(this IEnumerable<TElement> elements, IGraphMethodGenerationContext context, IGraphElement graphElement)
			where TElement : CodeElement
		{
			foreach (var element in elements)
			{
				yield return element.Bind(context, graphElement);
			}
		}
	}
}
