﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using Ludiq.CodeDom;

namespace Ludiq
{
	public abstract class GraphGenerator<TGraph> : IGraphGenerator
		where TGraph : class, IGraph
	{
		public GraphGenerator(TGraph graph)
		{
			this.graph = graph;
		}

		protected TGraph graph;
		protected abstract Type graphScriptType { get; }
		protected abstract Type machineScriptType { get; }

		public GeneratedGraph Generate(GraphGenerationSystem generationSystem, string namePrefix)
		{
			var graphScript = GenerateGraphScript(generationSystem, namePrefix);
			var machineScript = GenerateMachineScript(generationSystem, namePrefix, graphScript.requiredMachineEvents);

			return new GeneratedGraph(graphScript, machineScript);
		}

		private GraphClassGenerationContext GenerateGraphScript(GraphGenerationSystem generationSystem, string namePrefix)
		{
			using (ProfilingUtility.SampleBlock("Generate graph script"))
			{
				var compileUnit = new CodeCompileUnit();

				var namespaceDeclaration = new CodeNamespace(GraphGenerationSystem.GeneratedScriptNamespace);
				compileUnit.StartDirectives.Add(new CodePragmaWarningDirective(CodePragmaWarningSetting.Disable, new[] { 162, 219, 429 }));
				compileUnit.Namespaces.Add(namespaceDeclaration);

				var classDeclaration = new CodeClassTypeDeclaration(CodeMemberModifiers.Public, namePrefix + GraphClassGenerationContext.GraphScriptNameSuffix);
				classDeclaration.BaseTypes.Add(CodeFactory.TypeRef(graphScriptType));
				namespaceDeclaration.Types.Add(classDeclaration);

				var context = new GraphClassGenerationContext(generationSystem, graph, namePrefix, compileUnit, classDeclaration, graphScriptType);
				PopulateGraphScript(context);
				return context;
			}
		}

		private GraphClassGenerationContext GenerateMachineScript(GraphGenerationSystem generationSystem, string namePrefix, IEnumerable<string> requiredMachineEvents)
		{
			using (ProfilingUtility.SampleBlock("Generate machine script"))
			{
				var compileUnit = new CodeCompileUnit();

				var namespaceDeclaration = new CodeNamespace(GraphGenerationSystem.GeneratedScriptNamespace);
				compileUnit.Namespaces.Add(namespaceDeclaration);

				var classDeclaration = new CodeClassTypeDeclaration(CodeMemberModifiers.Public, namePrefix + GraphClassGenerationContext.MachineScriptNameSuffix);
				classDeclaration.BaseTypes.Add(CodeFactory.TypeRef(machineScriptType));
				namespaceDeclaration.Types.Add(classDeclaration);

				var context = new GraphClassGenerationContext(generationSystem, graph, namePrefix, compileUnit, classDeclaration, machineScriptType);
				PopulateMachineScript(context, requiredMachineEvents);

				return context;
			}
		}

		protected abstract void PopulateGraphScript(GraphClassGenerationContext context);
		protected abstract void PopulateMachineScript(GraphClassGenerationContext context, IEnumerable<string> requiredMachineEvents);
	}
}
