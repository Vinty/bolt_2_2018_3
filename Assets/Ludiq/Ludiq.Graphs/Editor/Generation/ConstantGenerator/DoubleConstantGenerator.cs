﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Double))]	
	public class DoubleConstantGenerator : ConstantGenerator<Double>
	{
		public DoubleConstantGenerator(Double value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
