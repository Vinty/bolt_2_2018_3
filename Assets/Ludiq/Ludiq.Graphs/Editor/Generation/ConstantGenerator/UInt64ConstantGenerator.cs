﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(UInt64))]	
	public class UInt64ConstantGenerator : ConstantGenerator<UInt64>
	{
		public UInt64ConstantGenerator(UInt64 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
