﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(UInt16))]	
	public class UInt16ConstantGenerator : ConstantGenerator<UInt16>
	{
		public UInt16ConstantGenerator(UInt16 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
