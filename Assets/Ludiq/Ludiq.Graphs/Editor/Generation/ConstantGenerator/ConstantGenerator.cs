﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	public abstract class ConstantGenerator<TValue> : IConstantGenerator
	{
		public ConstantGenerator(TValue value)
		{
			this.value = value;
		}

		protected TValue value;

		public abstract CodeExpression GenerateExpression();
	}
}
