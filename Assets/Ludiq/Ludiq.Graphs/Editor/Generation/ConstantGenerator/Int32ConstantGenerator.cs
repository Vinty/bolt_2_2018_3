﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Int32))]	
	public class Int32ConstantGenerator : ConstantGenerator<Int32>
	{
		public Int32ConstantGenerator(Int32 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
