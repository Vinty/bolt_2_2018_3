﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Quaternion))]	
	public class QuaternionConstantGenerator : ConstantGenerator<Quaternion>
	{
		public QuaternionConstantGenerator(Quaternion value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Quaternion)).ObjectCreate(
				CodeFactory.Primitive(value.x),
				CodeFactory.Primitive(value.y),
				CodeFactory.Primitive(value.z),
				CodeFactory.Primitive(value.w)
			);
		}
	}
}
