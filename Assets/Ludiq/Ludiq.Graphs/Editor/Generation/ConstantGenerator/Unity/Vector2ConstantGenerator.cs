﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Vector2))]	
	public class Vector2ConstantGenerator : ConstantGenerator<Vector2>
	{
		public Vector2ConstantGenerator(Vector2 value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Vector2));

			if (value == Vector2.zero) return typeRef.Expression().Field("zero");
			else if (value == Vector2.one) return typeRef.Expression().Field("one");
			else if (value == Vector2.left) return typeRef.Expression().Field("left");
			else if (value == Vector2.right) return typeRef.Expression().Field("right");
			else if (value == Vector2.up) return typeRef.Expression().Field("up");
			else if (value == Vector2.down) return typeRef.Expression().Field("down");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y)
				);
			}
		}
	}
}
