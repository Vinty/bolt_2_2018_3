﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Vector4))]	
	public class Vector4ConstantGenerator : ConstantGenerator<Vector4>
	{
		public Vector4ConstantGenerator(Vector4 value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Vector4));
			
			if (value == Vector4.zero) return typeRef.Expression().Field("zero");
			else if (value == Vector4.one) return typeRef.Expression().Field("one");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y),
					CodeFactory.Primitive(value.z),
					CodeFactory.Primitive(value.w)
				);
			}
		}
	}
}
