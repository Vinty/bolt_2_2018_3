﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Vector3))]	
	public class Vector3ConstantGenerator : ConstantGenerator<Vector3>
	{
		public Vector3ConstantGenerator(Vector3 value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Vector3));

			if (value == Vector3.zero) return typeRef.Expression().Field("zero");
			else if (value == Vector3.one) return typeRef.Expression().Field("one");
			else if (value == Vector3.left) return typeRef.Expression().Field("left");
			else if (value == Vector3.right) return typeRef.Expression().Field("right");
			else if (value == Vector3.up) return typeRef.Expression().Field("up");
			else if (value == Vector3.down) return typeRef.Expression().Field("down");
			else if (value == Vector3.forward) return typeRef.Expression().Field("forward");
			else if (value == Vector3.back) return typeRef.Expression().Field("back");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y),
					CodeFactory.Primitive(value.z)
				);
			}
		}
	}
}
