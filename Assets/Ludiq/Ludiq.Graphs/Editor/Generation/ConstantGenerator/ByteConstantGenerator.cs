﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Byte))]	
	public class ByteConstantGenerator : ConstantGenerator<Byte>
	{
		public ByteConstantGenerator(Byte value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
