﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public class GeneratedGraph
	{
		public GraphClassGenerationContext graphScript { get; }
		public GraphClassGenerationContext machineScript { get; }

		public GeneratedGraph(GraphClassGenerationContext graphScript, GraphClassGenerationContext machineScript)
		{
			this.graphScript = graphScript;
			this.machineScript = machineScript;
		}

		public IEnumerable<CodePredeclaredType> PredeclaredTypes
		{
			get
			{
				yield return graphScript.predeclaredType;
				yield return machineScript.predeclaredType;
			}
		}

		public void WriteClasses(ICodeWriterSystem writerSystem, CodeGeneratorOptions options)
		{
			graphScript.WriteClass(writerSystem, options);
			machineScript.WriteClass(writerSystem, options);
		}
	}
}
