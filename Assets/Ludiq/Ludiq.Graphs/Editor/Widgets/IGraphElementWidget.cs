﻿using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public interface IGraphElementWidget : IWidget
	{
		IGraphElement element { get; }

		#region Selecting

		bool canSelect { get; }

		bool isSelected { get; }

		#endregion

		#region Layouting

		bool canAlignAndDistribute { get; }

		#endregion

		#region Clipboard

		bool canCopy { get; }

		void ExpandCopyGroup(HashSet<IGraphElement> group);

		#endregion

		#region Resizing

		bool canResizeHorizontal { get; }
		bool canResizeVertical { get; }
		bool isResizing { get; }

		#endregion

		#region Dragging

		bool canDrag { get; }
		bool isDragging { get; }
		void BeginDrag();
		void LockDragOrigin();
		void FreeDrag(Vector2 delta);
		void ApplyDrag(Vector2 constraint, Vector2 snapOffset);
		void EndDrag();
		void ExpandDragGroup(HashSet<IGraphElement> group);
		bool AddToDragGroup(HashSet<IGraphElement> group);
		void ExpandSnapGroup(HashSet<IGraphElement> group);
		void RegisterSnappingAnchors(SnappingSystem snapping, SnappingAnchorType type);

		#endregion

		#region Deleting

		bool canDelete { get; }
		void Delete();
		void ExpandDeleteGroup(HashSet<IGraphElement> group);

		#endregion

		#region Inspecting

		void ShowInspector();
	
		#endregion
	}
}