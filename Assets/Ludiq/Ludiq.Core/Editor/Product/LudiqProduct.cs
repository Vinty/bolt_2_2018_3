﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Product(ID)]
	public sealed class LudiqProduct : Product
	{
		public LudiqProduct() { }

		public override string configurationPanelLabel => "Ludiq";

		public override string name => "Ludiq Framework";
		public override string description => "Complete toolset for quality Unity plugin development.";
		public override string authorLabel => "Designed & Developed by ";
		public override string author => "Lazlo Bonin";
		public override string copyrightHolder => "Ludiq";
		public override string communityUrl => "http://support.ludiq.io";
		public override SemanticVersion version => "2.0.0a3";
		public const string ID = "Ludiq";

		public const int ToolsMenuPriority = -1000000;
		public const int DeveloperToolsMenuPriority = ToolsMenuPriority + 1000;

		public static LudiqProduct instance => (LudiqProduct)ProductContainer.GetProduct(ID);

		public override void Initialize()
		{
			base.Initialize();

			logo = LudiqCore.Resources.LoadTexture("Logos/LogoFramework.png", CreateTextureOptions.Scalable).Single();
			authorLogo = LudiqCore.Resources.LoadTexture("Logos/LogoLudiq.png", CreateTextureOptions.Scalable).Single();
		}

		[PreferenceItem("Ludiq")]
		private static void HookConfigurationPanel()
		{
			instance.configurationPanel.PreferenceItem();
		}

		[MenuItem("Tools/Ludiq/About...", priority = ToolsMenuPriority + 1)]
		private static void HookAboutWindow()
		{
			instance.aboutWindow.Show();
		}

		[MenuItem("Tools/Ludiq/Support...", priority = ToolsMenuPriority + 2)]
		private static void HookSupport()
		{
			WebWindow.Show(new GUIContent("Ludiq Support", LudiqCore.Icons.supportWindow[IconSize.Small]), instance.communityUrl);
		}

		[MenuItem("Tools/Ludiq/Setup Wizard...", priority = ToolsMenuPriority + 101)]
		private static void HookSetupWizard()
		{
			instance.setupWizard.Show();
		}

		[MenuItem("Tools/Ludiq/Update Wizard...", priority = ToolsMenuPriority + 102)]
		private static void HookUpdateWizard()
		{
			UpdateWizard.global.Show();
		}

		[MenuItem("Tools/Ludiq/Configuration...", priority = ToolsMenuPriority + 201)]
		private static void HookConfiguration()
		{
			instance.configurationPanel.Show();
		}
	}
}