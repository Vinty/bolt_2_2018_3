﻿using System;

namespace Ludiq
{
	[FuzzyOption(typeof(Type))]
	public class TypeOption : DocumentedOption<Type>
	{
		public TypeOption(Type type, FuzzyOptionMode mode) : base(mode)
		{
			value = type;
			label = type.DisplayName();
			getIcon = type.Icon;
			documentation = type.Documentation();
			zoom = true;
		}

		public override string SearchResultLabel(string query)
		{
			return $"{SearchUtility.HighlightQuery(haystack, query)} <color=#{ColorPalette.unityForegroundDim.ToHexString()}>(in {value.Namespace().DisplayName()})</color>";
		}
	}
}