﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public class TypeTree
	{
		private Type _type = null;

		public TypeTree()
		{
		}

		public TypeTree(Type type)
		{
			this.type = type;
		}

		public TypeTree(Type substitutedType, Type genericParameter)
		{
			type = substitutedType;

			this.genericParameter = genericParameter;

			humanLabel = NameUtility.HumanName(genericParameter);
			programmerLabel = genericParameter.Name;
		}

		public Type type
		{
			get
			{
				return _type;
			}

			set
			{
				if (_type != value)
				{
					_type = value;

					genericParameter = null;
					genericTypeDefinition = null;
					humanLabel = null;
					programmerLabel = null;
					children = null;

					if (_type != null)
					{
						if (_type.IsGenericParameter)
						{
							genericParameter = _type;
							humanLabel = NameUtility.HumanName(_type);
							programmerLabel = _type.Name;
						}
						else if (_type.IsGenericType)
						{
							genericTypeDefinition = _type.GetGenericTypeDefinition();

							var genericParameters = genericTypeDefinition.GetGenericArguments();
							var typeArguments = _type.GetGenericArguments();

							children = new TypeTree[typeArguments.Length];

							for (int i = 0; i < typeArguments.Length; i++)
							{
								children[i] = new TypeTree(typeArguments[i], genericParameters[i]);
							}
						}
					}
				}
			}
		}

		public string humanLabel { get; private set; }
		public string programmerLabel { get; private set; }
		public Type genericParameter { get; private set; }
		public Type genericTypeDefinition { get; private set; }
		public TypeTree[] children { get; private set; }

		public int recursiveNodeCount
		{
			get
			{
				var result = 1;

				if (children != null)
				{
					foreach (var child in children)
					{
						result += child.recursiveNodeCount;
					}
				}

				return result;
			}
		}

		public int recursiveDepth
		{
			get
			{
				var result = 1;

				if (children != null)
				{
					foreach (var child in children)
					{
						result = Math.Max(result, child.recursiveDepth + 1);
					}
				}

				return result;
			}
		}

		public bool IsClosedForm()
		{
			if (genericTypeDefinition != null)
			{
				foreach (var child in children)
				{
					if (!child.IsClosedForm())
					{
						return false;
					}
				}

				return true;
			}
			else
			{
				return true;
			}
		}

		public Type CreateType()
		{
			if (_type == null)
			{
				throw new InvalidOperationException("Cannot create null type");
			}
			if (!IsClosedForm())
			{
				throw new InvalidOperationException("Cannot create type unless it is in closed form");
			}

			if (genericTypeDefinition != null)
			{
				var genericTypeArguments = new Type[children.Length];

				for (int i = 0; i < children.Length; i++)
				{
					genericTypeArguments[i] = children[i].CreateType();
				}

				return genericTypeDefinition.MakeGenericType(genericTypeArguments);
			}
			else
			{
				return type;
			}
		}
	}
}
