﻿namespace Ludiq
{
	[FuzzyOption(typeof(Namespace))]
	public class NamespaceOption : FuzzyOption<Namespace>
	{
		public NamespaceOption(Namespace @namespace, FuzzyOptionMode mode) : base(mode)
		{
			value = @namespace;
			label = @namespace.DisplayName(false);
			getIcon = @namespace.Icon;
		}

		public override string haystack => value.DisplayName();
	}
}