﻿using System.IO;

namespace Ludiq
{
	public static class FastSerializerUtilities
	{
		public static void WriteNullable(this BinaryWriter writer, string val)
		{
			var hasVal = val != null;

			writer.Write(hasVal);

			if (hasVal)
			{
				writer.Write(val);
			}
		}

		public static string ReadNullableString(this BinaryReader reader)
		{
			if (reader.ReadBoolean())
			{
				return reader.ReadString();
			}
			else
			{
				return null;
			}
		}
	}
}
