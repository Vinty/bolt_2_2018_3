﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Ludiq.OdinSerializer;
using Ludiq.FullSerializer;
using UnityEditor;
using UnityEngine;
using YamlDotNet.RepresentationModel;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public static class EditorSerializationUtility
	{
		#region Policy Testing

		[MenuItem("Tools/Ludiq/Developer/Test Serialization Policies")]
		public static void TestSerializationPolicies()
		{
			var typesToTest = Codebase.types.Where(t => t.Namespace == "Ludiq" || t.Namespace == "Bolt" || t.Namespace().Root.Name == "UnityEngine").ToHashSet();
			
			float i = 0;

			var matching = 0;

			foreach (var type in typesToTest)
			{
				ProgressUtility.DisplayProgressBar("Test Serialization Policies", type.CSharpName(), i++ / typesToTest.Count);

				if (TestSerializationPolicies(type))
				{
					matching++;
				}
			}
			
			Debug.Log($"Serialization policy test complete. \nSuccessfully matched: {matching} / {typesToTest.Count}");

			ProgressUtility.ClearProgressBar();
		}

		private static bool TestSerializationPolicies(Type type)
		{
			var fullSerializerMembers = fsMetaType
				.Get(new fsConfig(), type)
				.Properties
				.Select(p => p._memberInfo)
				.OrderBy(m => m.MetadataToken)
				.ToList();

			var odinSerializerMembers = FormatterUtilities.GetSerializableMembers(type, SerializationPolicy.instance)
				.OrderBy(m => m.MetadataToken)
				.ToList();

			var matches = fullSerializerMembers.SequenceEqual(odinSerializerMembers);

			if (!matches)
			{
				Debug.LogWarning($"Serialization Policy mismatch on {type}: \n\nFull Serializer members:\n{fullSerializerMembers.Select(m => m.Name).ToLineSeparatedString()}\n\nOdin Serializer members:\n{odinSerializerMembers.Select(m => m.Name).ToLineSeparatedString()}\n");
			}

			return matches;
		}

		#endregion


		#region YAML Deserialization

		public static void DeserializeYamlAsset(UnityObject asset, string topNodeKey = "MonoBehaviour", string dataNodeKey = "_data")
		{
			Ensure.That(nameof(asset)).IsNotNull(asset);

			var data = DeserializeYamlAsset(AssetDatabase.GetAssetPath(asset), topNodeKey, dataNodeKey);

			var dataField = new Member(asset.GetType(), dataNodeKey, Type.EmptyTypes, asset);

			dataField.Set(data);

			(asset as ISerializationCallbackReceiver)?.OnAfterDeserialize();
		}

		public static FullSerializationData DeserializeYamlAsset(string asset, string topNodeKey = "MonoBehaviour", string dataNodeKey = "_data")
		{
			Ensure.That(nameof(asset)).IsNotNull(asset);
			Ensure.That(nameof(topNodeKey)).IsNotNull(topNodeKey);
			Ensure.That(nameof(dataNodeKey)).IsNotNull(dataNodeKey);

			var assetPath = Path.Combine(Paths.project, asset);

			if (!File.Exists(assetPath))
			{
				throw new FileNotFoundException("Asset file not found.", assetPath);
			}

			try
			{
				var input = new StreamReader(assetPath);

				var yaml = new YamlStream();

				yaml.Load(input);

				// Find the data node.
				var rootNode = (YamlMappingNode)yaml.Documents[0].RootNode;
				var topNode = (YamlMappingNode)rootNode.Children[topNodeKey];
				var dataNode = (YamlMappingNode)topNode.Children[dataNodeKey];
				var jsonNode = (YamlScalarNode)dataNode.Children["_json"];
				var objectReferencesNode = (YamlSequenceNode)dataNode.Children["_objectReferences"];

				// Read the contents
				var json = jsonNode.Value;

				var objectReferences = new List<UnityObject>();

				foreach (var objectReferenceNode in objectReferencesNode.Children.Cast<YamlScalarNode>())
				{
					objectReferences.Add(EditorUtility.InstanceIDToObject(int.Parse(objectReferenceNode.Value)));
				}

				// Return the final serialization data
				return new FullSerializationData(json, objectReferences);
			}
			catch (Exception ex)
			{
				throw new SerializationException("Failed to deserialize YAML asset.", ex);
			}
		}

		#endregion
	}
}