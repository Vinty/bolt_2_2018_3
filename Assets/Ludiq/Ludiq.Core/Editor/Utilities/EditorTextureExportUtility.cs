﻿using System;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public static class EditorTextureExportUtility
	{
		private static Texture2D CreateReadableCopy(Texture2D source)
		{
			RenderTexture rt = RenderTexture.GetTemporary(
				source.width,
				source.height,
				0,
				RenderTextureFormat.Default,
				RenderTextureReadWrite.Linear);

			Graphics.Blit(source, rt);
			RenderTexture previous = RenderTexture.active;
			RenderTexture.active = rt;
			Texture2D readableText = new Texture2D(source.width, source.height);
			readableText.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
			readableText.Apply();
			RenderTexture.active = previous;
			RenderTexture.ReleaseTemporary(rt);
			return readableText;
		}

		[MenuItem("Tools/Ludiq/Developer/Export All Editor Textures...", priority = LudiqProduct.DeveloperToolsMenuPriority + 302)]
		public static void ExportAllEditorIcon()
		{
			var assetBundle = typeof(EditorGUIUtility).GetMethod("GetEditorAssetBundle", BindingFlags.Static | BindingFlags.NonPublic)
			                                          .Invoke(null,null) as AssetBundle;
			
			var outputFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "EditorTextures");

			PathUtility.CreateDirectoryIfNeeded(outputFolder);

			var textures = assetBundle.LoadAllAssets<Texture2D>();
			
			try
			{
				for (int i = 0; i < textures.Length; i++)
				{
					var texture = textures[i];

					try
					{
						ProgressUtility.DisplayProgressBar("Export Editor Textures", texture.name, (float)i / textures.Length);
						var outputPath = Path.Combine(outputFolder, texture.name + ".png");
						File.WriteAllBytes(outputPath, CreateReadableCopy(texture).EncodeToPNG());
					}
					catch (Exception ex)
					{
						Debug.LogError($"Failed to export {texture.name}:\n{ex}");
					}
				}
			}
			finally
			{
				ProgressUtility.ClearProgressBar();
			}
		}
	}
}