﻿using System;
using System.Collections.Generic;

namespace Ludiq
{
	[Plugin(LudiqCore.ID)]
	internal class Changelog_2_0_0a1 : PluginChangelog
	{
		public Changelog_2_0_0a1(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a1";
		public override DateTime date => new DateTime(2018, 11, 07);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Odin Serializer Dependency";
				yield return "[Added] Layout Swapping Hotkeys (Ctrl/Cmd+Space)";
				yield return "[Added] Unity Message Proxies";
				yield return "[Added] VectorXInt Support";
				yield return "[Changed] Folder Structure";
			}
		}
	}

	[Plugin(LudiqCore.ID)]
	internal class Changelog_2_0_0a3 : PluginChangelog
	{
		public Changelog_2_0_0a3(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a3";
		public override DateTime date => new DateTime(2018, 12, 21);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Breadcrumbs to fuzzy window";
				yield return "[Added] Category search in fuzzy window";
				yield return "[Added] Script reference resolution for all Bolt 1 scripts";
				yield return "[Added] Options panel to Extractor";
				yield return "[Added] Nested types inclusion for hierarchical type extraction";
				yield return "[Added] Nested type icons now use fallback to their parent type";
				yield return "[Changed] Merged AOT Pre-Build in Generation process";
				yield return "[Optimized] General search and option fetching speed";
				yield return "[Fixed] Fuzzy Finder rendering glitches";
			}
		}
	}
}