﻿namespace Ludiq
{
	[Plugin(LudiqCore.ID)]
	public sealed class LudiqCoreManifest : PluginManifest
	{
		private LudiqCoreManifest(LudiqCore plugin) : base(plugin) { }

		public override string name => "Ludiq Core";
		public override string author => "Ludiq";
		public override string description => "IoC framework and toolset for Unity plugin development.";
		public override SemanticVersion version => "2.0.0a3";
	}
}