﻿namespace Ludiq
{
	[Plugin(LudiqCore.ID)]
	internal class Acknowledgement_EditorIconViewer : PluginAcknowledgement
	{
		public Acknowledgement_EditorIconViewer(Plugin plugin) : base(plugin) { }

		public override string title => "Editor Icon Viewer";
		public override string author => "Zach Aikman";
	}
}