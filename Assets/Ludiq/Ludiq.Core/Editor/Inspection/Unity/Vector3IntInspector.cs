﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Inspector(typeof(Vector3Int))]
	public class Vector3IntInspector : VectorInspector
	{
		public Vector3IntInspector(Metadata metadata) : base(metadata) { }

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			Vector3Int newValue;
			
			if (adaptiveWidth)
			{
				newValue = LudiqGUI.AdaptiveVector3IntField(position, GUIContent.none, (Vector3Int)metadata.value);
			}
			else if (position.width <= Styles.compactThreshold)
			{
				newValue = LudiqGUI.CompactVector3IntField(position, GUIContent.none, (Vector3Int)metadata.value);
			}
			else
			{
				newValue = EditorGUI.Vector3IntField(position, GUIContent.none, (Vector3Int)metadata.value);
			}

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newValue;
			}
		}

		public override float GetAdaptiveWidth()
		{
			var vector = (Vector3Int)metadata.value;

			return LudiqGUI.GetTextFieldAdaptiveWidth(vector.x) + LudiqStyles.compactHorizontalSpacing +
				   LudiqGUI.GetTextFieldAdaptiveWidth(vector.y) + LudiqStyles.compactHorizontalSpacing +
				   LudiqGUI.GetTextFieldAdaptiveWidth(vector.z) + LudiqStyles.compactHorizontalSpacing;
		}
	}
}