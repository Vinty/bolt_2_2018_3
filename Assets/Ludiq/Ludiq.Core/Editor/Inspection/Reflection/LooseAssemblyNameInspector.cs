using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Inspector(typeof(LooseAssemblyName))]
	public sealed class LooseAssemblyNameInspector : Inspector
	{
		public LooseAssemblyNameInspector(Metadata metadata) : base(metadata) { }

		protected override float GetHeight(float width, GUIContent label)
		{
			return HeightWithLabel(metadata, width, EditorGUIUtility.singleLineHeight, label);
		}

		private IFuzzyOptionTree GetOptions()
		{
			return new LooseAssemblyNameOptionTree();
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			var fieldPosition = new Rect
			(
				position.x,
				position.y,
				position.width,
				EditorGUIUtility.singleLineHeight
			);
			
			var newAssemblyName = LudiqGUI.AssemblyField(fieldPosition, GUIContent.none, (LooseAssemblyName)metadata.value, GetOptions);

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newAssemblyName;
			}
		}
	}
}