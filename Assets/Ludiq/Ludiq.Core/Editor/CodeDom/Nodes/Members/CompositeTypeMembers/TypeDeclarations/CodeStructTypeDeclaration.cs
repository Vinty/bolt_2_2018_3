﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ludiq.CodeDom
{
	public sealed class CodeStructTypeDeclaration : CodeCompositeTypeDeclaration
	{
		public CodeStructTypeDeclaration(CodeMemberModifiers modifiers, string name)
			: base(modifiers, name)
		{
		}

		public override bool IsInterface => false;
		public override string Keyword => "struct";
	}
}
