﻿using UnityEngine;

namespace Ludiq
{
	public interface IFuzzyOption
	{
		object value { get; }
		FuzzyOptionMode mode { get; }

		string label { get; }
		string haystack { get; }
		EditorTexture Icon();
		GUIStyle style { get; }

		string headerLabel { get; }
		bool showHeaderIcon { get; }

		bool hasFooter { get; }
		float GetFooterHeight(FuzzyOptionNode node, float width);
		void OnFooterGUI(FuzzyOptionNode node, Rect position);

		string SearchResultLabel(string query);

		void OnPopulate();
	}
}