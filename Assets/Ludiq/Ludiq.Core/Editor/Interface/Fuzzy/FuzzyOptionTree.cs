﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Ludiq
{
	public abstract class FuzzyOptionTree : IFuzzyOptionTree
	{
		protected FuzzyOptionTree() { }

		protected FuzzyOptionTree(GUIContent header) : this()
		{
			this.header = header;
		}

		public GUIContent header { get; set; }

		public ICollection<object> selected { get; } = new HashSet<object>();

		public bool showBackgroundWorkerProgress { get; protected set; }

		public virtual void Prewarm() { }

		#region Multithreading

		public bool multithreaded { get; } = true;

		#endregion

		#region Hierarchy

		public abstract IEnumerable<IFuzzyOption> Root();

		public abstract IEnumerable<IFuzzyOption> Children(IFuzzyOption parent);

		public abstract IEnumerable<IFuzzyOption> SearchableChildren(IFuzzyOption parent);

		#endregion

		#region Search

		public virtual bool searchable { get; } = false;

		public virtual IEnumerable<IFuzzyOption> OrderedSearchResults(string query, IFuzzyOption parent, CancellationToken cancellation)
		{
			return SearchResults(query, parent, cancellation).OrderByRelevance();
		}

		public virtual IEnumerable<ISearchResult<IFuzzyOption>> SearchResults(string query, IFuzzyOption parent, CancellationToken cancellation)
		{
			return Enumerable.Empty<ISearchResult<IFuzzyOption>>();
		}

		public virtual string SearchResultLabel(IFuzzyOption item, string query)
		{
			return item.SearchResultLabel(query);
		}

		#endregion

		#region Favorites

		public virtual ICollection<IFuzzyOption> favorites => null;

		public virtual bool UseExplicitRootLabel(IFuzzyOption item)
		{
			return false;	
		}

		public virtual string ExplicitLabel(IFuzzyOption item)
		{
			throw new NotSupportedException();
		}

		public virtual bool CanFavorite(IFuzzyOption item)
		{
			throw new NotSupportedException();
		}

		public virtual void OnFavoritesChange() { }

		#endregion
	}
}