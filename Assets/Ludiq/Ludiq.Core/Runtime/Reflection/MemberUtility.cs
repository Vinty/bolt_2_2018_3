using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public static class MemberUtility
	{
		static MemberUtility()
		{
			var extensionMethodDeclarers = RuntimeCodebase.types.Where(type => type.IsStatic() && !type.IsGenericType && !type.IsNested);

			definedExtensionMethods = new Dictionary<Type, HashSet<MethodInfo>>();
			resolvedExtensionMethods = new Dictionary<Type, HashSet<MethodInfo>>();
			genericExtensionMethods = new HashSet<MethodInfo>();

			foreach (var extensionMethodDeclarer in extensionMethodDeclarers)
			{
				var extensionMethods = extensionMethodDeclarer.GetMethods().Where(m => m.IsExtension());

				foreach (var extensionMethod in extensionMethods)
				{
					var thisParameterType = extensionMethod.GetThisParameter(false).ParameterType;

					if (!definedExtensionMethods.TryGetValue(thisParameterType, out var extensionMethodsCache))
					{
						extensionMethodsCache = new HashSet<MethodInfo>();
						definedExtensionMethods.Add(thisParameterType, extensionMethodsCache);
					}

					extensionMethodsCache.Add(extensionMethod);
				}
			}

			definedOperators = new Dictionary<Type, HashSet<MethodInfo>>();
			definedOperatorNames = new Dictionary<Type, HashSet<string>>();

			foreach (var type in RuntimeCodebase.types)
			{
				definedOperators[type] = new HashSet<MethodInfo>();
				definedOperatorNames[type] = new HashSet<string>();
			}
			foreach (var type in RuntimeCodebase.types)
			{
				foreach (var method in type.GetMethods())
				{
					if (method.IsOperator())
					{
						definedOperators[type].Add(method);
						definedOperatorNames[type].Add(method.Name);

						var parameters = method.GetParameters();
						if (parameters.Length == 2)
						{
							var leftType = parameters[0].ParameterType;
							var rightType = parameters[1].ParameterType;
							if (leftType != type)
							{
								definedOperators[leftType].Add(method);
								definedOperatorNames[leftType].Add(method.Name);
							}
							else if (rightType != type)
							{
								definedOperators[rightType].Add(method);
								definedOperatorNames[rightType].Add(method.Name);
							}
						}
					}
				}
			}
		}

		private static readonly Dictionary<Type, HashSet<MethodInfo>> definedExtensionMethods;
		private static readonly Dictionary<Type, HashSet<MethodInfo>> resolvedExtensionMethods;
		private static readonly HashSet<MethodInfo> genericExtensionMethods;
		private static readonly Dictionary<Type, HashSet<MethodInfo>> definedOperators;
		private static readonly Dictionary<Type, HashSet<string>> definedOperatorNames;
	
		public static bool IsOperator(this MethodInfo method)
		{
			return method.IsSpecialName && OperatorUtility.operatorSymbols.ContainsKey(method.Name);
		}

		public static bool IsOperator(this MemberInfo memberInfo)
		{
			return memberInfo is MethodInfo methodInfo && methodInfo.IsOperator();
		}

		public static OperatorCategory GetOperatorCategory(this MethodInfo method)
		{
			if (method.IsSpecialName)
			{
				if (OperatorUtility.TryGetUnaryByMethodName(method.Name, out var unaryOperator))
				{
					return unaryOperator.GetOperatorCategory();
				}
				if (OperatorUtility.TryGetBinaryByMethodName(method.Name, out var binaryOperator))
				{
					return binaryOperator.GetOperatorCategory();
				}
			}

			return OperatorCategory.None;
		}

		public static OperatorCategory GetOperatorCategory(this MemberInfo memberInfo)
		{
			if (memberInfo is MethodInfo methodInfo)
			{
				return methodInfo.GetOperatorCategory();
			}

			return OperatorCategory.None;
		}

		public static bool IsUserDefinedConversion(this MethodInfo method)
		{
			return method.IsSpecialName && (method.Name == "op_Implicit" || method.Name == "op_Explicit");
		}

		/// <remarks>This may return an open-constructed method as well.</remarks>
		public static MethodInfo MakeGenericMethodVia(this MethodInfo openConstructedMethod, params Type[] closedConstructedParameterTypes)
		{
			using (ProfilingUtility.SampleBlock(nameof(MakeGenericMethodVia)))
			{
				Ensure.That(nameof(openConstructedMethod)).IsNotNull(openConstructedMethod);
				Ensure.That(nameof(closedConstructedParameterTypes)).IsNotNull(closedConstructedParameterTypes);

				if (!openConstructedMethod.ContainsGenericParameters)
				{
					// The method contains no generic parameters,
					// it is by definition already resolved.
					return openConstructedMethod;
				}

				var openConstructedParameterTypes = openConstructedMethod.GetParameters().Select(p => p.ParameterType).ToArray();

				if (openConstructedParameterTypes.Length != closedConstructedParameterTypes.Length)
				{
					throw new ArgumentOutOfRangeException(nameof(closedConstructedParameterTypes));
				}

				var resolvedGenericParameters = new Dictionary<Type, Type>();

				for (var i = 0; i < openConstructedParameterTypes.Length; i++)
				{
					// Resolve each open-constructed parameter type via the equivalent
					// closed-constructed parameter type.

					var openConstructedParameterType = openConstructedParameterTypes[i];
					var closedConstructedParameterType = closedConstructedParameterTypes[i];

					openConstructedParameterType.MakeGenericTypeVia(closedConstructedParameterType, resolvedGenericParameters);
				}

				// Construct the final closed-constructed method from the resolved arguments

				var openConstructedGenericArguments = openConstructedMethod.GetGenericArguments();
				var closedConstructedGenericArguments = openConstructedGenericArguments.Select(openConstructedGenericArgument =>
				{
					// If the generic argument has been successfully resolved, use it;
					// otherwise, leave the open-constructed argument in place.

					if (resolvedGenericParameters.ContainsKey(openConstructedGenericArgument))
					{
						return resolvedGenericParameters[openConstructedGenericArgument];
					}
					else
					{
						return openConstructedGenericArgument;
					}
				}).ToArray();

				return openConstructedMethod.MakeGenericMethod(closedConstructedGenericArguments);
			}
		}

		public static bool IsGenericExtension(this MethodInfo methodInfo)
		{
			return genericExtensionMethods.Contains(methodInfo);
		}

		public static IEnumerable<MethodInfo> GetExtensionMethods(this Type thisArgumentType)
		{
			lock (resolvedExtensionMethods)
			{
				if (!MemberUtility.resolvedExtensionMethods.TryGetValue(thisArgumentType, out var resolvedExtensionMethods))
				{
					resolvedExtensionMethods = new HashSet<MethodInfo>(ResolveExtensionMethods(thisArgumentType));
					MemberUtility.resolvedExtensionMethods.Add(thisArgumentType, resolvedExtensionMethods);
				}

				return resolvedExtensionMethods;
			}
		}

		public const bool supportGenericExtensionMethods = false;

		private static IEnumerable<MethodInfo> ResolveExtensionMethods(this Type thisArgumentType)
		{
			foreach (var kvp in definedExtensionMethods)
			{
				var definedThisParameterType = kvp.Key;
				var definedMethods = kvp.Value;

				bool exactThis = false;
				bool closeConstructingThis = false;
				bool inheritedThis = false;

				if (definedThisParameterType == thisArgumentType)
				{
					exactThis = true;
				}
				else if (supportGenericExtensionMethods && definedThisParameterType.CanMakeGenericTypeVia(thisArgumentType))
				{
					closeConstructingThis = true;
				}
				else if (definedThisParameterType.IsAssignableFrom(thisArgumentType))
				{
					inheritedThis = true;
				}

				var compatibleThis = exactThis || closeConstructingThis || inheritedThis;

				if (!compatibleThis)
				{
					continue;
				}

				foreach (var definedMethod in definedMethods)
				{
					if (closeConstructingThis && definedMethod.ContainsGenericParameters)
					{
						var closedConstructedParameterTypes = thisArgumentType.Yield().Concat(definedMethod.GetParametersWithoutThis().Select(p => p.ParameterType));

						var closedConstructedMethod = definedMethod.MakeGenericMethodVia(closedConstructedParameterTypes.ToArray());

						genericExtensionMethods.Add(closedConstructedMethod);

						yield return closedConstructedMethod;
					}
					else
					{
						yield return definedMethod;
					}
				}
			}

			yield break;
		}

		public static HashSet<MethodInfo> GetOperators(this Type type)
		{
			return definedOperators[type];
		}

		public static HashSet<string> GetOperatorNames(this Type type)
		{
			return definedOperatorNames[type];
		}

		public static bool IsExtension(this MethodInfo methodInfo)
		{
			return methodInfo.HasAttribute<ExtensionAttribute>(false);
		}

		public static bool IsExtensionMethod(this MemberInfo memberInfo)
		{
			return memberInfo is MethodInfo methodInfo && methodInfo.IsExtension();
		}

		public static Delegate CreateDelegate(this MethodInfo methodInfo, Type delegateType)
		{
			return Delegate.CreateDelegate(delegateType, methodInfo);
		}

		public static bool IsAccessor(this MemberInfo memberInfo)
		{
			return memberInfo is FieldInfo || memberInfo is PropertyInfo;
		}

		public static Type GetAccessorType(this MemberInfo memberInfo)
		{
			if (memberInfo is FieldInfo)
			{
				return ((FieldInfo)memberInfo).FieldType;
			}
			else if (memberInfo is PropertyInfo)
			{
				return ((PropertyInfo)memberInfo).PropertyType;
			}
			else
			{
				return null;
			}
		}

		public static bool IsPubliclyGettable(this MemberInfo memberInfo)
		{
			if (memberInfo is FieldInfo)
			{
				return ((FieldInfo)memberInfo).IsPublic;
			}
			else if (memberInfo is PropertyInfo)
			{
				var propertyInfo = (PropertyInfo)memberInfo;

				return propertyInfo.CanRead && propertyInfo.GetGetMethod(false) != null;
			}
			else if (memberInfo is MethodInfo)
			{
				return ((MethodInfo)memberInfo).IsPublic;
			}
			else if (memberInfo is ConstructorInfo)
			{
				return ((ConstructorInfo)memberInfo).IsPublic;
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public static Type DeclaringOrExtendedType(this MemberInfo memberInfo)
		{
			if (memberInfo is MethodInfo methodInfo && methodInfo.IsExtension())
			{
				return methodInfo.GetParameters()[0].ParameterType;
			}
			else
			{
				return memberInfo.DeclaringType;
			}
		}

		public static bool IsStatic(this PropertyInfo propertyInfo)
		{
			return (propertyInfo.GetGetMethod(true)?.IsStatic ?? false) ||
				   (propertyInfo.GetSetMethod(true)?.IsStatic ?? false);
		}

		public static bool IsStatic(this MemberInfo memberInfo)
		{
			if (memberInfo is FieldInfo)
			{
				return ((FieldInfo)memberInfo).IsStatic;
			}
			else if (memberInfo is PropertyInfo)
			{
				return ((PropertyInfo)memberInfo).IsStatic();
			}
			else if (memberInfo is MethodBase)
			{
				return ((MethodBase)memberInfo).IsStatic;
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public static bool IsIndexer(this PropertyInfo propertyInfo)
		{
			return propertyInfo.GetIndexParameters().Length > 0;
		}

		public static ParameterInfo GetThisParameter(this MethodInfo methodInfo, bool safe = true)
		{
			if (safe && !methodInfo.IsExtension())
			{
				throw new InvalidOperationException("Cannot get this parameter of a non-extension method.");
			}

			return methodInfo.GetParameters()[0];
		}

		public static IEnumerable<ParameterInfo> GetParametersWithoutThis(this MethodBase methodBase)
		{
			return methodBase.GetParameters().Skip(methodBase.IsExtensionMethod() ? 1 : 0);
		}

		public static Type UnderlyingParameterType(this ParameterInfo parameterInfo)
		{
			if (parameterInfo.ParameterType.IsByRef)
			{
				return parameterInfo.ParameterType.GetElementType();
			}
			else
			{
				return parameterInfo.ParameterType;
			}
		}

		// https://stackoverflow.com/questions/9977530/
		// https://stackoverflow.com/questions/16186694
		public static bool HasDefaultValue(this ParameterInfo parameterInfo)
		{
			return (parameterInfo.Attributes & ParameterAttributes.HasDefault) == ParameterAttributes.HasDefault;
		}
		
		public static object DefaultValue(this ParameterInfo parameterInfo)
		{
			if (parameterInfo.HasDefaultValue())
			{
				var defaultValue = parameterInfo.DefaultValue;

				// https://stackoverflow.com/questions/45393580
				if (defaultValue == null && parameterInfo.ParameterType.IsValueType)
				{
					defaultValue = parameterInfo.ParameterType.Default();
				}

				return defaultValue;
			}
			else
			{
				return parameterInfo.UnderlyingParameterType().Default();
			}
		}

		public static object PseudoDefaultValue(this ParameterInfo parameterInfo)
		{
			if (parameterInfo.HasDefaultValue())
			{
				var defaultValue = parameterInfo.DefaultValue;

				// https://stackoverflow.com/questions/45393580
				if (defaultValue == null && parameterInfo.ParameterType.IsValueType)
				{
					defaultValue = parameterInfo.ParameterType.PseudoDefault();
				}

				return defaultValue;
			}
			else
			{
				return parameterInfo.UnderlyingParameterType().PseudoDefault();
			}
		}

		public static bool AllowsNull(this ParameterInfo parameterInfo)
		{
			var type = parameterInfo.ParameterType;

			return (type.IsReferenceType() && parameterInfo.HasAttribute<AllowsNullAttribute>()) || Nullable.GetUnderlyingType(type) != null;
		}

		public static bool CanWrite(this FieldInfo fieldInfo)
		{
			return !(fieldInfo.IsInitOnly || fieldInfo.IsLiteral);
		}

		public static Member ToManipulator(this MemberInfo memberInfo, Type targetType)
		{
			if (memberInfo is FieldInfo fieldInfo)
			{
				return fieldInfo.ToManipulator(targetType);
			}

			if (memberInfo is PropertyInfo propertyInfo)
			{
				return propertyInfo.ToManipulator(targetType);
			}

			if (memberInfo is MethodInfo methodInfo)
			{
				return methodInfo.ToManipulator(targetType);
			}

			if (memberInfo is ConstructorInfo constructorInfo)
			{
				return constructorInfo.ToManipulator(targetType);
			}

			throw new InvalidOperationException();
		}

		public static Member ToManipulator(this MemberInfo memberInfo, object target = null)
		{
			if (memberInfo is FieldInfo fieldInfo)
			{
				return fieldInfo.ToManipulator(target);
			}

			if (memberInfo is PropertyInfo propertyInfo)
			{
				return propertyInfo.ToManipulator(target);
			}

			if (memberInfo is MethodInfo methodInfo)
			{
				return methodInfo.ToManipulator(target);
			}

			if (memberInfo is ConstructorInfo constructorInfo)
			{
				return constructorInfo.ToManipulator(target);
			}

			throw new InvalidOperationException();
		}

		public static Member ToManipulator(this FieldInfo fieldInfo, Type targetType)
		{
			return new Member(targetType, fieldInfo);
		}

		public static Member ToManipulator(this PropertyInfo propertyInfo, Type targetType)
		{
			return new Member(targetType, propertyInfo);
		}

		public static Member ToManipulator(this MethodInfo methodInfo, Type targetType)
		{
			return new Member(targetType, methodInfo);
		}

		public static Member ToManipulator(this ConstructorInfo constructorInfo, Type targetType)
		{
			return new Member(targetType, constructorInfo);
		}

		public static Member ToManipulator(this FieldInfo fieldInfo, object target = null)
		{
			return new Member(target?.GetType() ?? fieldInfo.DeclaringType, fieldInfo, target);
		}

		public static Member ToManipulator(this PropertyInfo propertyInfo, object target = null)
		{
			return new Member(target?.GetType() ?? propertyInfo.DeclaringType, propertyInfo, target);
		}

		public static Member ToManipulator(this MethodInfo methodInfo, object target = null)
		{
			return new Member(target?.GetType() ?? methodInfo.DeclaringOrExtendedType(), methodInfo, target);
		}

		public static Member ToManipulator(this ConstructorInfo constructorInfo, object target = null)
		{
			return new Member(target?.GetType() ?? constructorInfo.DeclaringType, constructorInfo, target);
		}

		public static ConstructorInfo GetConstructorAccepting(this Type type, Type[] paramTypes, bool nonPublic)
		{
			var bindingFlags = BindingFlags.Instance | BindingFlags.Public;

			if (nonPublic)
			{
				bindingFlags |= BindingFlags.NonPublic;
			}

			return type
				.GetConstructors(bindingFlags)
				.FirstOrDefault(constructor =>
				{
					var parameters = constructor.GetParameters();

					if (parameters.Length != paramTypes.Length)
					{
						return false;
					}

					for (var i = 0; i < parameters.Length; i++)
					{
						if (paramTypes[i] == null)
						{
							if (!parameters[i].ParameterType.IsNullable())
							{
								return false;
							}
						}
						else
						{
							if (!parameters[i].ParameterType.IsAssignableFrom(paramTypes[i]))
							{
								return false;
							}
						}
					}

					return true;
				});
		}
		
		public static ConstructorInfo GetConstructorAccepting(this Type type, params Type[] paramTypes)
		{
			return GetConstructorAccepting(type, paramTypes, true);
		}

		public static ConstructorInfo GetPublicConstructorAccepting(this Type type, params Type[] paramTypes)
		{
			return GetConstructorAccepting(type, paramTypes, false);
		}

		public static ConstructorInfo GetDefaultConstructor(this Type type)
		{
			return GetConstructorAccepting(type);
		}

		public static ConstructorInfo GetPublicDefaultConstructor(this Type type)
		{
			return GetPublicConstructorAccepting(type);
		}

		public static MemberInfo[] GetExtendedMember(this Type type, string name, MemberTypes types, BindingFlags flags)
		{
			using (ProfilingUtility.SampleBlock("GetExtendedMember"))
			{
				var members = type.GetMember(name, types, flags).ToList();

				using (ProfilingUtility.SampleBlock("GetExtendedMember.Extensions"))
				{
					if (types.HasFlag(MemberTypes.Method)) // Check for extension methods
					{
						members.AddRange(type.GetExtensionMethods()
							.Where(extension => extension.Name == name)
							.Cast<MemberInfo>());
					}
				}

				return members.ToArray();
			}
		}

		public static MemberInfo[] GetExtendedMembers(this Type type, BindingFlags flags)
		{
			var members = type.GetMembers(flags).ToHashSet();

			foreach (var extensionMethod in type.GetExtensionMethods())
			{
				members.Add(extensionMethod);
			}

			return members.ToArray();
		}

		#region Signature Disambiguation
		
		private static bool NameMatches(this MemberInfo member, string name)
		{
			return member.Name == name;
		}

		private static bool ParametersMatch(this MethodBase methodBase, IEnumerable<Type> parameterTypes)
		{
			Ensure.That(nameof(parameterTypes)).IsNotNull(parameterTypes);
			
			return methodBase.GetParametersWithoutThis().Select(paramInfo => paramInfo.ParameterType).SequenceEqual(parameterTypes);
		}

		private static bool GenericArgumentsMatch(this MethodInfo method, IEnumerable<Type> genericArgumentTypes)
		{
			Ensure.That(nameof(genericArgumentTypes)).IsNotNull(genericArgumentTypes);

			if (method.ContainsGenericParameters)
			{
				return false;
			}

			return method.GetGenericArguments().SequenceEqual(genericArgumentTypes);
		}

		public static bool SignatureMatches(this FieldInfo field, string name)
		{
			return field.NameMatches(name);
		}

		public static bool SignatureMatches(this PropertyInfo property, string name)
		{
			return property.NameMatches(name);
		}

		public static bool SignatureMatches(this ConstructorInfo constructor, string name, IEnumerable<Type> parameterTypes)
		{
			return constructor.NameMatches(name) && constructor.ParametersMatch(parameterTypes);
		}

		public static bool SignatureMatches(this MethodInfo method, string name, IEnumerable<Type> parameterTypes)
		{
			return method.NameMatches(name) && method.ParametersMatch(parameterTypes) && !method.ContainsGenericParameters;
		}

		public static bool SignatureMatches(this MethodInfo method, string name, IEnumerable<Type> parameterTypes, IEnumerable<Type> genericArgumentTypes)
		{
			return method.NameMatches(name) && method.ParametersMatch(parameterTypes) && method.GenericArgumentsMatch(genericArgumentTypes);
		}

		public static FieldInfo GetFieldUnambiguous(this Type type, string name, BindingFlags flags)
		{
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(name)).IsNotNull(name);
			
			flags |= BindingFlags.DeclaredOnly;

			while (type != null)
			{
				var field = type.GetField(name, flags);

				if (field != null)
				{
					return field;
				}

				type = type.BaseType;
			}

			return null;
		}

		public static PropertyInfo GetPropertyUnambiguous(this Type type, string name, BindingFlags flags)
		{
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(name)).IsNotNull(name);

			flags |= BindingFlags.DeclaredOnly;

			while (type != null)
			{
				var property = type.GetProperty(name, flags);

				if (property != null)
				{
					return property;
				}

				type = type.BaseType;
			}
			
			return null;
		}

		public static MethodInfo GetMethodUnambiguous(this Type type, string name, BindingFlags flags)
		{
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(name)).IsNotNull(name);

			flags |= BindingFlags.DeclaredOnly;

			while (type != null)
			{
				var method = type.GetMethod(name, flags);

				if (method != null)
				{
					return method;
				}

				type = type.BaseType;
			}
			
			return null;
		}

		private static TMemberInfo DisambiguateHierarchy<TMemberInfo>(this IEnumerable<TMemberInfo> members, Type type) where TMemberInfo : MemberInfo
		{
			while (type != null)
			{
				foreach (var member in members)
				{
					if (member.DeclaringOrExtendedType() == type)
					{
						return member;
					}
				}

				type = type.BaseType;
			}

			return null;
		}

		public static FieldInfo Disambiguate(this IEnumerable<FieldInfo> fields, Type type)
		{
			Ensure.That(nameof(fields)).IsNotNull(fields);
			Ensure.That(nameof(type)).IsNotNull(type);

			return fields.DisambiguateHierarchy(type);
		}

		public static PropertyInfo Disambiguate(this IEnumerable<PropertyInfo> properties, Type type)
		{
			Ensure.That(nameof(properties)).IsNotNull(properties);
			Ensure.That(nameof(type)).IsNotNull(type);

			return properties.DisambiguateHierarchy(type);
		}

		public static ConstructorInfo Disambiguate(this IEnumerable<ConstructorInfo> constructors, Type type, IEnumerable<Type> parameterTypes)
		{
			Ensure.That(nameof(constructors)).IsNotNull(constructors);
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(parameterTypes)).IsNotNull(parameterTypes);
			
			return constructors.Where(m => m.ParametersMatch(parameterTypes) && !m.ContainsGenericParameters).DisambiguateHierarchy(type);
		}

		public static MethodInfo Disambiguate(this IEnumerable<MethodInfo> methods, Type type, IEnumerable<Type> parameterTypes)
		{
			Ensure.That(nameof(methods)).IsNotNull(methods);
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(parameterTypes)).IsNotNull(parameterTypes);
			
			return methods.Where(m => m.ParametersMatch(parameterTypes) && !m.ContainsGenericParameters).DisambiguateHierarchy(type);
		}

		public static MethodInfo Disambiguate(this IEnumerable<MethodInfo> methods, Type type, IEnumerable<Type> parameterTypes, IEnumerable<Type> genericArgumentTypes)
		{
			Ensure.That(nameof(methods)).IsNotNull(methods);
			Ensure.That(nameof(type)).IsNotNull(type);
			Ensure.That(nameof(parameterTypes)).IsNotNull(parameterTypes);
			Ensure.That(nameof(genericArgumentTypes)).IsNotNull(genericArgumentTypes);

			return methods.Where(m => m.ParametersMatch(parameterTypes) && m.GenericArgumentsMatch(genericArgumentTypes)).DisambiguateHierarchy(type);
		}

		#endregion
	}
}