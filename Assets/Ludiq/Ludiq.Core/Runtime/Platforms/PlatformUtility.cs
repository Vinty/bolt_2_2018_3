﻿using System;
using System.Linq.Expressions;
using UnityEngine;

namespace Ludiq
{
	public static class PlatformUtility
	{
		public static readonly bool supportsJit;

		static PlatformUtility()
		{
			supportsJit = CheckJitSupport();
		}

		private static bool CheckJitSupport()
		{
			if (Application.platform.IsEditor())
			{
				return true;
			}
			else if (Application.platform.IsStandalone())
			{
				try
				{
					Expression.Lambda(Expression.Parameter(typeof(int), "Test")).Compile();
					Debug.Log("JIT is supported on this standalone build.");
					return true;
				}
				catch (Exception)
				{
					Debug.LogWarning("JIT is not supported on this standalone build.");
				}
			}

			return false;
		}
		
		public static bool IsEditor(this RuntimePlatform platform)
		{
			return
				platform == RuntimePlatform.WindowsEditor ||
				platform == RuntimePlatform.OSXEditor ||
				platform == RuntimePlatform.LinuxEditor;
		}
		
		public static bool IsStandalone(this RuntimePlatform platform)
		{
			return
				platform == RuntimePlatform.WindowsPlayer ||
				platform == RuntimePlatform.OSXPlayer ||
				platform == RuntimePlatform.LinuxPlayer;
		}
	}
}