//--------------------------------------------------------------
//              _______      _____      __       ________       
//             |  ___  \    /     \    |  |     |___  ___|      
//             | |   \  |  /  ___  \   |  |         / /         
//             | |___/ /  /  /   \  \  |  |        / /_         
//             | |   \ \  \  \___/  /  |  |       /_  /         
//             | |___/  |  \       /   |  |____    | /          
//             |_______/    \_____/    |_______|   |/           
//                                                              
//                 V I S U A L    S C R I P T I N G             
//--------------------------------------------------------------
//                                                              
// THIS FILE IS AUTO-GENERATED.                                 
//                                                              
// ANY CHANGES WILL BE LOST NEXT TIME THIS SCRIPT IS GENERATED. 
//                                                              
//--------------------------------------------------------------
#pragma warning disable 219

namespace Ludiq.Generated.Aot
{
	[UnityEngine.Scripting.PreserveAttribute()]
	public class AotStubs
	{
		// UnityEngine.Transform.position
		[UnityEngine.Scripting.PreserveAttribute()]
		public static void UnityEngine_Transform_position()
		{
			UnityEngine.Transform target = default(UnityEngine.Transform);
			UnityEngine.Vector3 accessor = target.position;
			
			target.position = default(UnityEngine.Vector3);
			
			Ludiq.InstancePropertyAccessor<UnityEngine.Transform, UnityEngine.Vector3> optimized = new Ludiq.InstancePropertyAccessor<UnityEngine.Transform, UnityEngine.Vector3>(default(System.Reflection.PropertyInfo));
			
			optimized.GetValue(default(UnityEngine.Transform));
			optimized.SetValue(default(UnityEngine.Transform), default(UnityEngine.Vector3));
		}
		
		// UnityEngine.Transform.Rotate
		[UnityEngine.Scripting.PreserveAttribute()]
		public static void UnityEngine_Transform_Rotate()
		{
			UnityEngine.Transform target = default(UnityEngine.Transform);
			UnityEngine.Vector3 arg0 = default(UnityEngine.Vector3);
			
			target.Rotate(arg0);
			
			Ludiq.InstanceActionInvoker<UnityEngine.Transform, UnityEngine.Vector3> optimized = new Ludiq.InstanceActionInvoker<UnityEngine.Transform, UnityEngine.Vector3>(default(System.Reflection.MethodInfo));
			
			optimized.Invoke(default(UnityEngine.Transform), arg0);
			optimized.Invoke(default(object[]));
		}
	}
}
