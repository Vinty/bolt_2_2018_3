//--------------------------------------------------------------
//              _______      _____      __       ________       
//             |  ___  \    /     \    |  |     |___  ___|      
//             | |   \  |  /  ___  \   |  |         / /         
//             | |___/ /  /  /   \  \  |  |        / /_         
//             | |   \ \  \  \___/  /  |  |       /_  /         
//             | |___/  |  \       /   |  |____    | /          
//             |_______/    \_____/    |_______|   |/           
//                                                              
//                 V I S U A L    S C R I P T I N G             
//--------------------------------------------------------------
//                                                              
// THIS FILE IS AUTO-GENERATED.                                 
//                                                              
// ANY CHANGES WILL BE LOST NEXT TIME THIS SCRIPT IS GENERATED. 
//                                                              
//--------------------------------------------------------------
#pragma warning disable 162, 219, 429

using Bolt;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Bolt.Generated
{
    public class BolttestGraphScript : FlowGraphScript
    {
        private EventHook startHook;
        private Action<EmptyEventArgs> startHandler;
        private EventHook updateHook;
        private Action<EmptyEventArgs> updateHandler;
        
        public BolttestGraphScript(IMachineScript machineScript) : base(machineScript) {}
        
        public override FlowGraphData graphData
        {
            get => _graphData;
            set
            {
                _graphData = value;
            }
        }
        
        public void Start()
        {
            gameObject.transform.position = new Vector3(1.5f, 0f, 0f);
        }
        
        public void Update()
        {
            gameObject.transform.Rotate(Vector3.right);
        }
        
        public override void StartListening()
        {
            startHook = new EventHook("Start", machineScript);
            startHandler = args => Start();
            EventBus.Register(startHook, startHandler);
            updateHook = new EventHook("Update", machineScript);
            updateHandler = args => Update();
            EventBus.Register(updateHook, updateHandler);
        }
        
        public override void StopListening()
        {
            EventBus.Unregister(startHook, startHandler);
            startHandler = null;
            EventBus.Unregister(updateHook, updateHandler);
            updateHandler = null;
        }
    }
}
