//--------------------------------------------------------------
//              _______      _____      __       ________       
//             |  ___  \    /     \    |  |     |___  ___|      
//             | |   \  |  /  ___  \   |  |         / /         
//             | |___/ /  /  /   \  \  |  |        / /_         
//             | |   \ \  \  \___/  /  |  |       /_  /         
//             | |___/  |  \       /   |  |____    | /          
//             |_______/    \_____/    |_______|   |/           
//                                                              
//                 V I S U A L    S C R I P T I N G             
//--------------------------------------------------------------
//                                                              
// THIS FILE IS AUTO-GENERATED.                                 
//                                                              
// ANY CHANGES WILL BE LOST NEXT TIME THIS SCRIPT IS GENERATED. 
//                                                              
//--------------------------------------------------------------
using Ludiq;
using System;

namespace Bolt.Generated
{
    public class BolttestMachineScript : Bolt.FlowMachineScript
    {
        public BolttestMachineScript()
        {
            graphScript = new BolttestGraphScript(this);
        }
        
        protected virtual void Update()
        {
            TriggerEvent(Bolt.EventHooks.Update);
        }
    }
}
